module.exports = {
    PORT: 3133,
    API_VERSION: '1.0.0',
    VERSION_HISTORY: {
        '1.0.0': 'v1'
    },
    secretAccessKey: "",
    accessKeyId: "",
    region: '', // region of your bucket
    MODE: "development",      // "production" || "development"

    JOIN_NIBI: {
        development: "https://sapp.nibi.io/signup",
        production: "https://www.app.nibi.io/signup"
    },
    JOIN_EVENT: {
        development: "https://sapp.nibi.io/join-event/search-events?event_code=",
        production: "https://www.app.nibi.io/join-event/search-events?event_code="
    },
    RESET_PASSWORD: {
        development: "https://sapp.nibi.io/reset-password?utm_source=",
        production: "https://www.app.nibi.io/reset-password?utm_source="
    },

    EMAIL_CONFIRMATION: {
        development: "https://sapp.nibi.io/verify-email?utm_source=",
        production: "https://www.app.nibi.io/verify-email?utm_source="
    },
    BUCKET: {
        development: {
            accessKeyId: 'AKIAW3ARZD76QAUOS7MZ',
            secretAccessKey: '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',
            Bucket: 'app.nibi.io-storage-bucket'
        },
        production: {
            accessKeyId: 'AKIAW3ARZD76QAUOS7MZ',
            secretAccessKey: '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',
            Bucket: 'nibi-prod-file-storage'
        }
    },
    SECRET_KEY: {
        development: "sk_test_51HpR7xATGHIudvnm3GogRrBlY3bALPrt1W1GpgGSPt93PWQK7M8j4FVCyWsCjeRm8mzus8F6hJ0E2xa9Mrpm7H5Z00xYFvOcct",
        production: "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"
    },
    PUBLISHABLE_KEY: {
        development: "pk_test_51HpR7xATGHIudvnmeZalMfVNYsC2YnjvGcgeyB9sZULcRORQXyr3siBP9QSGQXkxEmIkb4z7HgtEpmjTyb50oCsY00IghNm6Oj",
        production: "pk_live_51HpR7xATGHIudvnmQwOh9kHaswxSv7Kj41BTqMsrfTpTq5cV4HVvPUz7jVLQ8Y2iRwFOfQHc5RtkrTwA0GoNao5100DOzFhCdC"
    },
    SPLIT_FROM: {                     // Key used on image url delete. In db store full image url , but on s3 store only image name. So split the image name from image url.
        development: "bucket/",
        production: "amazonaws.com/"
    },
    TWILIO_ACCOUNT_SID: {
        development: "AC1b51b6b9319778ace982ae04f9c134e0",
        production: "AC1b51b6b9319778ace982ae04f9c134e0"
    },
    TWILIO_AUTH_TOKEN: {
        development: "72f479aba9e3355a85079b34dbbcfd99",
        production: "72f479aba9e3355a85079b34dbbcfd99"
    },
    SENDGRID_API_KEY: {
        development: "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",
        production: "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"
    },
    APP_VERSION: {
        development: "2.1.0",
        production: "2.1.0"
    },
}

