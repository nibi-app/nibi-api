var twilio = require('twilio');
const Env = require("../../env");
const accountSid = Env.TWILIO_ACCOUNT_SID[Env.MODE];
const authToken = Env.TWILIO_AUTH_TOKEN[Env.MODE];
const client = require('twilio')(accountSid, authToken);

module.exports = {
    sendMsg(smsDetails) {
        client.messages.create({
            body: smsDetails.body,
            to: smsDetails.to,                    // Text this number
            from: "+12142714650"                // From a valid Twilio number
        }).then((message) =>
            console.log(message.sid))
    }

};