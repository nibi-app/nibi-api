const admin = require("firebase-admin");
const moment = require('moment');
const User = require("../release/v1/models/user");
const cron = require('node-cron');
// const emoji = require('node-emoji');
const serviceAccount = require("../../nibi-288314-firebase-adminsdk-4ztcy-ab23becd92.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    // databaseURL: "https://nibi-288314.firebaseio.com",
    //  authDomain: "nibi-288314.firebaseapp.com",
});

const db = admin.firestore();

const notification_options = {
    priority: "high",
    timeToLive: 60 * 60 * 24,
    content_available: true,
    mutable_content: true
};

const doc = db.collection('chatRoom');
doc.onSnapshot(docSnapshot => {
    docSnapshot.docChanges().forEach(change => {
        // if (change.type === 'added') {
        //     console.log('New city: ', change.doc.data());
        // }
        if (change.type === 'modified') {
            //   console.log(change.doc.data());
            if (change.doc.data().lastMessage.isRead == false) {
                db.collection('users').where('nibiId', '==', change.doc.data().lastMessage.receiver.nibiId).get()
                    .then(function (userRecord) {
                        userRecord.forEach(doc => {
                            if (doc.data()) {
                                User.findOne({
                                    nibiId: change.doc.data().lastMessage.receiver.nibiId,
                                    isNotify: true
                                }, function (err, user) {
                                    if (user) {
                                        if (user.fcmToken) {
                                            let fcmTokens = user.fcmToken;
                                            let badge = `${doc.data().unreadNotificationCount + doc.data().unreadChatCount + 1}`;
                                            let body;
                                            if (change.doc.data().lastMessage.type == "text") {
                                                body = change.doc.data().lastMessage.message
                                            }
                                            if (change.doc.data().lastMessage.type == "image") {
                                                body = "Sent you an image."
                                            }
                                            if (change.doc.data().lastMessage.type == "GIF") {
                                                body = "Sent you a gif."
                                            }
                                            if (change.doc.data().lastMessage.type == "audio") {
                                                body = "Sent you an audio."
                                            }
                                            if (change.doc.data().lastMessage.type == "video") {
                                                body = "Sent you a video."
                                            }


                                            let payload = {
                                                notification: {
                                                    title: change.doc.data().lastMessage.sender.firstName + " " + change.doc.data().lastMessage.sender.lastName,
                                                    body: body,
                                                    sound: "default",
                                                    badge: badge
                                                },
                                                data: {
                                                    title: change.doc.data().lastMessage.sender.firstName + " " + change.doc.data().lastMessage.sender.lastName,
                                                    body: body,
                                                    postId: "",
                                                    eventId: "",
                                                    notificationType: "message",
                                                    id: change.doc.data().id,
                                                    userNibiId: change.doc.data().lastMessage.sender.nibiId,
                                                    userFirstName: change.doc.data().lastMessage.sender.firstName,
                                                    userLastName: change.doc.data().lastMessage.sender.lastName,
                                                    userImage: change.doc.data().lastMessage.sender.profilePic
                                                },
                                            }
                                            db.collection('users').doc(change.doc.data().lastMessage.receiver.nibiId).update({
                                                'unreadChatCount': doc.data().unreadChatCount + 1
                                            }).then(updatedResult => {
                                                console.log("firebase unread chat count update.")
                                            });
                                            const registrationToken = fcmTokens;
                                            // const message_notification = {
                                            //     notification: {
                                            //         title: payload.notification.title,
                                            //         body: payload.notification.body,
                                            //         badge: payload.notification.badge,
                                            //         sound: "default"
                                            //     },
                                            //     data: payload.data
                                            // };
                                            const options = notification_options
                                            admin.messaging().sendToDevice(registrationToken, payload, options)
                                                .then(response => {
                                                    console.log(response);
                                                    // db.collection('users').doc(change.doc.data().lastMessage.receiver.nibiId).update({
                                                    //     'unreadChatCount': doc.data().unreadChatCount + 1
                                                    // }).then(updatedResult => {
                                                        User.updateOne({nibiId: change.doc.data().lastMessage.receiver.nibiId}, {isNewMessage: true}, function (err, isNewMessageStatus) {
                                                            if (err) {
                                                                console.log(err);
                                                            } else {
                                                                console.log("Notification sent successfully");
                                                            }
                                                        });
                                                    // }).catch(error => {
                                                    //     console.log("Error in update badge count.");
                                                    //     console.log(error);
                                                    // });
                                                }).catch(error => {
                                                console.log("Error in send notification");
                                                console.log(error);
                                            });
                                        } else {
                                            console.log("User not wants to get notification.")
                                        }
                                    } else {
                                        console.log("No user details found from database.")
                                    }
                                });
                            } else {
                                console.log("User data not found in firestore.")
                            }

                        });
                    }).catch(function (error) {
                    console.log("Error fetching user data:", error);
                });
            } else {
                console.log("No new message found.");
            }
        }
        // if (change.type === 'removed') {
        //     console.log('Removed city: ', change.doc.data());
        // }
    });
    // ...
}, err => {
    console.log(`Encountered error: ${err}`);
});



const document = db.collection('chatRoomGroup');
document.onSnapshot(docSnapshot => {
    docSnapshot.docChanges().forEach(change => {
        // if (change.type === 'added') {
        //     console.log('New city: ', change.doc.data());
        // }
        if (change.type === 'modified') {
            //   console.log(change.doc.data());
            if (change.doc.data().type == "Single") {
                if (change.doc.data().lastMessage.isRead == false) {
                    db.collection('users').where('nibiId', '==', change.doc.data().lastMessage.receiver.nibiId).get()
                        .then(function (userRecord) {
                            userRecord.forEach(doc => {
                                if (doc.data()) {
                                    User.findOne({
                                        nibiId: change.doc.data().lastMessage.receiver.nibiId,
                                        isNotify: true
                                    }, function (err, user) {
                                        if (user) {
                                            if (user.fcmToken) {
                                                let fcmTokens = user.fcmToken;
                                                let badge = `${doc.data().unreadNotificationCount + doc.data().unreadChatCount + 1}`;
                                                let body;
                                                if (change.doc.data().lastMessage.type == "text") {
                                                    body = change.doc.data().lastMessage.message
                                                }
                                                if (change.doc.data().lastMessage.type == "image") {
                                                    body = "Sent you an image."
                                                }
                                                if (change.doc.data().lastMessage.type == "GIF") {
                                                    body = "Sent you a gif."
                                                }
                                                if (change.doc.data().lastMessage.type == "audio") {
                                                    body = "Sent you an audio."
                                                }
                                                if (change.doc.data().lastMessage.type == "video") {
                                                    body = "Sent you a video."
                                                }


                                                let payload = {
                                                    notification: {
                                                        title: change.doc.data().lastMessage.sender.firstName + " " + change.doc.data().lastMessage.sender.lastName,
                                                        body: body,
                                                        sound: "default",
                                                        badge: badge
                                                    },
                                                    data: {
                                                        title: change.doc.data().lastMessage.sender.firstName + " " + change.doc.data().lastMessage.sender.lastName,
                                                        body: body,
                                                        postId: "",
                                                        eventId: "",
                                                        notificationType: "message",
                                                        id: change.doc.data().id,
                                                        userNibiId: change.doc.data().lastMessage.sender.nibiId,
                                                        userFirstName: change.doc.data().lastMessage.sender.firstName,
                                                        userLastName: change.doc.data().lastMessage.sender.lastName,
                                                        userImage: change.doc.data().lastMessage.sender.profilePic
                                                    },
                                                }
                                                db.collection('users').doc(change.doc.data().lastMessage.receiver.nibiId).update({
                                                    'unreadChatCount': doc.data().unreadChatCount + 1
                                                }).then(updatedResult => {
                                                    console.log("firebase unread chat count update.")
                                                });
                                                const registrationToken = fcmTokens;
                                                // const message_notification = {
                                                //     notification: {
                                                //         title: payload.notification.title,
                                                //         body: payload.notification.body,
                                                //         badge: payload.notification.badge,
                                                //         sound: "default"
                                                //     },
                                                //     data: payload.data
                                                // };
                                                const options = notification_options
                                                admin.messaging().sendToDevice(registrationToken, payload, options)
                                                    .then(response => {
                                                        console.log(response);
                                                        // db.collection('users').doc(change.doc.data().lastMessage.receiver.nibiId).update({
                                                        //     'unreadChatCount': doc.data().unreadChatCount + 1
                                                        // }).then(updatedResult => {
                                                        User.updateOne({nibiId: change.doc.data().lastMessage.receiver.nibiId}, {isNewMessage: true}, function (err, isNewMessageStatus) {
                                                            if (err) {
                                                                console.log(err);
                                                            } else {
                                                                console.log("Notification sent successfully");
                                                            }
                                                        });
                                                        // }).catch(error => {
                                                        //     console.log("Error in update badge count.");
                                                        //     console.log(error);
                                                        // });
                                                    }).catch(error => {
                                                    console.log("Error in send notification");
                                                    console.log(error);
                                                });
                                            } else {
                                                console.log("User not wants to get notification.")
                                            }
                                        } else {
                                            console.log("No user details found from database.")
                                        }
                                    });
                                } else {
                                    console.log("User data not found in firestore.")
                                }

                            });
                        }).catch(function (error) {
                        console.log("Error fetching user data:", error);
                    });
                } else {
                    console.log("No new message found.");
                }
            }else {    // type group start here
                // if (change.doc.data().lastMessage.isRead == false) {
                let receiversArray = change.doc.data().lastMessage.receivers;
                let notifiedUser = receiversArray.filter(e => {
                    return e.read == true;  // Check already notification send or not. if true then notification received all user.
                });
                if (notifiedUser.length > 0) {
                    console.log("No user left to get notification.")
                } else {
                receiversArray.forEach(userData => {
                    if (userData.read == false) {
                        db.collection('users').where('nibiId', '==', userData.nibiId).get()
                            .then(function (userRecord) {
                                userRecord.forEach(doc => {
                                    if (doc.data()) {
                                        User.findOne({
                                            nibiId: userData.nibiId,
                                            isNotify: true
                                        }, function (err, user) {
                                            if (user) {
                                                if (user.fcmToken) {
                                                    let fcmTokens = user.fcmToken;
                                                    let badge = `${doc.data().unreadNotificationCount + doc.data().unreadChatCount + 1}`;
                                                    let body;
                                                    if (change.doc.data().lastMessage.type == "text") {
                                                        body = change.doc.data().lastMessage.message
                                                    }
                                                    if (change.doc.data().lastMessage.type == "image") {
                                                        body = "Sent an image."
                                                    }
                                                    if (change.doc.data().lastMessage.type == "GIF") {
                                                        body = "Sent a gif."
                                                    }
                                                    if (change.doc.data().lastMessage.type == "audio") {
                                                        body = "Sent an audio."
                                                    }
                                                    if (change.doc.data().lastMessage.type == "video") {
                                                        body = "Sent a video."
                                                    }


                                                    let payload = {
                                                        notification: {
                                                            title: change.doc.data().groupName,
                                                            body: change.doc.data().lastMessage.sender.firstName + " " + change.doc.data().lastMessage.sender.lastName + ": " + body,
                                                            sound: "default",
                                                            badge: badge
                                                        },
                                                        data: {
                                                            title: change.doc.data().groupName,
                                                            body: change.doc.data().lastMessage.sender.firstName + " " + change.doc.data().lastMessage.sender.lastName + ": " + body,
                                                            postId: "",
                                                            eventId: "",
                                                            notificationType: "group_message",
                                                            id: change.doc.data().id,
                                                            adminNibiId: change.doc.data().adminNibiId,
                                                            groupName: change.doc.data().groupName,
                                                            groupIcon: change.doc.data().groupIcon,
                                                            userNibiId: change.doc.data().lastMessage.sender.nibiId,
                                                            userFirstName: change.doc.data().lastMessage.sender.firstName,
                                                            userLastName: change.doc.data().lastMessage.sender.lastName,
                                                            userImage: change.doc.data().lastMessage.sender.profilePic
                                                        },
                                                    }
                                                    db.collection('users').doc(userData.nibiId).update({
                                                        'unreadChatCount': doc.data().unreadChatCount + 1
                                                    }).then(updatedResult => {
                                                        console.log("firebase unread chat count update.")
                                                    });
                                                    const registrationToken = fcmTokens;
                                                    // const message_notification = {
                                                    //     notification: {
                                                    //         title: payload.notification.title,
                                                    //         body: payload.notification.body,
                                                    //         badge: payload.notification.badge,
                                                    //         sound: "default"
                                                    //     },
                                                    //     data: payload.data
                                                    // };
                                                    const options = notification_options
                                                    admin.messaging().sendToDevice(registrationToken, payload, options)
                                                        .then(response => {
                                                            console.log(response);
                                                            // db.collection('users').doc(change.doc.data().lastMessage.receiver.nibiId).update({
                                                            //     'unreadChatCount': doc.data().unreadChatCount + 1
                                                            // }).then(updatedResult => {
                                                            User.updateOne({nibiId: userData.nibiId}, {isNewMessage: true}, function (err, isNewMessageStatus) {
                                                                if (err) {
                                                                    console.log(err);
                                                                } else {
                                                                    console.log("Notification sent successfully");
                                                                }
                                                            });
                                                            // }).catch(error => {
                                                            //     console.log("Error in update badge count.");
                                                            //     console.log(error);
                                                            // });
                                                        }).catch(error => {
                                                        console.log("Error in send notification");
                                                        console.log(error);
                                                    });
                                                } else {
                                                    console.log("fcm token not found.");
                                                }
                                            } else {
                                                console.log("User not wants to get notification.");
                                            }
                                        });
                                    } else {
                                        console.log("User data not found in firestore.");
                                    }
                                });
                            }).catch(function (error) {
                            console.log("Error fetching user data:", error);
                        });
                    } else {
                        console.log("No new message found.");
                    }
                });
            }
                // } else {
                //     console.log("No new message found.");
                // }
            }

        }
        // if (change.type === 'removed') {
        //     console.log('Removed city: ', change.doc.data());
        // }
    });
    // ...
}, err => {
    console.log(`Encountered error: ${err}`);
});





module.exports = {

    createTopic(fcmTokens, topics, res, message){
        let registrationTokens = fcmTokens;
        let topic = topics
        admin.messaging().subscribeToTopic(registrationTokens, topic)
            .then(function(response) {
                console.log('Successfully subscribed to topic:', response);

                admin.messaging().send(message)
                    .then(response => {
                        res.send({
                            serverResponse: {
                                isError: false,
                                message: "Notification Send Successfully.",
                                statusCode: 200
                            }
                        });
                        console.log("final response", response);
                    })
                    .catch(error => {
                        console.log("Error in send notification");
                        console.log(error);
                    });

            })
            .catch(function(error) {
                console.log('Error subscribing to topic:', error);
            });
    },

//     notificationForAllUser(req, res, payload, fcmToken) {
//         let topic = 'highScores';
//
//         let message = {
//             data: {
//                 score: '850',
//                 time: '2:45'
//             },
//             topic: topic
//         };
//     admin.messaging().send(message)
//         .then(response => {
//             console.log(response);
//             console.log("Notification sent successfully");
//         })
//         .catch(error => {
//             console.log("Error in send notification");
//             console.log(error);
//         });
// },
    notificationForMultipleUser(payload, fcmTokens, res){
        const message_notification = {
            notification: {
                title: payload.notification.title,
                body: payload.notification.body,
                sound: "default"
            },
            data: payload.data
        };
        const registrationToken = fcmTokens;
        const options = notification_options;
        admin.messaging().sendToDevice(registrationToken, message_notification, options)
            .then(response => {
                res.send({
                    serverResponse: {
                        isError: false,
                        message: "Notification Send Successfully.",
                        statusCode: 200
                    }
                });
            })
            .catch(error => {
                console.log("Error in send notification");
                console.log(error);
            });
    },
    sendNotification(fcmTokens, payload) {
        let allFirebaseUsers =[];
        fcmTokens.forEach (user =>{
            allFirebaseUsers.push( db.collection('users').where('nibiId', '==', user.nibiId).get())
        });
        const message_notification = {
            notification: {
                title: payload.notification.title,
                body: payload.notification.body,
                sound: "default",
                badge: "1"
            },
            data: payload.data
        };
        const options = notification_options;
        Promise.all(allFirebaseUsers).then(users =>{
            users.forEach(user => {
                let unreadNotificationCount = user.docs[0].data().unreadNotificationCount;
                let unreadChatCount =user.docs[0].data().unreadChatCount;
                let thisUser = fcmTokens.map(dbUser => {
                    if(dbUser.nibiId == user.docs[0].data().nibiId){
                        return dbUser;
                    }
                });
                if (thisUser) {
                    const registrationToken = thisUser[0].fcmToken;
                    // message_notification.notification.badge = `${unreadNotificationCount + unreadChatCount + 1}`
                    message_notification.notification.badge = `${unreadNotificationCount + unreadChatCount}`
                    db.collection('users').doc(thisUser[0].nibiId).update({
                        'unreadNotificationCount': unreadNotificationCount + 1
                    }).then(notificationCountUpdate => {
                        console.log("Firebase notification count updated.")
                    });
                    admin.messaging().sendToDevice(registrationToken, message_notification, options)
                        .then(response => {
                            console.log(response);
                            console.log("Notification sent successfully");
                        })
                        .catch(error => {
                            console.log("Error in send notification");
                            console.log(error);
                        });
                } else {
                    console.log("No user wants to get notification");
                }
            });
        });

    },

    allUser(req, res) {
        admin.auth().listUsers()
            .then(function (userRecord) {
                res.status(200).send(userRecord);
            })
            .catch(function (error) {
                console.log("Error fetching user data:", error);
            });
    },
    // This function is used to when a user signup (email, google,facebook, apple) his/her account details will be store in fire store
    createUser(firebaseUser, res) {
        db.collection('users').doc(firebaseUser.nibiId).set(firebaseUser).then(result => {
            if (result) {
                console.log("User data saved to fire store");
            } else {
                console.log("Error in store user data to firebase");
            }
        });
    },
    // This function used for create previous user that already saved in mongo but not in firebase cloud
    createPreUser(user, res) {
        db.collection('users').doc(user.nibiId).set(user).then(result => {
            if (result) {
                console.log("User data saved to fire store");
            } else {
                console.log("Error in store user data to firebase");
            }
        });
    },
    // This function is used to when a user login his/her current status changed to online
    updateUser(nibiId) {
        db.collection('users').doc(nibiId).update({
            'userCurrentStatus': 'online',
            'updatedAt': new Date
        }).then(updatedResult => {
            console.log("User status changed to online");
        });
    },
    // This function is used to when a user login his/her current status changed to offline
    offlineUser(nibiId) {
        db.collection('users').doc(nibiId).update({
            'userCurrentStatus': 'offline'
        }).then(updatedResult => {
            console.log("firebase user offline successfully.")
        });
    },

};

// This cron is for add new field in user document
// cron.schedule("*/1 * * * *", function (){
//     db.collection('users').get().then(function (allUser) {
//
//         let user = allUser.docs.map(doc => {
//
//             db.collection('users').doc(doc.data().nibiId).update({
//                 'unreadNotificationCount': 0,
//                 'unreadChatCount': 0
//             })
//                 .then(updatedResult => {
//                     console.log("User status changed to online");
//                 });
//         });
//     });
// });

// cron.schedule("*/10 * * * * *", function() {
//     db.collection('users').get().then(function(allUser){
//
//         let user = allUser.docs.map(doc => {
//             let currentTime = moment(admin.firestore.Timestamp.now().toDate());
//             let userTime = moment((doc.data().updatedAt).toDate());
//             let timeDiff= currentTime.diff(userTime, 'seconds')
//             if(timeDiff > 120){
//                 db.collection('users').doc(doc.data().nibiId).update({'userCurrentStatus': 'offline'}).then(updatedResult =>{
//                  //   console.log("user current status changed to offline mode.")
//                 })
//             }
//             //  console.log(timeDiff);
//             //  return (doc.data().updatedAt).toDate()
//         });
//        // console.log(admin.firestore.Timestamp.now().toDate())
//     }).catch(function (error) {
//         console.log("Error fetching user data:", error);
//     });
// });



