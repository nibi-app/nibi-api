const sgMail = require('@sendgrid/mail');
const Env = require("../../env");
sgMail.setApiKey(Env.SENDGRID_API_KEY[Env.MODE]);
const {base64encode, base64decode} = require('nodejs-base64');

module.exports = {
  sendEmail(email, name, encodeNibiId){
    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'Password Reset.',
      templateId: 'd-ae955d81ad5a4798aafc9af111418941',
      dynamic_template_data: {
        "name": name,
        "link": Env.RESET_PASSWORD[Env.MODE]+encodeNibiId
      }
    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },

  joinNibiRequest(email, name){
    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'Request for join Nibi',
      templateId: 'd-8a5e324567534bf8b80a32602ef858ad',
      dynamic_template_data: {
        "link": Env.JOIN_NIBI[Env.MODE],
        "hostName": name
      }
    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },
  confirmationMailOnRegistration(email, response){
    let nibiId = base64encode(response.nibiId)

    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'Registration Successful.',
      templateId: "d-4af5d0774ec14ea8a8cded1572bfe25b",
      dynamic_template_data: {
        "name": response.fname,
        "confirmEmailLink": Env.EMAIL_CONFIRMATION[Env.MODE]+ nibiId
      }
    };


    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },


  resetPasswordMail(item){
    const msg = {
      to: item.email,
      from: 'info@nibi.io',
      subject: 'Account Update: Password Changed',
      templateId: "d-3179052a2fe44ea59052e0d007cfc208",
      dynamic_template_data: {
        "name": item.fname
      }
    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },

  pastMail(email, item){
    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'Your Nibi e-mail address has been updated',
      templateId: "d-b38b853ff2d14423bb836f7e31f182f4",
      dynamic_template_data: {
        "name": item.fname,
        "newEmail" : item.email
      }
    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },

  presentMail(email, item){
    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'Your Nibi e-mail address has been updated',
      templateId: "d-c4edf57d1cdc477f9f576394bb88048d",
      dynamic_template_data: {
        "name": item.fname,
        "newEmail" : email
      }
    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },

  cancelEventMail(email, item, host){
    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'Event has been cancelled.',
      templateId: "d-11827ac473624c0c9a407762ecfc9291",
      dynamic_template_data: {
        "eventName": item.title,
        "hostName": host.fname + " " + host.lname
      }

    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
      }else{
        console.log("Email send successfully.");
      }
    });
  },

  addCoHostMail(email, item, userTimeZone){
    const msg = {
      to: email,
      from: 'info@nibi.io',
      subject: 'You have added co-host.',
      templateId: "d-618226fee4fc493fa14d719ec4295db5",
      dynamic_template_data: {
        "eventName": item.title,
        "joinLink": item.joinLink,
        "hostName": userTimeZone.fname +" "+ userTimeZone.lname
      }
    };
    sgMail.send(msg, (error, result) => {
      if(error){
        console.log(" error in send mail");
        console.log(error.response.body.errors);
      }else{
        console.log("Email send successfully.");
      }
    });
  },

}