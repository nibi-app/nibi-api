const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const jwtConf = require('../config/globals/keyfile')
const secondaryServiceAccount = require('../../../../nibi-288314-firebase-adminsdk-4ztcy-ab23becd92.json');
const secondaryAppConfig = {
    credential: admin.credential.cert(secondaryServiceAccount),
};
const secondary = admin.initializeApp(secondaryAppConfig, 'secondary');
const db = secondary.firestore();

module.exports = async (req, res, next) => {

    if (req.headers.authorization || req.body.nibiId) {
        let nibiId ;
        if(req.headers.authorization){
            let token = req.headers.authorization
            const decoded = await jwt.verify(token, jwtConf.secret.d);
            req.decoded = decoded;
            nibiId = req.decoded.nibiId;
        }
        if(req.body.nibiId){
            nibiId = req.body.nibiId;
        }

        db.collection('users').where('nibiId', '==', nibiId).get().then(function (user) {
            if (user.docs.length > 0) {
                db.collection('users').doc(nibiId).update({'updatedAt': new Date}).then(updatedResult => {
                    //  console.log("firebase user current time updated successfully.");
                    User.findOne({nibiId: nibiId, isActive: false}, function(err, user) {
                        if(user){
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 403,
                                    message: "User currently inactive."
                                }
                            });
                            return true;
                        }else{
                            next();
                        }
                    });
                });
            } else {
                User.findOne({nibiId: nibiId, isActive: false}, function(err, user) {
                    if(user){
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 403,
                                message: "User currently inactive."
                            }
                        });
                        return true;
                    }else{
                        next();
                    }
                });
            }
        });
    } else {

        next();
    }

}