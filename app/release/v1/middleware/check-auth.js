const jwt = require('jsonwebtoken');
const jwtconf = require('../config/globals/keyfile');


module.exports = async (req, res, next) => {
    const token = req.headers.authorization && req.headers.authorization != '' ? req.headers.authorization : res.json({
        serverResponse:{
            isError: true,
            message: 'Please send Token',
            statusCode: 404,
        }

    });

    try {
        // console.log("printing fromn here ++++++++++++++++++++1")
        const decoded = await jwt.verify(token, jwtconf.secret.d);
        req.decoded = decoded;
     //  console.log(req.decoded);
        next();

    } catch (error) {
        /**
         * If error then check the type of error
         * if error type is TokenExpiredError : send new token
         * else if error type is JsonWebTokenError : send error
         */

        if (error.name.toString() == 'TokenExpiredError') {

            res.json({
                serverResponse: {
                    isError: true,
                    message: 'Session Expired',
                    statusCode: 403
                }

            })
        } else if (error.name.toString() == 'JsonWebTokenError') {

            res.json({
                serverResponse: {
                    isError: true,
                    message: 'Auth Failed',
                    statusCode: 500
                }

            });
        }else{
            res.json({
                serverResponse:{
                    isError: true,
                    message: 'Unexpected Token.',
                    statusCode: 500
                }
            });
        }


    }

}