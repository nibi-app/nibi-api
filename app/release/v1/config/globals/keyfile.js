module.exports ={
    'secret' : 
        {
            "kty": "RSA",
            "d": "FuO9lvUlK3DXxdGflGZ3hNHeHNZ70zSKlTvl5GH3ycQFW5F6eQ473FfsSqblyjvY5C0DCQjFr5ALKujtNEZG6Jsq4CIVXngOfZJl_1TSkyzLwMgAXNGSzj44AODIs4qUKNmauB3kaCccvphntotkBKDzo6y4jOtamBGLIlUpQNMATd2MgcqR1WLZuTzX40U8N1eQZkpC68pj456unEGQDXKW3mpZpTxAUFqLQWnbrdxi6Hx7ug1rH1bKmnq0rYJirTt5ycinyWjjck4LOs08tD6bOmEcnFlqyyeHJmpQrzOg4ydO69vdA4FvBVllXVp-GCHkrAtuhg71MqKEOcvOsQ",
            "e": "AQAB",
            "use": "enc",
            "kid": "MIO",
            "alg": "RS256",
            "n": "gASkWKuLFs7qQU78EW9JFWV6Ae9_k_9tZ01GTxUFMZaBPH1x4HgAlKPzpbYq7oPCp6ZS4zOpUcnK2Iv_EKLItA_7APBCbRcMI3-eg5Q0yBoxUhg7LcoS9X4Eb99aDzCJcnjCYtkUn9H3eS_lD9dVa9AeKYRD2RxYr2N4_AfEtSoJapJ5SrQsTHNqkOhEtWuHeFjJk77s8Ldbe4cFeugK7me8j2a1rcdF8LLWEX73iRLUPMLCY5oUO8ZmDQJaIwltWxcHHETyx4aNxq5LY-va7-9jAudAM-L8xe6MEwjpN8cz_D1FYqJvBnnTpmgBgNTC3xLpMTbiONV6eSWLJVxp1w"
        }
    
}