const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');



//server connection
 mongoose.connect('mongodb://nibi_app_usr:54J7CFJkDaq<xj)8@localhost/nibi_app',{
    useNewUrlParser:true,
    useUnifiedTopology: true
 });
// localhost connection
// mongoose.connect('mongodb://localhost:27017/nibi_app',{
//    useNewUrlParser:true,
//     useUnifiedTopology: true
// });
mongoose.connection.on('connected', function () {  
    console.log(' Mongoose default connection open to ' +  "localhost/nibi_app");
  });

  // If the connection throws an error
mongoose.connection.on('error',function (err) {  
    console.log('Mongoose default connection error: ' + err);
  });
  // When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
    console.log('Mongoose default connection disconnected'); 
  });

  // If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
      console.log('Mongoose default connection disconnected through app termination'); 
      process.exit(0); 
    }); 
  });



//aws connection
// mongoose.connect('mongodb://username:password@ip_address:port/db_name',{
//     useNewUrlParser:true
// })


const userRoute = require('./endpoints/user');
const eventRoute = require('./endpoints/event');
const gameRoute = require('./endpoints/game');
const categoryRoute = require('./endpoints/category');
const postRoute = require('./endpoints/post');
const paymentRoute = require('./endpoints/payment');
/**
 *  ROUTES -  ROOT LEVEL
 */
router.use("/user",userRoute);
router.use("/event",eventRoute);
router.use("/game",gameRoute);
router.use("/category",categoryRoute);
router.use("/post",postRoute);
router.use("/payment",paymentRoute);


module.exports = router
