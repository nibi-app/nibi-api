const express = require('express');
const router = express.Router();
const paymentController = require('../../controllers/payment');
router.use(express.json());
router.use(express.urlencoded({extended:true}))

router.post('/create_plan', paymentController.createPlan);
router.post('/plan_list', paymentController.planList);
router.post('/create_payment', paymentController.createPaymentRequest);
router.post('/save_transaction_data', paymentController.saveTransactionData);
router.post('/redeem_points', paymentController.redeemUserPoints);
router.post('/add_promo_points', paymentController.addPromoPoints);
router.post('/transaction_list', paymentController.transactionList);
module.exports = router;