const express = require('express');
const router = express.Router();
const updateUser = require('../../middleware/update_user');
const gameController = require('../../controllers/game');
router.use(express.json());
router.use(express.urlencoded({extended:true}))


router.post('/create_game', updateUser, gameController.createGame);
router.post('/category_wise_game_list', updateUser, gameController.categoryWiseGameList);
router.post('/specific_game', updateUser, gameController.specificGame);
router.post('/edit_game', updateUser, gameController.editGame);
router.post('/delete_game', updateUser, gameController.deleteGame);
router.post('/create_game_for_event', updateUser, gameController.createGameForEvent);
router.post('/event_wise_game_list', updateUser, gameController.eventWiseGameList);
router.post('/edit_event_game', updateUser, gameController.editEventGame);
router.post('/published_game', updateUser, gameController.publishedGame);
//router.post('/event_wise_game_list_for_user', updateUser, gameController.eventWiseGameListForUser);
router.post('/give_answer', updateUser, gameController.giveAnswer);
router.post('/delete_event_game', updateUser, gameController.deleteEventGame);
router.post('/game_wise_users_score', updateUser, gameController.gameWiseUsersScore);
router.post('/specific_user_score', updateUser, gameController.specificUserScore);
router.post('/save_app_settings_info', gameController.saveAppSettingsInfo);
router.post('/update_app_settings_info', gameController.updateAppSettingsInfo);
module.exports = router;