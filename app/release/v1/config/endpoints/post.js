const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth');
const updateUser = require('../../middleware/update_user');
const postController = require('../../controllers/post');
router.use(express.json());
router.use(express.urlencoded({extended:true}))


router.post('/vote_on_poll', updateUser, postController.voteOnPoll);

router.post('/create_post', updateUser, postController.createPost);
router.post('/reply_on_post', updateUser, postController.replyOnPost);
router.post('/send_reply_on_reply', updateUser, postController.sendReplyOnReply);
router.post('/specific_post', updateUser, checkAuth, postController.specificPost);
router.post('/event_wise_post', updateUser, postController.eventWisePost);
router.post('/remove_source_file', updateUser, postController.removeSourceFile);
router.post('/edit_post', updateUser, postController.editPost);
router.post('/edit_poll_option', updateUser, postController.editPollOption);
router.post('/get_poll_option', updateUser, postController.getPollOption);
router.post('/event_wise_media_list', updateUser, checkAuth, postController.eventWiseMediaList);
router.post('/like_dislike', updateUser, postController.likeDislike);
router.post('/like_dislike_on_reply', updateUser, postController.likeDislikeOnReply);
router.post('/like_dislike_reply_on_reply', updateUser, postController.likeDislikeReplyOnReply);
router.post('/delete_post', updateUser, postController.deletePost);
router.post('/notification_list', updateUser, postController.notificationList);
module.exports = router;