const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth');
const updateUser = require('../../middleware/update_user');
const eventController = require('../../controllers/event');
router.use(express.json());
router.use(express.urlencoded({extended:true}))


router.post('/create_event', updateUser, checkAuth, eventController.createEvent);
router.post('/join_code_availablity', updateUser, eventController.joinCodeAvailable);
router.post('/user_availablity', updateUser, eventController.userAvailable);
router.post('/cohost_availablity', updateUser, eventController.coHostAvailable);
router.post('/upload_image', updateUser, eventController.uploadImage);
router.post('/predefined_image', updateUser, eventController.predefinedImage);
router.post('/predefined_gif', updateUser, eventController.predefinedGif);
router.post('/event_list', updateUser, checkAuth, eventController.eventList);
router.post('/event_list_for_admin', eventController.eventListForAdmin);
router.post('/search_from_event_list', updateUser, checkAuth, eventController.searchFromEventList);
router.post('/update_event', updateUser, checkAuth, eventController.updateEvent);
router.post('/update_event_for_admin', eventController.updateEventForAdmin);
router.post('/event_from_join_code', updateUser, eventController.eventFromJoinCode);
router.post('/specific_event', updateUser, eventController.specificEvent);
router.post('/search_event', updateUser, eventController.searchEvent);
router.post('/remove_guest', eventController.removeGuestFromGuestList);
router.post('/remove_cohost', eventController.removeCOHost);
router.post('/join_event', updateUser, eventController.joinEvent);
router.post('/check_host_approval', updateUser, checkAuth, eventController.checkHosApproval);
router.post('/update_host_approval', updateUser, checkAuth, eventController.updateHosApprovalStatus);
router.get('/export_guest_list/:eventId', updateUser, eventController.exportGuestList);
router.post('/cancel_event', updateUser, eventController.cancelEvent);
router.post('/create_report', updateUser, eventController.createReport);
router.post('/find_report', updateUser, eventController.findReport);
router.post('/report_list', updateUser, eventController.reportList);
router.post('/add_guest', eventController.addGuest);
router.post('/upgrade_event', eventController.upgradeEvent);
router.post('/data_count', eventController.dataCount);
module.exports = router;