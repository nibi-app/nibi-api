const express = require('express');
const router = express.Router();
const updateUser = require('../../middleware/update_user');
const categoryController = require('../../controllers/category');
router.use(express.json());
router.use(express.urlencoded({extended:true}))


router.post('/create_category', updateUser, categoryController.createCategory);
router.post('/category_list', updateUser, categoryController.categoryList);
router.post('/specific_category', updateUser, categoryController.specificCategory);
router.post('/update_category', updateUser, categoryController.updateCategory);
module.exports = router;