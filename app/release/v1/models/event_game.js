const mongoose = require('mongoose');
const eventGameSchema = new mongoose.Schema({
    categoryId: {type: String, required: true},
    event: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Event"
    },
    gameId: {type: String},
    gameName: {type: String, default: ""},
    description: {type: String, default: ""},
    isEditable: {type: Boolean, default: false},
    isPublished: {type: Boolean, default: false},
    shortDescription: {type: String, default: ""},

    gameType: {type: String, required: true},
    rounds: [
        {
            questionId: {type: String},
            questionText: {type: String, required: true},
            titleImage: {type: String, default: ""},
            options: [{
                text: {type: String},
                order: {type: Number},
                isCorrect: {type: Boolean, default: false},
                imageUrl: {type: String, default: ""}
            }],
        }
    ],
    user: [
        {
            firstName: {type:String , default: ""},
            lastName: {type:String , default: ""},
            profilePic: {type:String , default: ""},
            nibiId: {type:String , default: ""},
            totalNumberOfQuestion: {type: Number, default: 0},   // how many question in a round
            numberOfCorrectAnswer: {type: Number, default: 0},   // how many question answered by the user
            totalScore: {type: Number, default: 0},            // total marks for all question
            score: {type: Number, default: 0},                  // marks obtained by the user
            currentRounds: {type:Number, default:0},                // which question last answered a user
            answeredAllQuestion: {type: Boolean, default: false}        // when a user give answer all question this key will be true
        }
    ],
    correctOptions: [],
    addedAt: {type: Date, default: Date.now}

});

const eventGame = mongoose.model("EventGame", eventGameSchema)
module.exports = eventGame