const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
    imageUrl: {type: String, default: ""}

});

const Image = mongoose.model("Image", imageSchema)
module.exports = Image