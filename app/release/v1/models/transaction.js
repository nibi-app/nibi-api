const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
        transactionId: {type: String, required: true},
        transactionType: {type: String, enum: ["onetime", "monthly", "points"], default: "onetime"},
        amount: {type: String, default: ""},
        currency: {type: String, default: ""},
        eventId: {type: String, required: true},
        user: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
        nibiId: {type: String, required: true},
        client: {type: String, enum: ["web", "android", "ios"], default: "web"},
        description: {type: String, default: ""},
    },
    {timestamps: true},
);

const Transaction = mongoose.model("Transaction", transactionSchema)
module.exports = Transaction