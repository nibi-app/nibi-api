const mongoose = require('mongoose');

const pollingSchema = new mongoose.Schema({
        postId: {type: String, default: ""},
        userId: {type: String, default: ""},
        nibiId: {type: String, default: ""},
        optionId: {type: String, default: ""},
        optionName: {type: String, default: ""},
        optionIndex: {type: Number, default: 0}
    },
    {timestamps: true},
);

const polling = mongoose.model("polling", pollingSchema)
module.exports = polling