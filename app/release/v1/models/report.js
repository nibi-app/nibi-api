const mongoose = require('mongoose');

const reportSchema = new mongoose.Schema({
    whoReported: {type: String, required: true},
    reportedPersonName: {type: String},
    whoseAgainst: {type: String, required: true},
    reportAgainstPersonName: {type: String},
    comment: {type: String},
    cause: {type: String},
    type: {
        type: String,
        enum: ['person', 'chatroom'],
        default: 'person'
    },
    event: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Event"
    },
    addedAt: {type: Date, default: Date.now}
});

const Report = mongoose.model("Report", reportSchema)
module.exports = Report