const mongoose = require('mongoose');

const gifSchema = new mongoose.Schema({
    sourceUrl: {type: String, default: ""}

});

const Gif = mongoose.model("Gif", gifSchema)
module.exports = Gif