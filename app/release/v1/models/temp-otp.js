const mongoose = require('mongoose');
const tempOtpSchema = new mongoose.Schema({
   // _id : mongoose.Schema.Types.ObjectId,
    mobile:String,
    otp:String
},
{
    timestamps: true 
})
const tempOtpContainer = mongoose.model("tempOtpContainer", tempOtpSchema, "tempOtpContainer")
module.exports = tempOtpContainer