const mongoose = require('mongoose');
const gameSchema = new mongoose.Schema({
    // categoryId: {type: String, required: true},
    // gameName: {type: String, Default: ""},
    // description: {type: String},
    // isEditable: {type: Boolean, Default: false},
    // shortDescription: {type: String, Default: ""},
    // questionId: {type: String},
    // questionText: {type: String, required: true},
    // titleImage: {type: String, default: ""},
    // gameType: {type: String, required: true},
    // options: [{
    //     text: {type: String},
    //     order: {type: Number},
    //     isCorrect: {type: Boolean, default: false},
    //     imageUrl: {type: String, default: ""}
    // }],
    //  correctOptions: [],
    // addedAt: {type: Date, default: Date.now}




    categoryId: {type: String, required: true},
    gameName: {type: String, default: ""},
    description: {type: String, default: ""},
    isEditable: {type: Boolean, default: false},
    shortDescription: {type: String, default: ""},

    gameType: {type: String, required: true},
    rounds:[
        {
            questionId: {type: String},
            questionText: {type: String, required: true},
            titleImage: {type: String, default: ""},
            options: [{
                text: {type: String},
                order: {type: Number},
                isCorrect: {type: Boolean, default: false},
                imageUrl: {type: String, default: ""}
            }],
        }
    ],

    correctOptions: [],
    addedAt: {type: Date, default: Date.now}

});

const Game = mongoose.model("Game", gameSchema)
module.exports = Game