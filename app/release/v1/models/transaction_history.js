const mongoose = require('mongoose');

const transactionHistorySchema = new mongoose.Schema({
    transactionId: {type: String, required: true},
    transactionDate: {type: Date, default: Date.now},
    transactionStatus: {type: String, enum: ["success", "failure"], default: "success"},
    transactionAmount: {type: Number, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
    event: {type: mongoose.Schema.Types.ObjectId, ref: "Event"},
    transactionType: {type: String, enum: ["premium", "onetime"], default: "premium"},
    paymentThrough: {type: String, enum: ["debit_card", "credit_card", "upi"], default: "debit_card"},
    createdAt: {type: Date, default: Date.now},
});

const TransactionHistory = mongoose.model("TransactionHistory", transactionHistorySchema)
module.exports = TransactionHistory