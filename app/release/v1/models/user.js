const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const userSchema = new mongoose.Schema({
        //  _id: mongoose.Schema.Types.ObjectId,
        fname: {
            type: String,
            default: ""
        },
        lname: {
            type: String,
            default: ""
        },
        email: {
            type: String
        },
        isEmailVerified: {type: Boolean, default: false},
        mobile: {
            type: String,
            default: "",
        },
        countryCode: {
            type: String,
            default: "",
        },
        password: {
            type: String,
        },
        isEmail: {type: Boolean, default: false},
        isFacebook: {type: Boolean, default: false},
        isApple: {type: Boolean, default: false},
        isGoogle: {type: Boolean, default: false},
        authToken: {type: String, default: ""},
        facebookId: {type: String, default: ""},
        googleId: {type: String, default: ""},
        appleId: {type: String, default: ""},
        fcmToken: {type: String, default: ""},
        deviceType: {type: String, default: ""},
        otherSocialMedia: [],
        userType: {
            type: String,
            enum: ['VISITORS', 'USER'],
            default: 'USER'
        },
        isActive: {
            type: Boolean,
            enum: [true, false],
            default: true
        },
        isApproved: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isLoggedIn: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isNotify: {                         // User wants push notification or not
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isNewNotification: {             // In app notification user view the notification or not
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isNewMessage: {             // In chat message if new message arrived then this key will be true
            type: Boolean,
            enum: [true, false],
            default: false
        },
        privateMessage: {              // If user set private message then other user send him message privately
            type: Boolean,
            enum: [true, false],
            default: false
        },
        appearedOffline: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isMobileVerified: {                 // user verified his mobile number
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isScreenShow: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isTwoFactorAuthenticate: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        accountType: {
            type: String,
            enum: ['premium', 'free'],
            default: 'free'
        },
        subscriptionStartDate: {
            type: Date
        },
        subscriptionEndDate: {
            type: Date
        },
        refundPoints: {
            type: Number,
            default: 0
        },
        profileCreatedAt: {
            type: Date,
            default: Date.now
        },
        profilePic: {type: String, default: ""},
        nibiId: {
            type: String
        },
        timeZone: {
            type: String
        },
        blockedUsers: [
            {
                firstName: {type: String, default: ""},
                lastName: {type: String, default: ""},
                profilePic: {type: String, default: ""},
                nibiId: {type: String, default: ""}
            }
        ],
        blockedMeByOthers: [
            {
                firstName: {type: String, default: ""},
                lastName: {type: String, default: ""},
                profilePic: {type: String, default: ""},
                nibiId: {type: String, default: ""}
            }
        ]
    },
    {
        timestamps: true, // add created_at , updated_at at the time of insert/update
        versionKey: false
    });

userSchema.pre('save', async function (next) {
    console.log("enter pre save");
    let user = this ;
    if (user.password) {
        if (this.isModified('password') || this.isNew) {

            try {
                let salt = await bcrypt.genSalt(saltRounds);
                let hashed_password = await bcrypt.hash(user.password, salt)
                user.password = hashed_password;
                user._id = new mongoose.Types.ObjectId();
                return next();
            } catch (error) {
                return next(error);
            }

        } else {
            return next();
        }
    } else {
        return next();
    }
})

userSchema.pre('updateOne', async function(next) {
    let user = this;
    if (user._update.password) {
            try {
                let salt = await bcrypt.genSalt(saltRounds);
                let hashed_password = await bcrypt.hash(user._update.password, salt)
               user._update.password = hashed_password;
                return next();
            } catch (error) {
                return next(error);
            }
    } else {
        return next();
    }
});

userSchema.methods.comparePassword = function (pw, cb) {
    bcrypt.compare(pw, this.password, function (err, isMatched) {
        if (err) return cb({"err from here": err})
        cb(null, isMatched)
    })
}

userSchema.methods.getPublicFields = function () {
    var returnObject = {
        _id: this._id,
        fname: this.fname,
        lname: this.lname,
        email: this.email,
        userType: this.userType,
        profilePic: this.profilePic,
        nibiId: this.nibiId,
        profileCreatedAt: this.profileCreatedAt,
        createdAt: this.createdAt,
        updatedAt: this.updatedAt,
        mobile: this.mobile,
        countryCode: this.countryCode,
        isTwoFactorAuthenticate: this.isTwoFactorAuthenticate,
        isEmail: this.isEmail,
        isFacebook: this.isFacebook,
        isApple: this.isApple,
        isGoogle: this.isGoogle,
        isNotify: this.isNotify,
        privateMessage: this.privateMessage,
        appearedOffline: this.appearedOffline,
        isNewNotification: this.isNewNotification
    };
    return returnObject;
};


const User = mongoose.model("User", userSchema)

module.exports = User