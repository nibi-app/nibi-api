const mongoose = require('mongoose');

const appSettingsSchema = new mongoose.Schema({
    maximumQuestionAllowed: {type: Number, default: 0}

});

const AppSettings = mongoose.model("AppSettings", appSettingsSchema)
module.exports = AppSettings