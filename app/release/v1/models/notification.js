const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    senderNibiId: {type: String},
    senderFirstName: {type: String, default: ""},
    senderLastName: {type: String, default: ""},
    senderImage: {type: String, default: ""},
    purpose: {type: String, default: ""},
    text: {type: String, default:""},
    eventId: {type: String, default:""},
    eventName: {type: String, default:""},
    postId: {type: String, default:""},
    notificationType: {type: String, default:""},
    receiver: [{
        nibiId: {type: String, default: ""},
        firstName: {type:String, default:""},
        lastName: {type:String, default:""},
        imageUrl: {type:String, default:""},
    }],
    mention: [{
        name: {type: String, Default: ""},
        nibiId: {type: String, Default: ""}
    }],
    isRead: {type: Boolean, default: false},
    addedAt: {type: Date, default: Date.now}
});

const Notification = mongoose.model("Notification", notificationSchema)
module.exports = Notification