const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
        nibiId: {type: String},
        title: {type: String, default: ""},
        joinCode: {type: String, required: true},
        joinLink: {type: String, required: true},
        description: {type: String, default: ""},
        backgroundImage: {type: String, default: ""},
        createdDate: {type: Date, default: new Date()},
        eventDate: {type: Date, required: true},
        startTime: {type: String, default: ""},
        endTime: {type: String, default: ""},
        eventStartAt: {type: Date},
        eventEndAt: {type: Date},
        joinBefore: {type: Date},
        location: {type: String, default: ""},
        latitude : {type: String, default: ""},
        longitude : {type: String, default: ""},
        virtualEventDetails: {type: String, default: ""},
        numberOfGuests: {type: Number, default: 0},
        hasCoHosts: {type: Boolean, enum: [true, false], default: false},
        isCancel: {type: Boolean, enum: [true, false], default: false},
        cancelReason: {type: String, default: ""},
        coHosts: [{
            nibiId: {type:String},
            fname: {type:String, default:""},
            lname: {type:String, default:""},
            profilePic: {type:String, default:""},
            email: {type:String, default:""},
        }],
        guestList: [{
            nibiId: {type: String, default: ""},
            firstName: {type:String, default:""},
            lastName: {type:String, default:""},
            email: {type:String, default:""},
            imageUrl: {type:String, default:""},
            role: {type:String, default: ""},
            hostApproval: {type:Boolean, default: false},
            privateMessage: {type: Boolean, enum: [true, false], default: false}
        }],
        removedGuest: [{
            nibiId: {type: String, default: ""},
            firstName: {type:String, default:""},
            lastName: {type:String, default:""},
            email: {type:String, default:""},
            imageUrl: {type:String, default:""},
            role: {type:String, default: ""},
            hostApproval: {type:Boolean, default: false},
            privateMessage: {type: Boolean, enum: [true, false], default: false}
        }],
        isGuestsAdmit: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isAnnounceOnEntry: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        isSearchable: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        hasPhotoAlbum: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        canGuestsMessage: {
            type: Boolean,
            enum: [true, false],
            default: false
        },
        type: {
            type: String,
            enum: ["free", "paid"],
            default: "free"
        },
        transactionType: {
            type: String,
            enum: ["free", "onetime", "premium"],
            default: "free"
        },
        transactionId: {
            type: String,
            default: ""
        },
        amount: {
            type: Number,
            default: 0
        }
    },
    {
        timestamps: true, // add created_at , updated_at at the time of insert/update
        versionKey: false
    });

const Event = mongoose.model("Event", eventSchema)
module.exports = Event