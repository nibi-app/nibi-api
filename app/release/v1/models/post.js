const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    eventId: {type: String, required: true},
    comment: {type: String},
    nibiId: {type: String},
    hostNibiId: {type: String},
    coHosts: [
        {
            nibiId: {type: String},
        }
    ],
    postType: {type: String, enum: ['media', 'poll', 'join', 'game'], default: ""},
    selectMultipleOption: {type: Boolean, enum: [true, false], default: false},
    viewResultToAllGuest: {type: Boolean, default: false},
    questionsForPoll: {type: String, default: ""},
    options: [{
        optionName: {type: String, default: ""},
        optionIndex: {type: Number, default: 0}
    }],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    sourceFile: [{
        sourceUrl: {type: String, default: ""},
        sourceType: {type: String, default: ""},
    }],
    mention: [{
        name: {type: String, Default: ""},
        nibiId: {type: String, Default: ""}
    }],
    reply: [{
        nibiId: {type: String},
        replyText: {type: String, default: ""},
        firstName: {type: String, default: ""},
        lastName: {type: String, default: ""},
        userImage: {type: String, default: ""},
        imageUrl: {type: String, default: ""},
        createdAt: {type: Date, default: Date.now},
        mentionOnReply: [{
            name: {type: String, Default: ""},
            nibiId: {type: String, Default: ""}
        }],
        likeOnReply: [{
            nibiId: {type: String, Default: ""}
        }],
        replyOnReply: [
            {
                nibiId: {type: String},
                replyText: {type: String, default: ""},
                firstName: {type: String, default: ""},
                lastName: {type: String, default: ""},
                userImage: {type: String, default: ""},
                imageUrl: {type: String, default: ""},
                createdAt: {type: Date, default: Date.now},
                likeOnReplyOnReply: [{
                    nibiId: {type: String, Default: ""}
                }],
                mentionOnReplyOnReply: [{
                    name: {type: String, Default: ""},
                    nibiId: {type: String, Default: ""}
                }],
            }],
    }],
    like: [{
        nibiId: {type: String, Default: ""}
    }],
    userScore: {
        totalScore: {type: Number, default: 0},    // total marks for all question
        score: {type: Number, default: 0}                  // marks obtained by the user
    },
    addedAt: {type: Date, default: Date.now},
    isDeleted: {type: Boolean, default: false}
});

const Post = mongoose.model("Post", postSchema)
module.exports = Post