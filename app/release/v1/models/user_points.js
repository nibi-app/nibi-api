const mongoose = require('mongoose');

const userPointsSchema = new mongoose.Schema({
        eventId: {type: String, default: ""},
        nibiId: {type: String, required: true},
        status: {type: String, enum: ["added", "subtracted"], default: "added"},
        points: {type: Number, default: 0},
        client: {type: String, enum: ["web", "android", "ios"], default: "web"},
    },
    {timestamps: true},
);

const UserPoints = mongoose.model("UserPoints", userPointsSchema)
module.exports = UserPoints