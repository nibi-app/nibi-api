const mongoose = require('mongoose');

const planSchema = new mongoose.Schema({
        planId: {type: String, default: ""},
        planName: {type: String, default: ""},
        amount: {type: Number, default: 0},
        currency: {type: String, default: ""},
        client: {type: String, enum: ["web", "android", "ios"], default: "web"},
        transactionType: {type: String, enum: ["onetime", "monthly"], default: "onetime"},
        description: {type: String, default: ""},
        features: []
    },
    {timestamps: true},
);

const Plan = mongoose.model("Plan", planSchema)
module.exports = Plan