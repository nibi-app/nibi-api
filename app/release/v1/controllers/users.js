const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const Env = require("../../../../env");
const Admin = require("../models/admin");
const TempOtp = require("../models/temp-otp");
const Event = require("../models/event");
const EventGame = require("../models/event_game");
const Post = require("../models/post");
const Notification = require("../models/notification");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const mailerController = require("../../../services/mailer");
const NotificationService = require("../../../services/notification");
const smsController = require("../../../services/sms");
const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const {base64encode, base64decode} = require('nodejs-base64');


module.exports = {

    sendOtp: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        try {
            let userData = {
                nibiId: req.decoded.nibiId
            }
            User.findOne(userData, function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (!item.mobile) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "We are unable to send a text message to this phone number. Please ensure you can receive text messages and try again.",
                                statusCode: 404
                            }
                        });
                    } else {
                        let otp =
                            (Math.floor(1000 + Math.random() * 9000));
                        let newOtp = new TempOtp({
                            mobile: item.mobile,
                            otp: otp
                        });
                        TempOtp.deleteMany({mobile: item.mobile}, function (err, deleteManyResponse) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + ": Error in delete many OTP.",
                                        statusCode: 500
                                    }
                                });
                            } else {
                                newOtp.save(function (err, response) {
                                    if (response) {
                                        let smsDetails = {
                                            body: "Your Nibi Verification Code is: " + otp,
                                            to: item.countryCode + item.mobile
                                        };
                                        smsController.sendMsg(smsDetails);

                                        // if (err) {
                                        //     res.json({
                                        //         serverResponse: {
                                        //             isError: true,
                                        //             message:"Error in send otp.",
                                        //             statusCode: 500
                                        //         }
                                        //     });
                                        //    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: "Code sent successfully.",
                                                statusCode: 200
                                            },
                                            result: {
                                                otp: otp,
                                            }
                                        });
                                        //  }
                                        //   });
                                    }
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "-Error: " + err,
                                                statusCode: 500
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });

        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    verifyOtp: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let otp = req.body.otp
            ? req.body.otp
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide OTP."
                }
            });
        if (!req.body.otp) {
            return true;
        }
        try {
            let userData = {
                nibiId: req.decoded.nibiId
            }
            User.findOne(userData).select('nibiId email profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, userDetails) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (userDetails === null) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["404"],
                                statusCode: 404
                            }
                        });
                    } else {
                        let mobile = {mobile: userDetails.mobile}
                        TempOtp.findOne(mobile, function (err, item) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {
                                if (!item) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["404"],
                                            statusCode: 404
                                        }
                                    });
                                } else {
                                    if (item.otp === otp) {
                                        TempOtp.deleteOne(mobile, function (err, deleteResult) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + ": Error in delete OTP",
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {
                                                let updateValues = {
                                                    $set: {
                                                        isMobileVerified: true,
                                                        isTwoFactorAuthenticate: true
                                                    }
                                                };
                                                User.updateOne(
                                                    userData,
                                                    updateValues,
                                                    function (err, item) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                }
                                                            });
                                                        } else {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: "Code verified successfully.",
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    userDetails: userDetails
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        })
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: "The code you entered does not match the code we sent. Please try again.",
                                                statusCode: 404
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    manualSignUp: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let fname = req.body.fname
            ? req.body.fname
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send First Name."
                }

            });
        let lname = req.body.lname
            ? req.body.lname
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send Last Name."
                }
            });
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send Email Id."
                }
            });
        let password = req.body.password
            ? req.body.password
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send Password."
                }
            });
        if (!req.body.fname || !req.body.lname || !req.body.email || !req.body.password) {
            return true;
        }
        let findUser = {
            email: email
        };
        try {
            await User.findOne(findUser, function (err, emailExist) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (emailExist) {
                        if (emailExist.email === req.body.email && emailExist.userType === 'VISITORS') {
                            let updateValues = {
                                password: password,
                                isEmail: true,
                                userType: 'USER'
                            };
                            User.updateOne(
                                {nibiId: emailExist.nibiId},
                                updateValues,
                                function (err, updatedItem) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            }
                                        });
                                    } else {
                                        User.findOne(findUser).select('-password -address -isEmailVerified -authToken -facebookId -googleId -appleId -fcmToken -deviceType -otherSocialMedia -isActive -isApproved -isLoggedIn -isMobileVerified -isScreenShow -blockedUsers -blockedMeByOthers').exec(function (err, user) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        userDetails: user
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "Email Already in Use.",
                                    statusCode: 404
                                }
                            });
                        }
                    } else {
                        let nibiId;
                        if (Env.MODE == "development") {
                            nibiId = "DEV-NIBI" + new Date().getTime();
                        } else {
                            nibiId = "NIBI" + new Date().getTime();
                        }
                        let newUser = new User({
                            fname: fname,
                            lname: lname,
                            email: email,
                            password: password,
                            nibiId: nibiId,
                            isEmail: true
                        });

                        newUser.save(function (err, item) {
                            if (item) {
                                let response = item.getPublicFields();
                                mailerController.confirmationMailOnRegistration(email, response);
                                let firebaseUser = item.getPublicFields();
                                delete firebaseUser._id;
                                firebaseUser.userCurrentStatus = "offline";
                                firebaseUser.unreadNotificationCount = 0;
                                firebaseUser.unreadChatCount = 0;
                                NotificationService.createUser(firebaseUser, res);
                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: "Account successfully created.",
                                        statusCode: 200
                                    },
                                    result: {
                                        userDetails: response
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                })
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    createVisitors: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let fname = req.body.fname
            ? req.body.fname
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send First Name."
                }

            });
        let lname = req.body.lname
            ? req.body.lname
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send Last Name."
                }
            });
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send Email Id."
                }
            });
        let timeZone = req.body.timeZone
            ? req.body.timeZone
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Send Timezone."
                }
            });
        if (!req.body.fname || !req.body.lname || !req.body.email || !req.body.timeZone) {
            return true;
        }
        let findUser = {
            email: email
        };
        try {
            if(req.body.appVersion && Env.APP_VERSION[Env.MODE] == req.body.appVersion){
            User.findOne(findUser).select('-password -address -isEmailVerified -authToken -facebookId -googleId -appleId -fcmToken -deviceType -otherSocialMedia -isActive -isApproved -isLoggedIn -isMobileVerified -isScreenShow  -blockedUsers -blockedMeByOthers').exec(function (err, emailExist) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (emailExist) {
                        if (emailExist.email === email && emailExist.userType === 'USER') {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "Email Already in Use.",
                                    statusCode: 404
                                }
                            });
                        } else {
                            if (emailExist.email === email && emailExist.userType === 'VISITORS') {
                                user = JSON.parse(JSON.stringify(emailExist));
                                let token = jwt.sign(user, config.secret.d, {});
                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: errorMsgJSON[lang]["200"],
                                        statusCode: 200
                                    },
                                    result: {
                                        userDetails: emailExist,
                                        token: token
                                    }
                                });
                            } else {
                                let nibiId;
                                if (Env.MODE == "development") {
                                    nibiId = "DEV-NIBI" + new Date().getTime();
                                } else {
                                    nibiId = "NIBI" + new Date().getTime();
                                }
                                let newUser = new User({
                                    fname: fname,
                                    lname: lname,
                                    email: email,
                                    nibiId: nibiId,
                                    userType: 'VISITORS',
                                    timeZone: timeZone
                                    //  isEmail: true
                                });

                                newUser.save(function (err, item) {
                                    if (item) {
                                        let response = item.getPublicFields();
                                        user = JSON.parse(JSON.stringify(response));
                                        let token = jwt.sign(user, config.secret.d, {});
                                        let firebaseUser = item.getPublicFields();
                                        delete firebaseUser._id;
                                        firebaseUser.userCurrentStatus = "offline";
                                        firebaseUser.unreadNotificationCount = 0;
                                        firebaseUser.unreadChatCount = 0;
                                        NotificationService.createUser(firebaseUser, res);
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: "Account successfully created.",
                                                statusCode: 200
                                            },
                                            result: {
                                                userDetails: response,
                                                token: token
                                            }
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    } else {
                        let nibiId;
                        if (Env.MODE == "development") {
                            nibiId = "DEV-NIBI" + new Date().getTime();
                        } else {
                            nibiId = "NIBI" + new Date().getTime();
                        }
                        let newUser = new User({
                            fname: fname,
                            lname: lname,
                            email: email,
                            nibiId: nibiId,
                            userType: 'VISITORS',
                            timeZone: timeZone
                            //  isEmail: true
                        });

                        newUser.save(function (err, item) {
                            if (item) {
                                let response = item.getPublicFields();
                                user = JSON.parse(JSON.stringify(response));
                                let token = jwt.sign(user, config.secret.d, {});
                                let firebaseUser = item.getPublicFields();
                                delete firebaseUser._id;
                                firebaseUser.userCurrentStatus = "offline";
                                firebaseUser.unreadNotificationCount = 0;
                                firebaseUser.unreadChatCount = 0;
                                NotificationService.createUser(firebaseUser, res);
                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: "Account successfully created.",
                                        statusCode: 200
                                    },
                                    result: {
                                        userDetails: response,
                                        token: token
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                })
                            }
                        });
                    }
                }
            });
            }else{
                res.json({
                    serverResponse: {
                        isError: true,
                        message: 'You are using an old version application. Please upgrade your application to continue.',
                        statusCode: 500
                    },
                });
            }
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    manualLogin: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Email."
                }
            });
        let password = req.body.password
            ? req.body.password
            : res.json({
                ServerResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Password."
                }
            });
        let timeZone = req.body.timeZone
            ? req.body.timeZone
            : res.json({
                ServerResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Timezone."
                }
            });
        if (!req.body.email || !req.body.password || !req.body.timeZone) {
            return true;
        }
        let findUserQry = {
            email: email
        };
        if(req.body.appVersion && Env.APP_VERSION[Env.MODE] == req.body.appVersion){
            User.findOne(findUserQry).select('nibiId email password profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (!item) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "There was a problem with your e-mail or password. Please try again.",
                                statusCode: 404
                            }
                        });
                    } else {
                        item.comparePassword(password, async function (err, isMatch) {
                            if (isMatch && !err) {
                                try {
                                    user = JSON.parse(JSON.stringify(item));
                                    delete user.password;
                                    let token = await jwt.sign(user, config.secret.d, {});

                                    //Update user isLoggedIn status to true
                                    req.body.isLoggedIn = true;
                                    delete req.body.email;
                                    delete req.body.password;
                                    let updateValues = {
                                        $set: {
                                            ...req.body
                                        }
                                    };
                                    User.updateOne(
                                        findUserQry,
                                        updateValues,
                                        function (err, item) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {
                                                if (user.appearedOffline == false) {
                                                    let nibiId = user.nibiId
                                                    NotificationService.updateUser(nibiId);
                                                }
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        userDetails: user,
                                                        token: token
                                                    }
                                                });
                                            }
                                        });
                                } catch (e) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                                            statusCode: 500
                                        }
                                    });
                                }
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "There was a problem with your e-mail or password. Please try again.",
                                        statusCode: 404
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }else{
            res.json({
                serverResponse: {
                    isError: true,
                    message: 'You are using an old version application. Please upgrade your application to continue.',
                    statusCode: 500
                },
            });
        }

    },
    specificUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let findUserQry = {
            nibiId: req.decoded.nibiId
        };
        User.findOne(findUserQry).select('nibiId profilePic fname lname profileCreatedAt createdAt updatedAt mobile userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType isActive').exec(function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            userDetails: item
                        }
                    });
                }
            }
        });
    },
    specificUserForAdmin: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Nibi Id."
                }
            });
        let findUserQry = {
            nibiId: nibiId
        };
        User.findOne(findUserQry).select('-password').exec(function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            userDetails: item
                        }
                    });
                }
            }
        });
    },
    // This API for get all user from db and store them in fire store
    allUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        User.find().select('nibiId profilePic fname lname profileCreatedAt createdAt updatedAt mobile userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType isActive').exec(function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {

                item.forEach(allUser => {
                    let user = allUser.toJSON();
                    user.userCurrentStatus = "offline";
                    delete user._id
                    NotificationService.createPreUser(user, res);
                });
                res.json({
                    serverResponse: {
                        isError: false,
                        message: errorMsgJSON[lang]["200"],
                        statusCode: 200
                    },
                    result: {
                        userDetails: item
                    }
                });

            }
        });
    },
    userListForAdmin: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let limit = 10;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let page = 1;
        page = (page - 1) * limit;
        if (req.body.page) {
            page = (req.body.page - 1) * limit;
        }
        User.find({}).countDocuments().exec(function (err, userCount) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                User.find({}).select('-password').limit(limit).skip(page).sort({'createdAt': 'desc'}).exec(function (err, user) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                userCount: userCount,
                                userList: user
                            },
                        });
                    }
                });
            }
        });
    },
    updateProfile: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId
        let query = {nibiId: nibiId};
        if (req.body.password) {
            let salt = await bcrypt.genSalt(saltRounds);
            let hashedNewPassword = await bcrypt.hash(req.body.password, salt);
            req.body.password = hashedNewPassword;
        }
        User.findOne(query, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {

                    if (req.body.mobile && req.body.mobile != "") {
                        let findByMobile = {
                            mobile: req.body.mobile,
                            nibiId: {$ne: nibiId}
                        }
                        User.findOne(findByMobile, function (err, item) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500,
                                    }
                                });
                            } else {
                                if (item) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: "This number is already registered to another account.",
                                            statusCode: 404
                                        }
                                    });
                                } else {
                                    let updateValues = {
                                        $set: {
                                            ...req.body
                                        }
                                    };
                                    User.updateOne(query, updateValues, function (err, res1) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500,
                                                }
                                            });
                                        } else {
                                            User.findOne(query).select('nibiId email profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, updatedItem) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        },
                                                    });
                                                } else {
                                                    // Update event guest name, image, notification sender name , image, blocked user name, image, unblocked user name iamge
                                                    if (req.body.fname || req.body.lname || req.body.profilePic) {
                                                        updateGuest(req, res, nibiId);
                                                        updateNotificationSender(req, res, nibiId);
                                                        updateBlockedUser(req, res, nibiId);
                                                        updateBlockedMeByOthersUser(req, res, nibiId);
                                                    }

                                                    if (req.body.privateMessage == true || req.body.privateMessage == false) {

                                                        Event.updateMany({'guestList.nibiId': nibiId}, {'guestList.$.privateMessage': req.body.privateMessage}, function (err, guestListUpdate) {
                                                            //  console.log(guestListUpdate)
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    }
                                                                });
                                                            } else {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: false,
                                                                        message: "Your profile has been updated successfully.",
                                                                        statusCode: 200
                                                                    },
                                                                    result: {
                                                                        userDetails: updatedItem
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: "Your profile has been updated successfully.",
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                userDetails: updatedItem
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });

                    } else {
                        let updateValues = {
                            $set: {
                                ...req.body
                            }
                        };
                        User.updateOne(query, updateValues, function (err, res1) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500,
                                    }
                                });
                            } else {
                                User.findOne(query).select('nibiId email profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, updatedItem) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        // Update event guest name, image, notification sender name , image, blocked user name, image, unblocked user name iamge
                                        if (req.body.fname || req.body.lname || req.body.profilePic) {
                                            updateGuest(req, res, nibiId);
                                            updateNotificationSender(req, res, nibiId);
                                            updateBlockedUser(req, res, nibiId);
                                            updateBlockedMeByOthersUser(req, res, nibiId);
                                        }
                                        if (req.body.privateMessage == true || req.body.privateMessage == false) {

                                            Event.updateMany({'guestList.nibiId': nibiId}, {'guestList.$.privateMessage': req.body.privateMessage}, function (err, guestListUpdate) {
                                                //  console.log(guestListUpdate)
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        }
                                                    });
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: false,
                                                            message: "Your profile has been updated successfully.",
                                                            statusCode: 200
                                                        },
                                                        result: {
                                                            userDetails: updatedItem
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    message: "Your profile has been updated successfully",
                                                    statusCode: 200
                                                },
                                                result: {
                                                    userDetails: updatedItem
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    },
    updateProfileForAdmin: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Nibi Id."
                }
            });
        if (!req.body.nibiId) {
            return true;
        }
        let findUser = {nibiId: nibiId};

        User.findOne(findUser, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    let updateValues = {
                        $set: {
                            ...req.body
                        }
                    };
                    User.updateOne(findUser, updateValues, function (err, res1) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            User.findOne(findUser).select('nibiId email profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, updatedItem) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    // Update event guest name, image, notification sender name , image, blocked user name, image, unblocked user name iamge
                                    if (req.body.fname || req.body.lname || req.body.profilePic) {
                                        updateGuest(req, res);
                                        updateNotificationSender(req, res);
                                        updateBlockedUser(req, res);
                                        updateBlockedMeByOthersUser(req, res);
                                    }
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: "Your profile has been updated successfully",
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: updatedItem
                                        }
                                    });

                                }
                            });
                        }
                    });

                }
            }
        });
    },
    updateEmail: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Email."
                }
            });
        if (!req.body.email) {
            return true;
        }
        let findEmail = {
            email: email,
            nibiId: {$ne: req.decoded.nibiId}
        };
        let query = {nibiId: req.decoded.nibiId};
        User.findOne(findEmail, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "Email Already in Use",
                            statusCode: 404
                        }
                    });
                } else {
                    let updateValues = {
                        $set: {
                            email: email
                        }
                    };
                    User.updateOne(query, updateValues, function (err, res1) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            updateGuestEmail(req, res, email);
                            User.findOne(query).select('nibiId email profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, updatedItem) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    mailerController.pastMail(req.decoded.email, updatedItem);
                                    mailerController.presentMail(updatedItem.email, updatedItem);
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: "Email updated successfully.",
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: updatedItem
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    forgotPassword: async (req, res, next) => {

        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Email."
                }
            });
        if (!req.body.email) {
            return true;
        }
        let query = {email: email};
        User.findOne(query, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "Email id not found.",
                            statusCode: 404
                        }
                    });
                } else {
                    let nibiId = item.nibiId;
                    let encodeNibiId = base64encode(nibiId);

                    let name = item.fname;

                    mailerController.sendEmail(item.email, name, encodeNibiId);
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: "Please check your email for instructions on resetting your password.",
                            statusCode: 200
                        },
                        result: {
                            nibiId: nibiId
                        }
                    });
                    // let randomNumber =
                    //     Math.floor(Math.random() * (999999 - 111111 + 1)) + 111111;
                    // console.log(randomNumber);
                    // var smsDetails = {
                    //     body: "Your temporary password is: " + randomNumber,
                    //     to: mobile,
                    //     from: 9800453236
                    // };
                    //     smsController.sendMsg(smsDetails, (err, data) => {

                    // if (err) {
                    //     res.json({
                    //         isError: true,
                    //         message: errorMsgJSON[lang]["400"],
                    //         statusCode: 400,
                    //         details: null
                    //     });
                    // } else {
                    // let normalPassword = JSON.stringify(randomNumber)
                    //  bcrypt.hash(normalPassword, saltRounds, function (err, hash) {
                    //      if (err) {
                    //          res.json({
                    //              isError: true,
                    //              message: errorMsgJSON[lang]["400"],
                    //              statusCode: 400,
                    //              details: err
                    //          });
                    //      } else {
                    //          var updateValues = {
                    //              $set: {
                    //                  password: hash
                    //              }
                    //          };
                    //          User.updateOne(query, updateValues, function (err, res1) {
                    //              if (err) {
                    //                  res.json({
                    //                      isError: true,
                    //                      message: errorMsgJSON[lang]["400"],
                    //                      statusCode: 400,
                    //                      details: null
                    //                  });
                    //              }
                    //              if (res1.nModified > 0) {
                    //                  res.json({
                    //                      isError: false,
                    //                      message: errorMsgJSON[lang]["202"],
                    //                      statusCode: 202,
                    //                      //    details: data
                    //                  });
                    //              }
                    //          });
                    //      }
                    //  });
                    //   }

                    //   });
                }
            }
        });
    },
    resetPassword: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
        let query = {nibiId: nibiId};
        let newPassword = req.body.newPassword
            ? req.body.newPassword
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please send new password."
                }
            });
        if (!req.body.newPassword) {
            return true;
        }
        let salt = await bcrypt.genSalt(saltRounds);
        let hashedNewPassword = await bcrypt.hash(newPassword, salt);
        User.findOne(query, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    let updateValues = {
                        $set: {
                            password: hashedNewPassword
                        }
                    };
                    User.updateOne(query, updateValues, function (err, res1) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            mailerController.resetPasswordMail(item);
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: "Password Updated Successfully.",
                                    statusCode: 200
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    updatePassword: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        let email = req.decoded.email;
        let password = req.body.currentPassword
            ? req.body.currentPassword
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please send current password."
                }
            });
        let newPassword = req.body.newPassword
            ? req.body.newPassword
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please send new password "
                }
            });
        if (!req.body.currentPassword || !req.body.newPassword) {
            return true;
        }
        let salt = await bcrypt.genSalt(saltRounds);
        let hashedNewPassword = await bcrypt.hash(newPassword, salt);

        let query = {email: email};
        User.findOne(query, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "There was a problem with your email or password.",
                            statusCode: 404
                        }
                    });
                } else {
                    bcrypt.compare(password, item.password, function (err, result) {
                        if (result === true) {
                            let updateValues = {
                                $set: {
                                    password: hashedNewPassword
                                }
                            };
                            User.updateOne(query, updateValues, function (err, res1) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500,
                                        }
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: "Password Updated Successfully.",
                                            statusCode: 200
                                        }
                                    });
                                }
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "There was a problem with your email or password.",
                                    statusCode: 404
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    socialLogin: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let socialId = req.body.socialId
            ? req.body.socialId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide social id."
                }
            });
        let registrationType = req.body.registrationType
            ? req.body.registrationType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide registration type."
                }
            });
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide email address."
                }
            });
        if (!req.body.socialId || !req.body.registrationType || !req.body.email) {
            return true;
        }
        let findUserQry = {};
        if (registrationType === "facebook") {
            findUserQry.facebookId = socialId
        }
        if (registrationType === "apple") {
            findUserQry.appleId = socialId
        }
        if (registrationType === "google") {
            findUserQry.googleId = socialId
        }

        let findEmail = {
            email: email
        }
        if(req.body.appVersion && Env.APP_VERSION[Env.MODE] == req.body.appVersion){
            User.findOne(findUserQry).select('nibiId mobile countryCode email password profilePic fname lname profileCreatedAt createdAt updatedAt userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500,
                        }
                    });
                } else {
                    if (item) {
                        user = JSON.parse(JSON.stringify(item));
                        let token = jwt.sign(user, config.secret.d, {});
                        //Update user isLoggedIn status to true
                        req.body.isLoggedIn = true;
                        delete req.body.socialId;
                        delete req.body.registrationType;
                        delete req.body.email;
                        let updateValues = {
                            $set: {
                                ...req.body
                            }
                        };
                        User.updateOne(
                            findUserQry,
                            updateValues,
                            function (err, item) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500,
                                        }
                                    });
                                } else {
                                    if (item.appearedOffline == false) {
                                        let nibiId = item.nibiId
                                        NotificationService.updateUser(nibiId);
                                    }
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: user,
                                            token: token
                                        }
                                    });
                                }
                            });
                    } else {

                        User.findOne(findEmail, function (err, emailExist) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500,
                                    }
                                });
                            } else {
                                if (!emailExist) {

                                    if (req.body.registrationType === "facebook") {
                                        req.body.isFacebook = true,
                                            req.body.facebookId = req.body.socialId
                                    }
                                    if (req.body.registrationType === "apple") {
                                        req.body.isApple = true,
                                            req.body.appleId = req.body.socialId
                                    }
                                    if (req.body.registrationType === "google") {
                                        req.body.isGoogle = true,
                                            req.body.googleId = req.body.socialId
                                    }
                                    let nibiId;
                                    if (Env.MODE == "development") {
                                        nibiId = "DEV-NIBI" + new Date().getTime();
                                    } else {
                                        nibiId = "NIBI" + new Date().getTime();
                                    }
                                    let newUser = new User({
                                        facebookId: req.body.facebookId,
                                        appleId: req.body.appleId,
                                        googleId: req.body.googleId,
                                        email: req.body.email,
                                        fname: req.body.fname,
                                        lname: req.body.lname,
                                        nibiId: nibiId,
                                        isFacebook: req.body.isFacebook,
                                        isGoogle: req.body.isGoogle,
                                        isApple: req.body.isApple,
                                        fcmToken: req.body.fcmToken,
                                        deviceType: req.body.deviceType,
                                        isLoggedIn: true
                                    });

                                    newUser.save(function (err, response) {
                                        //   if (response) {
                                        let userData = response.getPublicFields();
                                        let token = jwt.sign(userData, config.secret.d, {});
                                        let firebaseUser = response.getPublicFields();
                                        delete firebaseUser._id;
                                        firebaseUser.userCurrentStatus = "offline";
                                        firebaseUser.unreadNotificationCount = 0;
                                        firebaseUser.unreadChatCount = 0;
                                        NotificationService.createUser(firebaseUser, res);
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: "Account successfully created.",
                                                statusCode: 200
                                            },
                                            result: {
                                                userDetails: userData,
                                                token: token
                                            }
                                        });

                                    });
                                } else {
                                    if (emailExist.userType === 'VISITORS') {
                                        req.body.userType = 'USER';
                                    }
                                    if (req.body.registrationType === "facebook") {
                                        req.body.isFacebook = true,
                                            req.body.facebookId = req.body.socialId
                                    }
                                    if (req.body.registrationType === "apple") {
                                        req.body.isApple = true,
                                            req.body.appleId = req.body.socialId
                                    }
                                    if (req.body.registrationType === "google") {
                                        req.body.isGoogle = true,
                                            req.body.googleId = req.body.socialId
                                    }

                                    let updateValues = {
                                        $set: {
                                            ...req.body
                                        }
                                    };
                                    User.updateOne(
                                        findEmail,
                                        updateValues,
                                        function (err, item) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500,
                                                    }
                                                });
                                            } else {
                                                User.findOne(findEmail).select('nibiId mobile countryCode email password profilePic fname lname profileCreatedAt createdAt updatedAt userType isTwoFactorAuthenticate isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, updatedSocialInformation) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500,
                                                            }
                                                        });
                                                    } else {
                                                        if (updatedSocialInformation.appearedOffline == false) {
                                                            let nibiId = updatedSocialInformation.nibiId
                                                            NotificationService.updateUser(nibiId);
                                                        }
                                                        user = JSON.parse(JSON.stringify(updatedSocialInformation));
                                                        let token = jwt.sign(user, config.secret.d, {});
                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                userDetails: user,
                                                                token: token
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                }
                            }
                        });
                    }
                }
            });
        }else{
            res.json({
                serverResponse: {
                    isError: true,
                    message: 'You are using an old version application. Please upgrade your application to continue.',
                    statusCode: 500
                },
            });
        }
    },
    internalLogin: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let timeZone = req.body.timeZone
            ? req.body.timeZone
            : res.json({
                ServerResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide timezone."
                }
            });
        if (!req.body.timeZone) {
            return true;
        }
        let decoded = req.decoded;
        let findUserQry = {
            nibiId: decoded.nibiId
        }
        let updateValues = {
            $set: {
                ...req.body
            }
        };
        if(req.body.appVersion && Env.APP_VERSION[Env.MODE] == req.body.appVersion){
            try {
                User.updateOne(
                    findUserQry,
                    updateValues,
                    function (err, item) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            User.findOne(findUserQry).select('nibiId email profilePic fname lname profileCreatedAt createdAt updatedAt mobile countryCode userType isTwoFactorAuthenticate  isEmail isFacebook isApple isGoogle isNotify privateMessage appearedOffline isNewNotification isNewMessage accountType refundPoints isActive').exec(function (err, userDetails) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: userDetails
                                        }
                                    });
                                }
                            });
                        }
                    });
            } catch (e) {
                res.json({
                    saveResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                        statusCode: 500,
                    }

                });
            }
        }else{
            res.json({
                serverResponse: {
                    isError: true,
                    message: 'You are using an old version application. Please upgrade your application to continue.',
                    statusCode: 403
                },
            });
        }
    },
    logOut: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId

        let findUserQry = {
            nibiId: nibiId
        };
        User.updateOne(
            findUserQry,
            {
                isLoggedIn: false, fcmToken: ""
            },
            function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500,
                        }

                    });
                } else {
                    NotificationService.offlineUser(nibiId)
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        }
                    });
                }
            });
    },
    verifyEmail: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });
        if (!req.body.nibiId) {
            return true;
        }
        let findUserQry = {
            nibiId: nibiId
        };
        User.updateOne(
            findUserQry,
            {
                isEmailVerified: true
            },
            function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500,
                        }

                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: "Email verified successfully.",
                            statusCode: 200
                        }
                    });
                }
            });
    },
    adminSignUp: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please send email."
                }
            });
        let password = req.body.password
            ? req.body.password
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please send password."
                }
            });
        if (!req.body.email || !req.body.password) {
            return true;
        }
        let salt = await bcrypt.genSalt(saltRounds);
        let hashedPassword = await bcrypt.hash(password, salt);
        try {
            let newAdmin = new Admin({
                email: email,
                password: hashedPassword
            });

            newAdmin.save(function (err, item) {
                if (item) {

                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            userDetails: item
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    })
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    adminLogin: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide email."
                }
            });
        let password = req.body.password
            ? req.body.password
            : res.json({
                ServerResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide password."
                }
            });
        if (!req.body.email || !req.body.password) {
            return true;
        }
        let findAdminQry = {
            email: email
        };
        Admin.findOne(findAdminQry, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "There was a problem with your email or password.",
                            statusCode: 404
                        }
                    });
                } else {
                    bcrypt.compare(password, item.password, function (err, isMatch) {
                        if (isMatch && !err) {
                            let adminData = item.toJSON();
                            delete adminData.password;
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["200"],
                                    statusCode: 200
                                },
                                result: {
                                    adminData: adminData
                                }
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "There was a problem with your e-mail and/or password.",
                                    statusCode: 404
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    inactiveUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide email."
                }
            });
        if (!req.body.email ) {
            return true;
        }
        let findUserQry = {
            email: email
        };
        User.findOne(
            findUserQry,
            function (err, user) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500,
                        }
                    });
                } else {
                    if (user) {
                        if (req.body.isActive == false) {
                            console.log("enter here");
                            User.updateOne(
                                findUserQry, {isActive: false},
                                function (err, item) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500,
                                            }
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: errorMsgJSON[lang]["200"],
                                                statusCode: 200
                                            }
                                        });
                                    }
                                });
                        } else {
                            User.updateOne(
                                findUserQry, {isActive: true},
                                function (err, item) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500,
                                            }
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: errorMsgJSON[lang]["200"],
                                                statusCode: 200
                                            }
                                        });
                                    }
                                });
                        }
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: "User details not found."
                            }
                        });
                    }
                }
            });
    },
    searchEmail: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        let keywords = req.body.keywords
            ? req.body.keywords
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide search keywords."
                }
            });
        if (!req.body.keywords) {
            return true;
        }
        let findEmail = {
            $or: [{"email": {$regex: keywords, $options: "i"}}]
        };

        User.find(findEmail, function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                res.json({
                    serverResponse: {
                        isError: false,
                        message: errorMsgJSON[lang]["200"],
                        statusCode: 200
                    }, result: {
                        user: user
                    }
                });
            }
        });
    },
    blockUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let blockedBy = req.body.blockedBy
            ? req.body.blockedBy
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id who blocked."
                }
            });
        let blockedUserNibiId = req.body.blockedUserNibiId
            ? req.body.blockedUserNibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide blocked user nibi id."
                }
            });
        if (!req.body.blockedBy || !req.body.blockedUserNibiId) {
            return true;
        }
        let findUser = {
            nibiId: blockedBy
        };
        let updateQuery = {
            nibiId: blockedUserNibiId,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            profilePic: req.body.profilePic
        }
        let blockMeByOther = {
            nibiId: blockedBy,
            firstName: req.body.blockedByUserFirstName,
            lastName: req.body.blockedByUserLastName,
            profilePic: req.body.blockedByUserProfilePic
        }
        User.findOne(findUser, function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (user) {
                    User.updateOne(findUser,
                        {$push: {"blockedUsers": updateQuery}}, function (err, updatedData) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {
                                User.updateOne({nibiId: blockedUserNibiId},
                                    {$push: {"blockedMeByOthers": blockMeByOther}}, function (err, updatedData) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            User.findOne(findUser).select('blockedUsers blockedMeByOthers').exec(function (err, blockedUser) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        },
                                                    });
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: false,
                                                            message: errorMsgJSON[lang]["200"],
                                                            statusCode: 200
                                                        },
                                                        result: {
                                                            blockedUsers: blockedUser
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                            }
                        });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "User details not found."
                        }
                    });
                }
            }
        });
    },
    unblockUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let unblockedBy = req.body.unblockedBy
            ? req.body.unblockedBy
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id who unblocked."
                }
            });
        let unblockedUserNibiId = req.body.unblockedUserNibiId
            ? req.body.unblockedUserNibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide unblocked user nibi id."
                }
            });
        if (!req.body.unblockedBy || !req.body.unblockedUserNibiId) {
            return true;
        }
        let findUser = {
            nibiId: unblockedBy
        };

        User.findOne(findUser, function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (user) {
                    User.updateOne(findUser, {"$pull": {"blockedUsers": {"nibiId": unblockedUserNibiId}}}, {
                        safe: true,
                        multi: true
                    }, function (err, removeLike) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            User.updateOne({nibiId: unblockedUserNibiId}, {"$pull": {"blockedMeByOthers": {"nibiId": unblockedBy}}}, {
                                safe: true,
                                multi: true
                            }, function (err, removeLike) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    User.findOne(findUser).select('blockedUsers blockedMeByOthers').exec(function (err, blockedUser) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    message: errorMsgJSON[lang]["200"],
                                                    statusCode: 200
                                                },
                                                result: {
                                                    blockedUsers: blockedUser
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "User details not found."
                        }
                    });
                }
            }
        });
    },
    listOfBlockUsers: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });

        if (!req.body.nibiId) {
            return true;
        }
        let findUser = {
            nibiId: nibiId
        };

        User.findOne(findUser).select('blockedUsers blockedMeByOthers').exec(function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (user) {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            blockedUsers: user
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "User details not found."
                        }
                    });
                }
            }
        });
    },
    linkAccount: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });
        let linkedAccountId = req.body.linkedAccountId
            ? req.body.linkedAccountId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide linked account id."
                }
            });
        let linkedAccountType = req.body.linkedAccountType
            ? req.body.linkedAccountType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide linked account type."
                }
            });
        if (!req.body.nibiId || !req.body.linkedAccountId || !req.body.linkedAccountType) {
            return true;
        }
        let findUserQry = {};
        if (linkedAccountType === "facebook") {
            findUserQry.facebookId = linkedAccountId
        }
        if (linkedAccountType === "google") {
            findUserQry.googleId = linkedAccountId
        }
        if (linkedAccountType === "apple") {
            findUserQry.appleId = linkedAccountId
        }

        let updateQry = {};
        if (linkedAccountType === "facebook") {
            $set:{
                updateQry.facebookId = linkedAccountId;
                updateQry.isFacebook = true
            }
        }
        if (linkedAccountType === "google") {
            $set:{
                updateQry.googleId = linkedAccountId;
                updateQry.isGoogle = true
            }
        }
        if (linkedAccountType === "apple") {
            $set:{
                updateQry.appleId = linkedAccountId;
                updateQry.isApple = true
            }
        }
        User.findOne(findUserQry, function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (user) {
                    if (user.nibiId == nibiId) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "User already linked with this account.",
                                statusCode: 404
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "This account already exist.",
                                statusCode: 404
                            }
                        });
                    }

                } else {
                    User.updateOne({nibiId: nibiId}, updateQry, function (err, user) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            User.findOne({nibiId: nibiId}).select('-password -blockedUsers -blockedMeByOthers').exec(function (err, updatedUserData) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: updatedUserData
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    deLinkAccount: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });
        let socialType = req.body.socialType
            ? req.body.socialType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide social type."
                }
            });
        if (!req.body.nibiId || !req.body.socialType) {
            return true;
        }
        let findUserQry = {
            nibiId: nibiId
        };

        let updateQry = {};
        if (socialType === "facebook") {
            $set:{
                updateQry.facebookId = "";
                updateQry.isFacebook = false
            }
        }
        if (socialType === "google") {
            $set:{
                updateQry.googleId = "";
                updateQry.isGoogle = false
            }
        }
        if (socialType === "apple") {
            $set:{
                updateQry.appleId = "";
                updateQry.isApple = false
            }
        }

        User.findOne(findUserQry, function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (user) {
                    User.updateOne({nibiId: nibiId}, updateQry, function (err, user) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            User.findOne({nibiId: nibiId}).select('-password -blockedUsers -blockedMeByOthers').exec(function (err, updatedUserData) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: updatedUserData
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "User details not found.",
                            statusCode: 404
                        }
                    });
                }
            }
        });
    },
    googleKey: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let encodeData = base64encode("AIzaSyD_3mA8BD58myLRAs3DhBtZc8w_DVBuN6s");
        res.json({
            serverResponse: {
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statusCode: 200
            },
            result: {
                data: encodeData
            }
        });
    },
    sendNotificationToAllUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let title = req.body.title
            ? req.body.title
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide title."
                }
            });
        let body = req.body.body
            ? req.body.body
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide body."
                }
            });
        let topics = req.body.topic
            ? req.body.topic
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide topic."
                }
            });
        let nibiIds = req.body.nibiIds
            ? req.body.nibiIds
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id's."
                }
            });
        if (!req.body.title || !req.body.body || !req.body.topic || !req.body.nibiIds) {
            return true;
        }
        if (nibiIds.length > 0) {
            User.find({nibiId: {$in: nibiIds}}, function (err, user) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (user.length > 0) {

                        let fcmFilter = user.filter(function (e) {
                            return e.fcmToken != "";
                        });

                        let fcmTokens = fcmFilter.map(fcm => {
                            return fcm.fcmToken;
                        });

                        let payload = {
                            notification: {
                                title: title,
                                body: body
                            },
                            data: {
                                title: title,
                                body: body,
                                notificationType: 'offer'
                            }
                        };
                        NotificationService.notificationForMultipleUser(payload, fcmTokens, res)
                    } else {
                        console.log("No user found to send notification.")
                    }
                }
            });
        } else {
            User.find({}, function (err, user) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    let fcmFilter = user.filter(function (e) {
                        return e.fcmToken != "";
                    });
                    let fcmTokens = fcmFilter.map(fcm => {
                        return fcm.fcmToken;
                    });

                    let message = {
                        notification: {
                            title: title,
                            body: body
                        },
                        data: {
                            title: title,
                            body: body,
                            notificationType: 'offer'
                        },
                        topic: topics
                    };
                    NotificationService.createTopic(fcmTokens, topics, res, message);
                }
            });
        }


    },
};


// When a user update first name or last name or profile image , if that user belong on guest list his data also be updated.
function updateGuest(req, res, nibiId) {
    let updateData = {};
    if (req.body.fname) {
        updateData['guestList.$.firstName'] = req.body.fname
    }
    if (req.body.lname) {
        updateData['guestList.$.lastName'] = req.body.lname
    }
    if (req.body.profilePic) {
        updateData['guestList.$.imageUrl'] = req.body.profilePic
    }
    Event.updateMany({"guestList.nibiId": nibiId, eventEndAt: {$gte: new Date()}}, updateData, function (err, event) {
        console.log("guest list updated.")
    })
}

// When a user update first name or last name or profile image , if that user belong on notification list his data also be updated.
function updateNotificationSender(req, res, nibiId) {
    let updateData = {};
    if (req.body.fname) {
        updateData.senderFirstName = req.body.fname
    }
    if (req.body.lname) {
        updateData.senderLastName = req.body.lname
    }
    if (req.body.profilePic) {
        updateData.senderImage = req.body.profilePic
    }
    Notification.updateMany({"senderNibiId": nibiId}, updateData, function (err, notification) {
        console.log("notification list updated.")
    })
}

// When a user update first name or last name or profile image , if that user belong on blocked user list his data also be updated.
function updateBlockedUser(req, res, nibiId) {
    let updateData = {};
    if (req.body.fname) {
        updateData['blockedUsers.$.firstName'] = req.body.fname
    }
    if (req.body.lname) {
        updateData['blockedUsers.$.lastName'] = req.body.lname
    }
    if (req.body.profilePic) {
        updateData['blockedUsers.$.profilePic'] = req.body.profilePic
    }
    User.updateMany({"blockedUsers.nibiId": nibiId}, updateData, function (err, event) {
        console.log("blocked user list updated.")
    })
}

// When a user update first name or last name or profile image , if that user belong on blocked me by others list his data also be updated.
function updateBlockedMeByOthersUser(req, res, nibiId) {
    let updateData = {};
    if (req.body.fname) {
        updateData['blockedMeByOthers.$.firstName'] = req.body.fname
    }
    if (req.body.lname) {
        updateData['blockedMeByOthers.$.lastName'] = req.body.lname
    }
    if (req.body.profilePic) {
        updateData['blockedMeByOthers.$.profilePic'] = req.body.profilePic
    }
    User.updateMany({"blockedMeByOthers.nibiId": nibiId}, updateData, function (err, event) {
        console.log("blocked me by others user list updated.")
    })
}

// When a user update email , if that user belong on guest list his data also be updated.
function updateGuestEmail(req, res, email) {
    let UpdateData = {
        $set: {
            "guestList.$.email": email
        }
    }
    let query = {
        "guestList.nibiId": req.decoded.nibiId
    }
    Event.updateMany(query, UpdateData, function (err, event) {
        console.log("guest list updated.")
    })
}

/*
function deleteEvent(req, res, user, next) {
    Event.deleteMany(
        {nibiId: user.nibiId},
        function (err, event) {
            if (err) {
                console.log("Error in delete event" + err);
                next();
            } else {
                console.log("event deleted.")
            }
        });
}

function removeFromCoHost(req, res, user, next) {
    Event.updateOne({nibiId:user.nibiId}, {"$pull": {"coHosts": {"nibiId": user.nibiId}}}, {
        safe: true,
        multi: true
    }, function (err, removeCoHost) {
        if (err) {
            console.log("Error in remove from co-hosts" + err);
            next();
        } else {
            console.log("Removed from post co-hosts");
        }
    });
}
function removeFromGuestList(req, res, user, next) {
    Event.updateOne({nibiId:user.nibiId}, {"$pull": {"guestList": {"nibiId": user.nibiId}}}, {
        safe: true,
        multi: true
    }, function (err, removeGuest) {
        if (err) {
            console.log("Error in remove from guest list" + err);
            next();
        } else {
            console.log("Removed from guest list");
        }
    });
}

function deletePost(req, res, user, next) {
    Post.deleteMany(
        {nibiId: user.nibiId},
        function (err, post) {
            if (err) {
                console.log("Error in delete post" + err);
                next();
            } else {
                console.log("event post.")
            }
        });
}

function removeFromMention(req, res, user, next) {
    Post.updateOne({nibiId:user.nibiId}, {"$pull": {"mention": {"nibiId": user.nibiId}}}, {
        safe: true,
        multi: true
    }, function (err, removeMention) {
        if (err) {
        console.log("Error in remove from mention" + err);
        next();
        } else {
console.log("Removed from mention");
        }
    });
}

function removeFromPostCoHost(req, res, user, next) {
    Post.updateOne({nibiId:user.nibiId}, {"$pull": {"coHosts": {"nibiId": user.nibiId}}}, {
        safe: true,
        multi: true
    }, function (err, removeMention) {
        if (err) {
            console.log("Error in remove from co-hosts" + err);
            next();
        } else {
            console.log("Removed from post co-hosts");
        }
    });
}
*/


