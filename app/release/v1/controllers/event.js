const mongoose = require("mongoose");
const Event = require("../models/event");
const Image = require("../models/image");
const User = require("../models/user");
const Gif = require("../models/gif");
const Post = require("../models/post");
const Game = require("../models/game");
const EventGame = require("../models/event_game");
const Report = require("../models/report");
const Notification = require("../models/notification");
const Plan = require("../models/plan");
const Transaction = require("../models/transaction");
const UserPoints = require("../models/user_points");
const NotificationService = require("../../../services/notification");
const errorMsgJSON = require("../../../services/errors.json");
const Env = require("../../../../env");
const multer = require('multer');
const multerS3 = require('multer-s3');
const Moment = require('moment');
const moment = require('moment-timezone');
const json2csv = require('json2csv').parse;
const mailerController = require("../../../services/mailer");
const cron = require('node-cron');
const fs = require('fs');
// const fastcsv = require('fast-csv');
const aws = require('aws-sdk');
aws.config.update({
    secretAccessKey: Env.BUCKET[Env.MODE].secretAccessKey,
    accessKeyId: Env.BUCKET[Env.MODE].accessKeyId
});

const s3 = new aws.S3();


// const fileFilter = (req, file, cb) => {
//     if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
//         console.log('Image file');
//         cb(null, true);
//     } else {
//         console.log('Not image');
//         cb(new Error('Invalid file type, only JPEG and PNG is allowed!'), false);
//     }
// }

module.exports = {

    createEvent: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let title = req.body.title
            ? req.body.title
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " title."
                }
            });
        let eventDate = req.body.eventDate
            ? req.body.eventDate
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event date."
                }
            });
        let startTime = req.body.startTime
            ? req.body.startTime
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event start time."
                }
            });
        let endTime = req.body.endTime
            ? req.body.endTime
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event end time."
                }
            });
        let joinBefore = req.body.joinBefore
            ? req.body.joinBefore
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event join before time."
                }
            });
        // let coHosts = req.body.coHosts
        //     ? req.body.coHosts
        //     : res.json({
        //         serverResponse: {
        //             isError: true,
        //             statusCode: 404,
        //             message: errorMsgJSON[lang]["404"] + " co-host."
        //         }
        //     });

        // Co-host not required
        let coHosts = req.body.coHosts
            ? req.body.coHosts
            : [];
        if (!req.body.title || !req.body.eventDate || !req.body.startTime || !req.body.endTime || !req.body.joinBefore) {
            return true;
        }
        try {
            await User.findOne({ nibiId: req.decoded.nibiId }, function (err, userTimeZone) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    let joinCode;
                    if (req.body.joinCode) {
                        joinCode = req.body.joinCode;
                    } else {
                        joinCode = "JOIN" + new Date().getTime();       // If user not send join code random join code will generate
                    }
                    let eventDateTime = eventDate + " " + startTime;
                    let eventStart = eventDate + " " + startTime;
                    let eventEnd = eventDate + " " + endTime;
                    // Convert time to user time zone
                    let eventDateTimeWithTimeZone = moment.tz(eventDateTime, userTimeZone.timeZone);
                    let startTimeWithTimeZone = moment.tz(eventStart, userTimeZone.timeZone);
                    let endTimeWithTimeZone = moment.tz(eventEnd, userTimeZone.timeZone);
                    let joinBeforeTimeWithTimeZone = moment.tz(joinBefore, userTimeZone.timeZone);
                    // convert time to utc format
                    eventDate = eventDateTimeWithTimeZone.utc().format();
                    req.body.eventStartAt = startTimeWithTimeZone.utc().format();
                    req.body.eventEndAt = endTimeWithTimeZone.utc().format();
                    req.body.joinBefore = joinBeforeTimeWithTimeZone.utc().format();


                    let newEvent = new Event({
                        nibiId: req.decoded.nibiId,
                        title: title,
                        joinCode: joinCode,
                        joinLink: Env.JOIN_EVENT[Env.MODE] + joinCode,
                        backgroundImage: req.body.backgroundImage,
                        description: req.body.description,
                        createdDate: req.body.createdDate,
                        eventDate: eventDate,
                        eventStartAt: req.body.eventStartAt,
                        eventEndAt: req.body.eventEndAt,
                        startTime: startTime,
                        endTime: endTime,
                        joinBefore: req.body.joinBefore,
                        location: req.body.location,
                        latitude: req.body.latitude,
                        longitude: req.body.longitude,
                        virtualEventDetails: req.body.virtualEventDetails,
                        numberOfGuests: 15,
                        type: req.body.type,
                        hasCoHosts: req.body.hasCoHosts,
                        isGuestsAdmit: req.body.isGuestsAdmit,
                        isAnnounceOnEntry: req.body.isAnnounceOnEntry,
                        isSearchable: req.body.isSearchable,
                        hasPhotoAlbum: req.body.hasPhotoAlbum,
                        canGuestsMessage: req.body.canGuestsMessage
                    });

                    let guestObj = {
                        nibiId: req.decoded.nibiId,
                        firstName: userTimeZone.fname,
                        lastName: userTimeZone.lname,
                        email: userTimeZone.email,
                        imageUrl: userTimeZone.profilePic,
                        role: 'host',
                        hostApproval: true
                    }
                    newEvent.save(function (err, item) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                }
                            });
                        } else {
                            if (item) {
                                Event.updateMany(
                                    { "_id": item._id },
                                    {
                                        $push: {
                                            "coHosts": coHosts,
                                            "guestList": guestObj
                                        }
                                    }, function (err, response) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                }
                                            });
                                        } else {
                                            if (coHosts.length > 0) {
                                                let nibiIds = coHosts.map(e => {
                                                    return e.nibiId
                                                });
                                                let email = coHosts.map(e => {
                                                    return e.email
                                                });
                                                addCoHostInGuestListOnCreateEvent(coHosts, item, next);
                                                mailerController.addCoHostMail(email, item, userTimeZone)
                                                let payload = {
                                                    notification: {
                                                        title: item.title,
                                                        body: "You have added co-host."
                                                    },
                                                    data: {
                                                        title: item.title,
                                                        body: "You have added co-host.",
                                                        notificationType: 'add_cohost',
                                                        postId: "",
                                                        eventId: `${item._id}`
                                                    }
                                                };
                                                SendNotificationOnCoHostAddAndRemove(res, nibiIds, payload);
                                            }

                                            Event.findOne({ _id: item._id }, function (err, eventData) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        }
                                                    });
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: false,
                                                            message: title + " successfully created.",
                                                            statusCode: 200
                                                        },
                                                        result: {
                                                            eventDetails: eventData
                                                        }
                                                    });
                                                }
                                            });

                                        }
                                    });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: errorMsgJSON[lang]["404"]
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    joinCodeAvailable: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let joinCode = req.body.joinCode
            ? req.body.joinCode
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " Join Code."
                }
            });
        if (!req.body.joinCode) {
            return true;
        }
        let findJoinCode = {
            joinCode: joinCode
        };
        Event.findOne(findJoinCode, function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "Join code already in use.",
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        }
                    });
                }
            }
        });
    },
    userAvailable: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " Email"
                }
            });
        if (!req.body.email) {
            return true;
        }
        let findUserEmail = {
            email: email
        };
        User.findOne(findUserEmail).select('nibiId').exec(function (err, user) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!user) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            userDetails: user
                        }
                    });
                }
            }
        });
    },
    coHostAvailable: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " Email"
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibiId"
                }
            });

        if (!req.body.email || !req.body.nibiId) {
            return true;
        }
        let findUserEmail = {
            email: email
        };
        User.findOne({ nibiId: nibiId }).select('fname lname').exec(function (err, eventHost) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                User.findOne(findUserEmail).select('nibiId fname lname profilePic email').exec(function (err, user) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        if (!user) {
                            let name = eventHost.fname +" "+ eventHost.lname ;
                            mailerController.joinNibiRequest(email, name);
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "Email send to user for join Nibi",
                                    statusCode: 404
                                }
                            });
                        } else {
                            if (user.nibiId == nibiId) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "Host can't belong in co-host.",
                                        statusCode: 404
                                    }
                                });
                            }else{
                                if (req.body.eventId && req.body.eventId != "") {
                                    Event.findOne({_id: req.body.eventId}).select({coHosts: {$elemMatch: {email: email}}}).exec(function (err, event) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            if (event.coHosts.length > 0) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: "You have already in co-host list.",
                                                        statusCode: 404
                                                    }
                                                });
                                            } else {

                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        userDetails: user
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            userDetails: user
                                        }
                                    });
                                }
                            }

                        }
                    }
                });
            }
        });
    },
    // const upload = multer({
    //     fileFilter,
    //     storage: multers3({
    //         acl: 'public-read',
    //         s3,
    //         contentType: multers3.AUTO_CONTENT_TYPE,
    //         bucket: '',
    //         metadata: function (req, file, cb){
    //             cb(null, {
    //                 fieldName: 'TESTING_METADATA'
    //             })
    //         },
    //         key: function(req, file, cb){
    //             cb(null, Date.now().toString())
    //         }
    //     })
    // });
    uploadImage: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let upload = multer({
            storage: multerS3({
                s3: s3,
                bucket: Env.BUCKET[Env.MODE].Bucket,
                key: function (req, file, cb) {
                    let extension = file.mimetype.split('/');       // split file to extract file extension
                    cb(null, (Date.now() + '.' + extension[1]).toString());
                }
            })
        }).array('file', 10);    // upload maximum 10 file
        upload(req, res, function (err) {
            if (!req.files) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: "Please send a file for upload.",
                        statusCode: 404
                    }
                });
                return true;
            }
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: "Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (req.files) {
                    let imageArray = [];
                    let allImage = req.files;
                    allImage.forEach((image) => {
                        let imageObj = {sourceUrl: image.location, sourceType: image.mimetype}
                        imageArray.push(imageObj);
                    });
                    //    Image.insertMany(imageArray,function(err, result){
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            url: imageArray
                        }
                    });
                    //     });

                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                }
            }

        });

    },
    predefinedImage: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        Image.find().select('imageUrl').exec(function (err, image) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!image) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            imageList: image
                        }
                    });
                }
            }
        });
    },
    predefinedGif: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        Gif.find().select('sourceUrl').exec(function (err, image) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!image) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            gifList: image
                        }
                    });
                }
            }
        });
    },
    eventList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId;

        let findPastEvent = {
            isCancel: false,
            $or: [
                {'nibiId': nibiId},
                {'guestList.nibiId': nibiId}
            ],
            eventEndAt: {$lte: new Date()}
        };
        let findOngoingEvent = {
            isCancel: false,
            $or: [
                {'nibiId': nibiId},
                {'guestList.nibiId': nibiId}
            ],
            eventStartAt: {$lte: new Date()},
            eventEndAt: {$gte: new Date()}
        };
        let findUpcomingEvent = {
            isCancel: false,
            $or: [
                {'nibiId': nibiId},
                {'guestList.nibiId': nibiId}
            ],
            eventStartAt: {$gte: new Date()}
        };


        Event.find(findPastEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, pastEvent) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                let pastEventWithStatus = pastEvent.map(e => {
                    let event = e.toJSON();
                    if (event.type == 'free') {
                        event.currentStatus = 'past';
                        let eventExpiryDate = moment(event.eventDate).add(30, 'days');
                        let currentDate = moment(new Date());
                        event.timeDifference = eventExpiryDate.diff(currentDate, 'days');
                        return event;
                    } else if (event.type == 'paid') {
                        event.currentStatus = 'past';
                        let eventExpiryDate = moment(event.eventDate).add(180, 'days');
                        let currentDate = moment(new Date());
                        event.timeDifference = eventExpiryDate.diff(currentDate, 'days');
                        return event;
                    } else {
                        event.currentStatus = 'past';
                        event.timeDifference = 0
                        return event;
                    }

                });
                Event.find(findOngoingEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, presentEvent) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        let ongoingEventWithStatus = presentEvent.map(e => {
                            let event = e.toJSON();
                            event.currentStatus = 'ongoing';
                            return event;
                        });
                        Event.find(findUpcomingEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, upcomingEvent) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {
                                let upcomingEventWithStatus = upcomingEvent.map(e => {
                                    let event = e.toJSON();
                                    event.currentStatus = 'upcoming';
                                    return event;
                                });
                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: errorMsgJSON[lang]["200"],
                                        statusCode: 200
                                    },
                                    result: {
                                        event: [
                                            {
                                                eventName: "Event in progress",
                                                item: ongoingEventWithStatus
                                            },
                                            {
                                                eventName: "Your upcoming events",
                                                item: upcomingEventWithStatus
                                            },
                                            {
                                                eventName: "Your past events",
                                                item: pastEventWithStatus
                                            }
                                        ],
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    eventListForAdmin: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let limit = 10;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let page = 1;
        page = (page - 1) * limit;
        if (req.body.page) {
            page = (req.body.page - 1) * limit;
        }
        Event.find({}).countDocuments().exec(function (err, eventCount) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Event.find({}).limit(limit).skip(page).sort({'createdAt': 'desc'}).exec(function (err, event) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                eventCount: eventCount,
                                eventList: event
                            },
                        });
                    }
                });
            }
        });
    },
    searchFromEventList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId;
        let keywords = req.body.keywords
            ? req.body.keywords
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide search keywords."
                }
            });
        if (!req.body.keywords) {
            return true;
        }
        let findPastEvent = {
            isCancel: false,

            $and: [
                {
                    $or: [{"title": {$regex: keywords, $options: "i"}},
                        {"joinCode": {$regex: keywords, $options: "i"}}]
                },
                {
                    $or: [{'nibiId': nibiId}, {'guestList.nibiId': nibiId}]
                }

            ],
            eventEndAt: {$lte: new Date()}
        };
        let findOngoingEvent = {
            isCancel: false,
            $and: [
                {
                    $or: [{"title": {$regex: keywords, $options: "i"}},
                        {"joinCode": {$regex: keywords, $options: "i"}}]
                },
                {
                    $or: [{'nibiId': nibiId}, {'guestList.nibiId': nibiId}]
                }

            ],
            eventStartAt: {$lte: new Date()},
            eventEndAt: {$gte: new Date()}
        };
        let findUpcomingEvent = {
            isCancel: false,
            $and: [
                {
                    $or: [{"title": {$regex: keywords, $options: "i"}},
                        {"joinCode": {$regex: keywords, $options: "i"}}]
                },
                {
                    $or: [{'nibiId': nibiId}, {'guestList.nibiId': nibiId}]
                }

            ],
            eventStartAt: {$gte: new Date()}
        };


        Event.find(findPastEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, pastEvent) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Event.find(findOngoingEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, presentEvent) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        Event.find(findUpcomingEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, upcomingEvent) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: errorMsgJSON[lang]["200"],
                                        statusCode: 200
                                    },
                                    result: {
                                        event: [
                                            {
                                                eventName: "Event in progress",
                                                item: presentEvent
                                            },
                                            {
                                                eventName: "Your upcoming events",
                                                item: upcomingEvent
                                            },
                                            {
                                                eventName: "Your past events",
                                                item: pastEvent
                                            }
                                        ],
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    updateEvent: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let id = req.body._id
            ? req.body._id
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide id."
                }
            });
        let eventDate = req.body.eventDate
            ? req.body.eventDate
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event date."
                }
            });
        let startTime = req.body.startTime
            ? req.body.startTime
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event start time."
                }
            });
        let endTime = req.body.endTime
            ? req.body.endTime
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event end time."
                }
            });
        let joinBefore = req.body.joinBefore
            ? req.body.joinBefore
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event join before time."
                }
            });
        let coHosts = req.body.coHosts
            ? req.body.coHosts
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " co-host.."
                }
            });
        if (!req.body._id || !req.body.eventDate || !req.body.startTime || !req.body.endTime || !req.body.joinBefore || !req.body.coHosts) {
            return true;
        }
        let findEvent = {
            _id: id
        };
        User.findOne({nibiId: req.decoded.nibiId}, function (err, userTimeZone) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    }
                });
            } else {
                if (userTimeZone) {
                    let eventDateTime = eventDate + " " + startTime;
                    let eventStart = eventDate + " " + startTime;
                    let eventEnd = eventDate + " " + endTime;
                    let eventTimeWithTimeZone = moment.tz(eventDateTime, userTimeZone.timeZone);
                    let startTimeWithTimeZone = moment.tz(eventStart, userTimeZone.timeZone);
                    let endTimeWithTimeZone = moment.tz(eventEnd, userTimeZone.timeZone);
                    let joinBeforeTimeWithTimeZone = moment.tz(joinBefore, userTimeZone.timeZone);

                    req.body.eventDate = eventTimeWithTimeZone.utc().format()
                    req.body.eventStartAt = startTimeWithTimeZone.utc().format();
                    req.body.eventEndAt = endTimeWithTimeZone.utc().format();
                    req.body.joinBefore = joinBeforeTimeWithTimeZone.utc().format();
                    Event.findOne(findEvent, function (err, item) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            if (item) {

                                let previousCoHostNibiIds = item.coHosts.map(e => {
                                    return e.nibiId;
                                });
                                let currentCoHostNibiIds = coHosts.map(e => {
                                    return e.nibiId;
                                });
                                let newAddedCoHost = [];
                                let removedCoHost = [];
                                coHosts.map(coHost => {
                                    if (previousCoHostNibiIds.indexOf(coHost.nibiId) == -1) {
                                        newAddedCoHost.push(coHost);
                                    }
                                });
                                item.coHosts.map(coHost => {
                                    if (currentCoHostNibiIds.indexOf(coHost.nibiId) == -1) {
                                        removedCoHost.push(coHost);
                                    }
                                });
                                if (removedCoHost.length > 0) {
                                    removedCoHostFromGuestList(req, res, id, removedCoHost, next);
                                }
                                if(newAddedCoHost.length > 0){
                                    addCoHostInGuestList(req, res, id, item, newAddedCoHost, next)
                                }
                               // console.log("new", newAddedCoHost);
                               // console.log("removed", removedCoHost);
                                delete req.body._id
                                let updateValues = {
                                    $set: {
                                        ...req.body
                                    }
                                };
                                  Event.updateOne(findEvent, updateValues, function (err, res1) {
                                       if (err) {
                                           res.json({
                                               serverResponse: {
                                                   isError: true,
                                                   message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                   statusCode: 500,
                                               }
                                           });
                                       } else {
                                           if(newAddedCoHost.length > 0){
                                               let nibiIds = newAddedCoHost.map(e => {
                                                   return e.nibiId
                                               });

                                               let payload = {
                                                   notification: {
                                                       title: item.title,
                                                       body: "You have added co-host."
                                                   },
                                                   data: {
                                                       title: item.title,
                                                       body: "You have added co-host.",
                                                       notificationType: 'add_cohost',
                                                       postId: "",
                                                       eventId: id
                                                   }
                                               };
                                               SendNotificationOnCoHostAddAndRemove(res, nibiIds, payload);
                                           }
                                           if(removedCoHost.length > 0){
                                               let nibiIds = removedCoHost.map(e => {
                                                   return e.nibiId
                                               });
                                               let payload = {
                                                   notification: {
                                                       title: item.title,
                                                       body: "You have removed from co-host as well as guest list."
                                                   },
                                                   data: {
                                                       title: item.title,
                                                       body: "You have removed from co-host as well as guest list.",
                                                       notificationType: 'removed_cohost',
                                                       postId: "",
                                                       eventId: id
                                                   }
                                               };
                                               SendNotificationOnCoHostAddAndRemove(res, nibiIds, payload);
                                           }

                                Event.findOne(findEvent).select('-createdAt -updatedAt').exec(function (err, updatedItem) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: errorMsgJSON[lang]["200"],
                                                statusCode: 200
                                            },
                                            result: {
                                                eventDetails: updatedItem
                                            }
                                        });
                                    }
                                });
                                   }
                                  });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: errorMsgJSON[lang]["404"] + "- Event details."
                                    }
                                });
                            }

                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: errorMsgJSON[lang]["404"] + " Timezone"
                        }
                    });
                }
            }
        });
    },
    updateEventForAdmin: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let id = req.body._id
            ? req.body._id
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide id."
                }
            });
        let eventDate = req.body.eventDate
            ? req.body.eventDate
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event date."
                }
            });
        let startTime = req.body.startTime
            ? req.body.startTime
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event start time."
                }
            });
        let endTime = req.body.endTime
            ? req.body.endTime
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event end time."
                }
            });
        let joinBefore = req.body.joinBefore
            ? req.body.joinBefore
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event join before time."
                }
            });

        let hostNibiId = req.body.hostNibiId
            ? req.body.hostNibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please Provide Host Nibi Id."
                }
            });
        if (!req.body._id || !req.body.eventDate || !req.body.startTime || !req.body.endTime || !req.body.joinBefore || !req.body.hostNibiId) {
            return true;
        }
        let findEvent = {
            _id: id
        };
        User.findOne({nibiId: hostNibiId}, function (err, userTimeZone) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    }
                });
            } else {
                if (userTimeZone) {
                    let eventDateTime = eventDate + " " + startTime;
                    let eventStart = eventDate + " " + startTime;
                    let eventEnd = eventDate + " " + endTime;
                    let eventTimeWithTimeZone = moment.tz(eventDateTime, userTimeZone.timeZone);
                    let startTimeWithTimeZone = moment.tz(eventStart, userTimeZone.timeZone);
                    let endTimeWithTimeZone = moment.tz(eventEnd, userTimeZone.timeZone);
                    let joinBeforeTimeWithTimeZone = moment.tz(joinBefore, userTimeZone.timeZone);

                    req.body.eventDate = eventTimeWithTimeZone.utc().format()
                    req.body.eventStartAt = startTimeWithTimeZone.utc().format();
                    req.body.eventEndAt = endTimeWithTimeZone.utc().format();
                    req.body.joinBefore = joinBeforeTimeWithTimeZone.utc().format();
                    Event.findOne(findEvent, function (err, item) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            if (item) {
                                delete req.body._id
                                let updateValues = {
                                    $set: {
                                        ...req.body
                                    }
                                };
                                Event.updateOne(findEvent, updateValues, function (err, res1) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500,
                                            }
                                        });
                                    } else {
                                        Event.findOne(findEvent, function (err, updatedItem) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        eventDetails: updatedItem
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: errorMsgJSON[lang]["404"] + "- Event details."
                                    }
                                });
                            }

                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: errorMsgJSON[lang]["404"] + " Timezone"
                        }
                    });
                }
            }
        });
    },
    specificEvent: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide Event Id."
                }
            });
        let nibiId = req.body.nibiId

        if (!req.body.eventId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };

        Event.findOne(findEvent).select('-createdAt -updatedAt').exec(function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    User.findOne({ nibiId: item.nibiId }).select('fname lname').exec(function (err, user) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (!user) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["404"],
                                        statusCode: 404
                                    }
                                });
                            } else {
                                item = item.toJSON()
                                if (item.eventEndAt <= new Date()) {
                                    item.currentStatus = 'past';
                                }
                                if (item.eventStartAt <= new Date() && item.eventEndAt >= new Date()) {
                                    item.currentStatus = 'ongoing';
                                }
                                if (item.eventStartAt >= new Date()) {
                                    item.currentStatus = 'upcoming';
                                }
                                if (item.joinBefore >= new Date() && item.eventStartAt <= new Date()) {
                                    item.currentStatus = 'will_start';
                                }
                                if (req.body.nibiId != "") {
                                    User.findOne({nibiId: nibiId}).select('blockedUsers blockedMeByOthers').exec(function (err, blockedUserList) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            let combinedArray = blockedUserList.blockedUsers.concat(blockedUserList.blockedMeByOthers);
                                            if (combinedArray.length > 0) {
                                                let combinedArrayNibiIds = combinedArray.map(blockedUsers => {
                                                    return blockedUsers.nibiId
                                                });
                                                item.guestList.filter(user => {
                                                    if (combinedArrayNibiIds.indexOf(user.nibiId) != -1) {
                                                        return user.isBlocked = true;
                                                    } else {
                                                        return user.isBlocked = false;
                                                    }
                                                });
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        eventDetails: item,
                                                        host: user
                                                    },
                                                });
                                            } else {
                                                item.guestList.forEach(element => {
                                                    element.isBlocked = false;
                                                });
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        eventDetails: item,
                                                        host: user
                                                    },
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    item.guestList.forEach(element => {
                                        element.isBlocked = false;
                                    });
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            eventDetails: item,
                                            host: user
                                        },
                                    });
                                }

                            }
                        }
                    });
                }
            }
        });
    },
    eventFromJoinCode: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let joinCode = req.body.joinCode
            ? req.body.joinCode
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide join code."
                }
            });
        if (!req.body.joinCode) {
            return true;
        }
        let findEvent = {
            joinCode: joinCode
        };
        Event.findOne(findEvent).select('title description backgroundImage location latitude longitude virtualEventDetails type nibiId joinCode joinLink coHosts eventDate transactionType').exec(function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            eventDetails: item
                        }
                    });
                }
            }
        });
    },
    searchEvent: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let keywords = req.body.keywords
            ? req.body.keywords
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " keywords for search."
                }
            });
        if (!req.body.keywords) {
            return true;
        }
        let queryForJoinCode = {
            isCancel: false,
            eventEndAt: {$gte: new Date()},
            joinCode: keywords
        };
        let searchQuery = {
            isSearchable: true,
            isCancel: false,
            eventEndAt: {$gte: new Date()},
            $or: [
                {"title": {$regex: keywords, $options: "i"}}
            ],
        };


        Event.find(queryForJoinCode).select('title description joinCode backgroundImage location latitude longitude virtualEventDetails nibiId eventDate type transactionType').exec(function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (item.length > 0) {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            eventDetails: item
                        }
                    });
                } else {
                    Event.find(searchQuery).select('title description joinCode backgroundImage location latitude longitude virtualEventDetails nibiId eventDate type transactionType').exec(function (err, item) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["200"],
                                    statusCode: 200
                                },
                                result: {
                                    eventDetails: item
                                }
                            });
                        }
                    });
                }
            }
        });


    },
    joinEvent: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.nibiId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };

        Event.findOne(findEvent, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    //    if (req.body.joinType === 'user') {

                    let findGuest = {
                        _id: eventId,
                        "guestList.nibiId": nibiId
                    };
                    Event.findOne(findGuest, function (err, guest) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (guest) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You already joined this event.",
                                        statusCode: 400
                                    }
                                });
                            } else {
                                Event.findOne(findEvent).select({removedGuest: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, eventRemovedGuest) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        if (eventRemovedGuest.removedGuest.length > 0) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: "You cannot rejoin this event because you were removed.",
                                                    statusCode: 404
                                                }
                                            });
                                        } else {
                                            //   if (item.joinBefore <= new Date() && item.eventEndAt >= new Date()) {
                                            if (item.type == 'free') {
                                               let guestCount= item.guestList.filter(guest => {
                                                    return guest.role == 'guest'
                                                });
                                                if (item.numberOfGuests > guestCount.length) {
                                                    if (item.isGuestsAdmit === true) {

                                                        let guest = {
                                                            nibiId: nibiId,
                                                            firstName: req.body.firstName,
                                                            lastName: req.body.lastName,
                                                            imageUrl: req.body.imageUrl,
                                                            email: req.body.email,
                                                            role: 'guest',
                                                            hostApproval: false
                                                        };

                                                        Event.updateMany(
                                                            {"_id": eventId},
                                                            {$push: {"guestList": guest}}, function (err, response) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        },
                                                                    });
                                                                } else {
                                                                    //   createPostOnEventJoin(req, res);
                                                                    // let notificationBody = {
                                                                    //     title: item.title,
                                                                    //     body: "Approval needed: " + req.body.firstName + " " + req.body.lastName + " would like to join.",
                                                                    // }
                                                                    let payload = {
                                                                        notification: {
                                                                            title: item.title,
                                                                            body: "Approval needed: " + req.body.firstName + " " + req.body.lastName + " would like to join."
                                                                        },
                                                                        data: {
                                                                            title: item.title,
                                                                            body: "Approval needed: " + req.body.firstName + " " + req.body.lastName + " would like to join.",
                                                                            notificationType: 'will_join_event',
                                                                            postId: "",
                                                                            eventId: eventId
                                                                        }
                                                                    };
                                                                    findUserFcmAndSendNotification(res, item, payload);
                                                                    saveNotificationWhenHostApprovalNeed(item, req, res, lang)
                                                                    if (item.joinBefore <= new Date() && item.eventEndAt >= new Date()) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: "Please wait, the event host will let you in soon.",
                                                                                statusCode: 202
                                                                            }
                                                                        });
                                                                    } else {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: "Please wait, the event host will let you in soon.",
                                                                                statusCode: 405
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });

                                                    } else {
                                                        let guest = {
                                                            nibiId: nibiId,
                                                            firstName: req.body.firstName,
                                                            lastName: req.body.lastName,
                                                            imageUrl: req.body.imageUrl,
                                                            email: req.body.email,
                                                            role: 'guest',
                                                            hostApproval: true
                                                        };

                                                        Event.updateMany(
                                                            {"_id": eventId},
                                                            {$push: {"guestList": guest}}, function (err, response) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        },
                                                                    });
                                                                } else {
                                                                    createPostOnEventJoin(lang, req, res);
                                                                    // let notificationBody = {
                                                                    //     title: item.title,
                                                                    //     body: req.body.firstName + " " + req.body.lastName + " just joined.",
                                                                    // }
                                                                    let payload = {
                                                                        notification: {
                                                                            title: item.title,
                                                                            body: req.body.firstName + " " + req.body.lastName + " just joined."
                                                                        },
                                                                        data: {
                                                                            title: item.title,
                                                                            body: req.body.firstName + " " + req.body.lastName + " just joined.",
                                                                            notificationType: 'join_event',
                                                                            postId: "",
                                                                            eventId: eventId
                                                                        }
                                                                    };
                                                                    findAllUserFcmAndSendNotification(res, item, nibiId, payload);
                                                                    let purpose = "just joined."
                                                                    saveNotificationForJoinEvent(item, req, res, purpose, lang);
                                                                    if (item.joinBefore <= new Date() && item.eventEndAt >= new Date()) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: "Event joined successfully.",
                                                                                statusCode: 200
                                                                            }
                                                                        });
                                                                    } else {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: "Event joined successfully.",
                                                                                statusCode: 405
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                    }
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: "Unfortunately, the guest limit for this event has been reached.  Please contact the host to increase the guest limit for the event.",
                                                            statusCode: 403
                                                        }
                                                    });
                                                }
                                            } else {
                                                if (item.isGuestsAdmit === true) {

                                                    let guest = {
                                                        nibiId: nibiId,
                                                        firstName: req.body.firstName,
                                                        lastName: req.body.lastName,
                                                        imageUrl: req.body.imageUrl,
                                                        email: req.body.email,
                                                        role: 'guest',
                                                        hostApproval: false
                                                    };

                                                    Event.updateMany(
                                                        {"_id": eventId},
                                                        {$push: {"guestList": guest}}, function (err, response) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                //   createPostOnEventJoin(req, res);
                                                                // let notificationBody = {
                                                                //     title: item.title,
                                                                //     body: "Approval needed: " + req.body.firstName + " " + req.body.lastName + " would like to join.",
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: item.title,
                                                                        body: "Approval needed: " + req.body.firstName + " " + req.body.lastName + " would like to join."
                                                                    },
                                                                    data: {
                                                                        title: item.title,
                                                                        body: "Approval needed: " + req.body.firstName + " " + req.body.lastName + " would like to join.",
                                                                        notificationType: 'will_join_event',
                                                                        postId: "",
                                                                        eventId: eventId
                                                                    }
                                                                };
                                                                findUserFcmAndSendNotification(res, item, payload);
                                                                saveNotificationWhenHostApprovalNeed(item, req, res, lang)
                                                                if (item.joinBefore <= new Date() && item.eventEndAt >= new Date()) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: "Please wait, the event host will let you in soon.",
                                                                            statusCode: 202
                                                                        }
                                                                    });
                                                                } else {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: "Please wait, the event host will let you in soon.",
                                                                            statusCode: 405
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });

                                                } else {
                                                    let guest = {
                                                        nibiId: nibiId,
                                                        firstName: req.body.firstName,
                                                        lastName: req.body.lastName,
                                                        imageUrl: req.body.imageUrl,
                                                        email: req.body.email,
                                                        role: 'guest',
                                                        hostApproval: true
                                                    };

                                                    Event.updateMany(
                                                        {"_id": eventId},
                                                        {$push: {"guestList": guest}}, function (err, response) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                createPostOnEventJoin(lang, req, res);
                                                                // let notificationBody = {
                                                                //     title: item.title,
                                                                //     body: req.body.firstName + " " + req.body.lastName + " just joined.",
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: item.title,
                                                                        body: req.body.firstName + " " + req.body.lastName + " just joined."
                                                                    },
                                                                    data: {
                                                                        title: item.title,
                                                                        body: req.body.firstName + " " + req.body.lastName + " just joined.",
                                                                        notificationType: 'join_event',
                                                                        postId: "",
                                                                        eventId: eventId
                                                                    }
                                                                };
                                                                findAllUserFcmAndSendNotification(res, item, nibiId, payload);
                                                                let purpose = "just joined."
                                                                saveNotificationForJoinEvent(item, req, res, purpose, lang);
                                                                if (item.joinBefore <= new Date() && item.eventEndAt >= new Date()) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: "Event joined successfully.",
                                                                            statusCode: 200
                                                                        }
                                                                    });
                                                                } else {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: "Event joined successfully.",
                                                                            statusCode: 405
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                }
                                            }

                                            // } else {
                                            //     res.json({
                                            //         serverResponse: {
                                            //             isError: true,
                                            //             message: "This event is scheduled in the future and it is too early to join the event. Depending on how the event was set up, you may be able to join the event within 24 hours of the start date and time. You can calendar the event and be reminded when it is time to join.",
                                            //             statusCode: 405
                                            //         }
                                            //     });
                                            // }
                                        }
                                    }
                                });
                            }
                        }
                    });
                    // } else if (req.body.joinType === 'visitor') {
                    //     if (item.joinBefore <= new Date()) {
                    //         if (item.isGuestsAdmit === true) {
                    //             let guest = {
                    //                 firstName: req.body.firstName,
                    //                 lastName: req.body.lastName,
                    //                 hostApproval: true
                    //             };
                    //             Event.updateMany(
                    //                 {"_id": eventId},
                    //                 {$push: {"guestList": guest}}, function (err, response) {
                    //                     if (err) {
                    //                         res.json({
                    //                             serverResponse: {
                    //                                 isError: true,
                    //                                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    //                                 statusCode: 500
                    //                             },
                    //                         });
                    //                     } else {
                    //                         res.json({
                    //                             serverResponse: {
                    //                                 isError: false,
                    //                                 message: "You have joined the event but wait till host approval.",
                    //                                 statusCode: 202
                    //                             }
                    //                         });
                    //                     }
                    //                 });
                    //         } else {
                    //             let guest = {
                    //                 firstName: req.body.firstName,
                    //                 lastName: req.body.lastName,
                    //                 hostApproval: false
                    //             };
                    //             Event.updateMany(
                    //                 {"_id": eventId},
                    //                 {$push: {"guestList": guest}}, function (err, response) {
                    //                     if(err){
                    //                         res.json({
                    //                             serverResponse: {
                    //                                 isError: true,
                    //                                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    //                                 statusCode: 500
                    //                             },
                    //                         });
                    //                     }else{
                    //                         res.json({
                    //                             serverResponse: {
                    //                                 isError: false,
                    //                                 message: "Event joined successfully.",
                    //                                 statusCode: 200
                    //                             }
                    //                         });
                    //                     }
                    //                 });
                    //         }
                    //     } else {
                    //         res.json({
                    //             serverResponse: {
                    //                 isError: false,
                    //                 message: "You can't join before the joining time.",
                    //                 statusCode: 404
                    //             }
                    //         });
                    //     }
                    // } else {
                    //     res.json({
                    //         serverResponse: {
                    //             isError: true,
                    //             statusCode: 400,
                    //             message: "Please provide the correct join type."
                    //         }
                    //     });
                    // }
                }
            }
        });
    },
    removeGuestFromGuestList: function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.nibiId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };
        Event.findOne(findEvent, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"] + " Event details.",
                            statusCode: 404
                        }
                    });
                } else {
                    Event.findOne(findEvent).select({ guestList: { $elemMatch: { nibiId: nibiId } } }).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length < 1) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "Guest not found.",
                                        statusCode: 404
                                    }
                                });
                            } else {
                                Event.updateOne(findEvent, { "$pull": { "guestList": { "nibiId": nibiId } } }, {
                                    safe: true,
                                    multi: true
                                }, function (err, removeGuest) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        if (event.guestList[0].role == 'cohost') {
                                            Event.updateOne(findEvent, { "$pull": { "coHosts": { "nibiId": nibiId } } }, {
                                                safe: true,
                                                multi: true
                                            }, function (err, removeCoHosts) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        },
                                                    });
                                                } else {
                                                    let notificationTitle = {
                                                        notification: {
                                                            title: item.title,
                                                            body: "You have removed from co-host as well as guest list."
                                                        },
                                                        data: {
                                                            title: item.title,
                                                            body: "You have removed from co-host as well as guest list.",
                                                            notificationType: 'removed_cohost',
                                                            postId: "",
                                                            eventId: eventId
                                                        }
                                                    };
                                                    findUserFcmAndSendNotificationOnHostApproval(res, nibiId, notificationTitle);

                                                    Event.findOne(findEvent, function (err, updatedEvent) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: errorMsgJSON[lang]["200"],
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    eventDetails: updatedEvent
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            Event.updateOne(
                                                { "_id": eventId },
                                                { $push: { "removedGuest": event.guestList } }, function (err, response) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {

                                                        let notificationTitle = {
                                                            notification: {
                                                                title: item.title,
                                                                body: "You have removed from guest list."
                                                            },
                                                            data: {
                                                                title: item.title,
                                                                body: "You have removed from guest list.",
                                                                notificationType: 'removed_guest',
                                                                postId: "",
                                                                eventId: eventId
                                                            }
                                                        };
                                                        findUserFcmAndSendNotificationOnHostApproval(res, nibiId, notificationTitle);

                                                        Event.findOne(findEvent, function (err, updatedEvent) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: false,
                                                                        message: errorMsgJSON[lang]["200"],
                                                                        statusCode: 200
                                                                    },
                                                                    result: {
                                                                        eventDetails: updatedEvent
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                        }

                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    },
    removeCOHost: function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.nibiId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };
        Event.findOne(findEvent, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"] + " Event details.",
                            statusCode: 404
                        }
                    });
                } else {
                    Event.findOne(findEvent).select({ coHosts: { $elemMatch: { nibiId: nibiId } } }).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.coHosts.length < 1) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "Co-host not found.",
                                        statusCode: 404
                                    }
                                });
                            } else {
                                Event.updateOne(findEvent, { "$pull": { "coHosts": { "nibiId": nibiId } } }, {
                                    safe: true,
                                    multi: true
                                }, function (err, removeCoHost) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        Event.findOne(findEvent).select({ guestList: { $elemMatch: { nibiId: nibiId } } }).exec(function (err, event) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                if (event.guestList.length < 1) {
                                                    Event.findOne(findEvent, function (err, updatedEvent) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: errorMsgJSON[lang]["200"],
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    eventDetails: updatedEvent
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    Event.updateOne(findEvent, { "$pull": { "guestList": { "nibiId": nibiId } } }, {
                                                        safe: true,
                                                        multi: true
                                                    }, function (err, removeGuest) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {
                                                            Event.findOne(findEvent, function (err, updatedEvent) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        },
                                                                    });
                                                                } else {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: errorMsgJSON[lang]["200"],
                                                                            statusCode: 200
                                                                        },
                                                                        result: {
                                                                            eventDetails: updatedEvent
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    },
    checkHosApproval: function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId;
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        if (!req.body.eventId) {
            return true;
        }
        let findGuest = {
            _id: eventId,
            "guestList.nibiId": nibiId
        };
        Event.findOne(findGuest, function (err, guest) {

            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (guest) {
                    Event.findOne({ _id: eventId }).select({
                        guestList: {
                            $elemMatch: {
                                nibiId: nibiId,
                                hostApproval: true
                            }
                        }
                    }).exec(function (err, hostApproval) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (hostApproval.guestList.length > 0) {
                                if (guest.joinBefore >= new Date()) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: "This event has not started yet. Please check back on the scheduled event date and time.",
                                            statusCode: 404
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                    });

                                }

                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "Need host approval.",
                                        statusCode: 404
                                    },
                                });
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "You are no longer participant in this event.",
                            statusCode: 404
                        }
                    });
                }
            }
        });
    },
    updateHosApprovalStatus: function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.nibiId) {
            return true;
        }
        let findGuest = {
            _id: eventId,
            "guestList.nibiId": nibiId
        };
        let updateHostApproval = {
            "guestList.$.hostApproval": true
        }
        Event.findOne(findGuest, function (err, guest) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (guest) {
                    Event.updateOne(findGuest, updateHostApproval, function (err, hostApproval) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            Event.findOne(findGuest).select('title nibiId guestList').exec(function (err, guestList) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    User.findOne({ nibiId: nibiId }).select('fname lname blockedUsers blockedMeByOthers').exec(function (err, blockedUserList) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            // Create a post on success of host approval
                                            createPostOnHostApproval(lang, req, res, blockedUserList);
                                            // let notificationBody = {
                                            //     title: guestList.title,
                                            //     body: blockedUserList.fname + " " + blockedUserList.lname + " just joined.",
                                            // }
                                            let payload = {
                                                notification: {
                                                    title: guestList.title,
                                                    body: blockedUserList.fname + " " + blockedUserList.lname + " just joined."
                                                },
                                                data: {
                                                    title: guestList.title,
                                                    body: blockedUserList.fname + " " + blockedUserList.lname + " just joined.",
                                                    notificationType: 'join_event',
                                                    postId: "",
                                                    eventId: eventId
                                                }
                                            };

                                            let item = guestList
                                            // Send notification all guest of event
                                            findAllUserFcmAndSendNotification(res, item, nibiId, payload);
                                            // let notificationTitle = {
                                            //     title: guestList.title,
                                            //     body: "You have been approved. You can access the event now.",
                                            // }
                                            let notificationTitle = {
                                                notification: {
                                                    title: guestList.title,
                                                    body: "You have been approved. You can access the event now."
                                                },
                                                data: {
                                                    title: guestList.title,
                                                    body: "You have been approved. You can access the event now.",
                                                    notificationType: 'host_approved',
                                                    postId: "",
                                                    eventId: eventId
                                                }
                                            };
                                            // Send notification that user whom host approved
                                            findUserFcmAndSendNotificationOnHostApproval(res, nibiId, notificationTitle)

                                            // save notification on db after host approval (receiver - all guest)
                                            saveNotificationForJoinEventAfterHostApproval(guest, req, res, lang)
                                            // save notification on db after host approval (receiver - user whom approved)
                                            saveNotificationForApprovedUserAfterHostApproval(guest, req, res, lang)
                                            guestList = guestList.toJSON()
                                            let combinedArray = blockedUserList.blockedUsers.concat(blockedUserList.blockedMeByOthers);
                                            if (combinedArray.length > 0) {
                                                combinedArray.map(blockedUsers => {

                                                    guestList.guestList.filter(user => {
                                                        if (user.nibiId === blockedUsers.nibiId) {
                                                            return user.isBlocked = true;
                                                        } else {
                                                            return user.isBlocked = false;
                                                        }

                                                    });
                                                });
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: "Successfully admitted to event.",
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        guestList: guestList
                                                    }
                                                });
                                            } else {
                                                guestList.guestList.forEach(element => {
                                                    element.isBlocked = false;
                                                });
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: "Successfully admitted to event.",
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        guestList: guestList
                                                    }
                                                });
                                            }

                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                }
            }
        });
    },
    exportGuestList: async function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.params.eventId
            ? req.params.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        if (!req.params.eventId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };
        Event.findOne(findEvent, function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event.guestList.length > 0) {
                    let newGuestList = [];
                    event.guestList.forEach(guest => {
                        let guestObj = {
                            FIRST_NAME: guest.firstName,
                            LAST_NAME: guest.lastName,
                            // PROFILE_IMAGE: guest.imageUrl,
                            EMAIL: guest.email,
                            // NIBI_ID: guest.nibiId
                        }
                        newGuestList.push(guestObj);
                    });
                    const csvString = json2csv(newGuestList);
                    //   res.setHeader('Content-disposition', 'attachment; filename=' + event._id + '.csv');
                    //   res.set('Content-Type', 'text/csv');
                    //  res.status(200).send(csvString);

                    // const ws = fs.createWriteStream("./uploads/" +event._id+".csv");
                    // fastcsv.write(newGuestList, { headers: true }).pipe(ws);

                    //   fs.readFile('./uploads/' +event._id+'.csv', (err, data) => {
                    //   if (err) throw err;
                    const params = {
                        Bucket: Env.BUCKET[Env.MODE].Bucket + '/guest_csv',
                        Key: event._id + '.csv', // file will be saved as testBucket/contacts.csv
                        Body: csvString
                        //   JSON.stringify(data, null, 2)
                    };

                    s3.upload(params, function (s3Err, data) {
                        if (s3Err) throw s3Err
                        //  res.status(200).send(`File uploaded successfully at ${data.Location}`);
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: "Guest list exported successfully.",
                                statusCode: 200
                            },
                            result: {
                                csvUrl: `${data.Location}`
                            }
                        });
                    });
                    //    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "No user found in guest list.",
                            statusCode: 404
                        },
                    });
                }
            }
        });
    },
    cancelEvent: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event id."
                }
            });
        if (!req.body.eventId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };
        Event.findOne(findEvent, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (item) {
                    if (item.eventStartAt <= new Date()) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 400,
                                message: "Unfortunately, you cannot cancel an ongoing event."
                            }
                        });
                    } else {
                        let updateValues = {
                            $set: {
                                isCancel: true,
                                cancelReason: req.body.cancelReason
                            }
                        };
                        Event.updateOne(findEvent, updateValues, function (err, response) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500,
                                    }
                                });
                            } else {
                                let mailId = item.guestList.map(mailIds => {
                                    return mailIds.email
                                });
                                mailerController.cancelEventMail(mailId, item);
                                if (item.transactionType == "onetime" || item.transactionType == "points") {
                                    User.findOne({ nibiId: item.nibiId }, function (err, userRefundPoint) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500,
                                                }
                                            });
                                        } else {
                                            let updateRefundPoints = (userRefundPoint.refundPoints + item.amount).toFixed(2);
                                            let updateValues = {
                                                $set: {
                                                    refundPoints: updateRefundPoints
                                                }
                                            };
                                            User.updateOne({ nibiId: item.nibiId }, updateValues, function (err, updateRefundPoint) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500,
                                                        }
                                                    });
                                                } else {
                                                    let newUserPoints = new UserPoints({
                                                        eventId: eventId,
                                                        nibiId: item.nibiId,
                                                        points: item.amount,
                                                        client: req.body.client,
                                                        status: "added",
                                                    });
                                                    newUserPoints.save(function (err, addPoints) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500,
                                                                }
                                                            });
                                                        } else {
                                                            User.findOne({ nibiId: item.nibiId }, function (err, userRefundPoint) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500,
                                                                        }
                                                                    });
                                                                } else {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: "Event cancelled successfully.",
                                                                            statusCode: 200
                                                                        },
                                                                        result: {
                                                                            refundPoints: userRefundPoint.refundPoints
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    User.findOne({ nibiId: item.nibiId }, function (err, userRefundPoint) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500,
                                                }
                                            });
                                        } else {
                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    message: "Event cancelled successfully.",
                                                    statusCode: 200
                                                },
                                                result: {
                                                    refundPoints: userRefundPoint.refundPoints
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: errorMsgJSON[lang]["404"]
                        }
                    });
                }
            }
        });
    },
    createReport: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let whoReported = req.body.whoReported
            ? req.body.whoReported
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " Please send nibi id who reporting."
                }
            });
        let whoseAgainst = req.body.whoseAgainst
            ? req.body.whoseAgainst
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " Please send nibi id whose against reported."
                }
            });
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " Please send event id."
                }
            });
        if (!req.body.whoReported || !req.body.whoseAgainst || !req.body.eventId) {
            return true;
        }
        try {
            Event.findOne({ _id: eventId }).select({ guestList: { $elemMatch: { nibiId: whoReported } } }).exec(function (err, event) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (event.guestList.length == 0) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "You are no longer participant in this event.",
                                statusCode: 410
                            },
                        });
                    } else {
                        let newReport = new Report({
                            whoReported: whoReported,
                            reportedPersonName: req.body.reportedPersonName,
                            whoseAgainst: whoseAgainst,
                            reportAgainstPersonName: req.body.reportAgainstPersonName,
                            comment: req.body.comment,
                            cause: req.body.cause,
                            event: mongoose.Types.ObjectId(eventId),
                            type: req.body.type
                        });
                        newReport.save(function (err, item) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {
                                if (item) {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            reportDetails: item
                                        }
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            statusCode: 404,
                                            message: errorMsgJSON[lang]["404"]
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });

        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    findReport: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let reportId = req.body.reportId
            ? req.body.reportId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide report id."
                }
            });
        if (!req.body.reportId) {
            return true;
        }
        Report.findOne({ _id: reportId }).populate('event', 'title').exec(function (err, report) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!report) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            reportDetails: report
                        }
                    });
                }
            }
        });
    },
    reportList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let limit = 25;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let page = 1;
        if (req.body.page) {
            page = req.body.page;
        }
        let skip = (page - 1) * limit;



        Report.find({}).countDocuments().exec(function (err, reportCount) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Report.find({}).populate('event', 'title').skip(skip).limit(limit).sort({ 'addedAt': 'desc' }).exec(function (err, report) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                totalCount: reportCount,
                                reportDetails: report
                            }
                        });
                    }
                });
            }
        });
    },
    addGuest: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.nibiId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };
        Event.findOne(findEvent, function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event) {
                    Event.findOne(findEvent).select({ removedGuest: { $elemMatch: { nibiId: nibiId } } }).exec(function (err, removedGuestList) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (removedGuestList.removedGuest.length > 0) {
                                Event.findOne(findEvent).select({ guestList: { $elemMatch: { nibiId: nibiId } } }).exec(function (err, guest) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        if (guest.guestList.length > 0) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    statusCode: 404,
                                                    message: "User already exist in guest list."
                                                }
                                            });
                                        } else {
                                            Event.updateOne(findEvent, { "$pull": { "removedGuest": { "nibiId": nibiId } } }, {
                                                safe: true,
                                                multi: true
                                            }, function (err, removeLike) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        },
                                                    });
                                                } else {
                                                    let updateQuery = {
                                                        nibiId: removedGuestList.removedGuest[0].nibiId,
                                                        firstName: removedGuestList.removedGuest[0].firstName,
                                                        lastName: removedGuestList.removedGuest[0].lastName,
                                                        email: removedGuestList.removedGuest[0].email,
                                                        imageUrl: removedGuestList.removedGuest[0].imageUrl,
                                                        role: removedGuestList.removedGuest[0].role,
                                                        hostApproval: true,
                                                        privateMessage: removedGuestList.removedGuest[0].privateMessage
                                                    }
                                                    Event.updateOne(findEvent,
                                                        { $push: { "guestList": updateQuery } }, function (err, updatedData) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                Event.findOne(findEvent, function (err, updatedEvent) {
                                                                    if (err) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: true,
                                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                statusCode: 500
                                                                            },
                                                                        });
                                                                    } else {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: errorMsgJSON[lang]["200"],
                                                                                statusCode: 200
                                                                            },
                                                                            result: {
                                                                                eventDetails: updatedEvent
                                                                            }
                                                                        });
                                                                    }
                                                                });

                                                            }
                                                        });
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Guest details not found in removed list."
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Event details not found."
                        }
                    });
                }
            }
        });
    },
    upgradeEvent: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event id."
                }
            });
        let transactionId = req.body.transactionId
            ? req.body.transactionId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " transaction id."
                }
            });
        let transactionType = req.body.transactionType
            ? req.body.transactionType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " transaction type."
                }
            });
        let amount = req.body.amount
            ? req.body.amount
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " amount."
                }
            });
        let userId = req.body.userId
            ? req.body.userId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " user id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let planId = req.body.planId
            ? req.body.planId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " plan id."
                }
            });
        let client = req.body.client
            ? req.body.client
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " client."
                }
            });
        if (!req.body.transactionId || !req.body.transactionType || !req.body.amount || !req.body.eventId || !req.body.nibiId || !req.body.planId || !req.body.client || !req.body.userId) {
            return true;
        }
        let findEvent = {
            _id: eventId
        };
        let findPlan = {
            planId: planId
        };
        Event.findOne(findEvent, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (item) {
                    Plan.findOne(findPlan, function (err, plan) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            if (plan) {
                                delete req.body.eventId;
                                let updateValues = {
                                    $set: {
                                        transactionId: transactionId,
                                        transactionType: transactionType,
                                        amount: plan.amount,
                                        type: 'paid'
                                    }
                                };
                                Event.updateOne(findEvent, updateValues, function (err, response) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500,
                                            }
                                        });
                                    } else {
                                        let newTransaction = new Transaction({
                                            transactionId: transactionId,
                                            transactionType: transactionType,
                                            amount: plan.amount,
                                            currency: req.body.currency,
                                            client: client,
                                            eventId: eventId,
                                            user: mongoose.Types.ObjectId(userId),
                                            nibiId: nibiId,
                                            description: amount
                                        });
                                        newTransaction.save(function (err, transaction) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {
                                                Event.findOne(findEvent, function (err, updatedItem) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                eventDetails: updatedItem
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Plan details not found."
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: errorMsgJSON[lang]["404"] + "- Event details."
                        }
                    });
                }
            }
        });
    },
    dataCount: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        User.find({}).countDocuments().exec(function (err, userCount) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Event.find({}).countDocuments().exec(function (err, eventCount) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        Game.find({}).countDocuments().exec(function (err, gameCount) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: errorMsgJSON[lang]["200"],
                                        statusCode: 200
                                    },
                                    result: {
                                        userCount: userCount,
                                        eventCount: eventCount,
                                        gameCount: gameCount
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },
}

function createPostOnEventJoin(lang, req, res) {
    let newPost = new Post({
        eventId: req.body.eventId,
        nibiId: req.body.nibiId,
        comment: req.body.firstName + " " + req.body.lastName + " has joined.",
        postType: "join",
        user: mongoose.Types.ObjectId(req.body.userId)
    });
    newPost.save(function (err, item) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            console.log("Post created");
        }
    });

}

function createPostOnHostApproval(lang, req, res, blockedUserList) {
    let newPost = new Post({
        eventId: req.body.eventId,
        nibiId: req.body.nibiId,
        comment: blockedUserList.fname + " " + blockedUserList.lname + " has joined.",
        postType: "join",
        user: mongoose.Types.ObjectId(blockedUserList._id)
    });
    newPost.save(function (err, item) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            console.log("Post created");
        }
    });

}

function findUserFcmAndSendNotification(res, item, payload) {

    User.findOne({ nibiId: item.nibiId, isNotify: true }, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "Generic error - Error: ",
                    statusCode: 500
                }
            });
        } else {
            if (fcm) {
                if (fcm.fcmToken) {
                    let fcmTokens = [];
                    fcmTokens.push(fcm);
                    NotificationService.sendNotification(fcmTokens, payload);
                } else {
                    console.log("User fcm not found.");
                }
            } else {
                console.log("User not interested to receive notification.");
            }

        }
    });
}

function findUserFcmAndSendNotificationOnHostApproval(res, nibiId, notificationTitle) {
    User.findOne({ nibiId: nibiId, isNotify: true }, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "Generic error - Error: ",
                    statusCode: 500
                }
            });
        } else {
            if (fcm) {
                if (fcm.fcmToken) {
                    let fcmTokens = [];
                    fcmTokens.push(fcm);
                    NotificationService.sendNotification(fcmTokens, notificationTitle);
                } else {
                    console.log("No user found to send notification.");
                }
            } else {
                console.log("User not interested to receive notification.");
            }
        }
    });
}

function findAllUserFcmAndSendNotification(res, item, nibiId, payload) {
    let guestNibiIds = item.guestList.map(e => {
        if (e.nibiId != nibiId) {
            return e.nibiId
        }
    });
    User.find({ nibiId: { $in: guestNibiIds }, isNotify: true }, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "Generic error - Error: ",
                    statusCode: 500
                }
            });
        } else {
            if (fcm.length > 0) {
                let fcmFilter = fcm.filter(function (e) {
                    return e.fcmToken != "";
                });
                let fcmTokens = fcmFilter.map(fcm => {
                    return fcm;
                });

                NotificationService.sendNotification(fcmTokens, payload);
            } else {
                console.log("No user found to send notification.")
            }

        }
    });
}


function SendNotificationOnCoHostAddAndRemove(res, nibiIds, payload) {

    User.find({ nibiId: { $in: nibiIds }, isNotify: true }, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "Generic error - Error: ",
                    statusCode: 500
                }
            });
        } else {
            if (fcm.length > 0) {
                let fcmFilter = fcm.filter(function (e) {
                    return e.fcmToken != "";
                });
                let fcmTokens = fcmFilter.map(fcm => {
                    return fcm;
                });

                NotificationService.sendNotification(fcmTokens, payload);
            } else {
                console.log("No user found to send notification.")
            }

        }
    });
}

function saveNotificationForJoinEvent(item, req, res, purpose, lang) {

    let newNotification = new Notification({
        senderNibiId: req.body.nibiId,
        senderFirstName: req.body.firstName,
        senderLastName: req.body.lastName,
        senderImage: req.body.imageUrl,
        purpose: purpose,
        text: item.title,
        eventId: item._id,
        eventName: item.title,
        notificationType: "join_event",
        receiver: item.guestList
    });
    newNotification.save(function (err, notification) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            item.guestList.forEach(guest => {
                User.updateMany({ nibiId: guest.nibiId }, { isNewNotification: true }, function (err, updateNewNotificationStatus) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            }
                        });
                    } else {
                        console.log("Notification saved to db for join event");
                    }
                });
            });
        }
    });
}

function saveNotificationWhenHostApprovalNeed(item, req, res, lang) {
    User.findOne({ nibiId: item.nibiId }, function (err, user) {

        let newNotification = new Notification({
            senderNibiId: req.body.nibiId,
            senderFirstName: req.body.firstName,
            senderLastName: req.body.lastName,
            senderImage: req.body.imageUrl,
            purpose: "would like to join",
            text: item.title,
            eventId: item._id,
            eventName: item.title,
            notificationType: "will_join_event",
            receiver: [{
                nibiId: item.nibiId,
                firstName: user.fname,
                lastName: user.lname,
                imageUrl: user.profilePic
            }],
        });
        newNotification.save(function (err, notification) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    }
                });
            } else {
                User.updateOne({ nibiId: item.nibiId }, { isNewNotification: true }, function (err, updateNewNotificationStatus) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            }
                        });
                    } else {
                        console.log("Notification saved to db for join event need host approval")
                    }
                });
            }
        });
    });
}

function saveNotificationForJoinEventAfterHostApproval(guest, req, res, lang) {
    let guestFilter = guest.guestList.filter(function (e) {
        return e.nibiId != req.body.nibiId;
    });
    User.findOne({ nibiId: req.body.nibiId }, function (err, user) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            if (guestFilter.length > 0) {
                let newNotification = new Notification({
                    senderNibiId: req.body.nibiId,
                    senderFirstName: user.fname,
                    senderLastName: user.lname,
                    senderImage: user.profilePic,
                    purpose: "just joined.",
                    text: guest.title,
                    eventId: guest._id,
                    eventName: guest.title,
                    notificationType: "join_event",
                    receiver: guestFilter,
                });
                newNotification.save(function (err, notification) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            }
                        });
                    } else {
                        guestFilter.forEach(guest => {
                            User.updateMany({ nibiId: guest.nibiId }, { isNewNotification: true }, function (err, updateNewNotificationStatus) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        }
                                    });
                                } else {
                                    console.log("Notification saved to db for join event");
                                }
                            });
                        });
                    }
                });
            } else {
                console.log("No guest found to save in app notification.")
            }

        }
    });
}

// This notification is saved for that user whom approved by the host
function saveNotificationForApprovedUserAfterHostApproval(guest, req, res, lang) {

    User.findOne({ nibiId: req.body.nibiId }, function (err, user) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            User.findOne({ nibiId: guest.nibiId }, function (err, host) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    let newNotification = new Notification({
                        senderNibiId: guest.nibiId,
                        senderFirstName: host.fname,
                        senderLastName: host.lname,
                        senderImage: host.profilePic,
                        purpose: "you can access the event now.",
                        text: guest.title,
                        eventId: guest._id,
                        eventName: guest.title,
                        notificationType: "host_approved",
                        receiver: [{
                            nibiId: req.body.nibiId,
                            firstName: user.fname,
                            lastName: user.lname,
                            imageUrl: user.profilePic
                        }],
                    });
                    newNotification.save(function (err, notification) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                }
                            });
                        } else {
                            User.updateOne({ nibiId: req.body.nibiId }, { isNewNotification: true }, function (err, updateNewNotificationStatus) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        }
                                    });
                                } else {
                                    console.log("Notification saved to db for join event host approved");
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

//0 0 * * *   every day 12 am.
// */10 * * * * *   every 10 sec
cron.schedule("0 0 * * *", function () {
    Event.find({}, function (err, event) {
        event.forEach(singleEvent => {
            let currentDate = moment(new Date());
            let eventOrganizedDate = moment(singleEvent.eventStartAt);
            if (singleEvent.type == "free") {
                if ((currentDate.diff(eventOrganizedDate, 'days')) > 30) {
                    deleteEvent(singleEvent);
                    deletePost(singleEvent);
                    deleteGame(singleEvent);
                    findSourceFileAndRemove(singleEvent);
                } else {
                    console.log("No old event.");
                }
            } else if (singleEvent.type == "paid" || singleEvent.type == "points") {
                if (currentDate.diff(eventOrganizedDate, 'days') > 180) {
                    deleteEvent(singleEvent);
                    deletePost(singleEvent);
                    deleteGame(singleEvent);
                    findSourceFileAndRemove(singleEvent);
                } else {
                    console.log("No old event.");
                }
            } else {
                console.log("Event type not matched.");
            }
        });
    });
});

function deleteEvent(singleEvent) {
    Event.deleteOne(
        { _id: singleEvent._id },
        function (err, item) {
            if (err) {
                console.log("err");
            } else {
                console.log("Event deleted.");
            }
        });
}

function deletePost(singleEvent) {
    Post.deleteMany(
        { eventId: singleEvent._id },
        function (err, item) {
            if (err) {
                console.log("err");
            } else {
                console.log("Post deleted.");
            }
        });
}

function deleteGame(singleEvent) {
    EventGame.deleteMany(
        { event: singleEvent._id },
        function (err, item) {
            if (err) {
                console.log("err");
            } else {
                console.log("Game deleted.");
            }
        });
}


function findSourceFileAndRemove(singleEvent) {
    Post.find({ eventId: singleEvent._id }, function (err, post) {
        let finalSourceFileArray = []
        post.forEach(singlePost => {
            //  if (singlePost.sourceFile.length > 0) {
            singlePost.sourceFile.forEach(singleSourceFile => {
                finalSourceFileArray.push(singleSourceFile.sourceUrl);
            });
        });
        let deleteUrl = [];
        finalSourceFileArray.forEach(allSourceUrl => {
            let imageUrl = allSourceUrl;
            let imageName = imageUrl.split(Env.SPLIT_FROM[Env.MODE]);
            let deletedObj = { Key: imageName[1] };
            deleteUrl.push(deletedObj)
        });
        const params = {
            Bucket: Env.BUCKET[Env.MODE].Bucket,
            Delete: {
                Objects: deleteUrl,
                Quiet: false
            }
        };
        s3.deleteObjects(params, function (err, data) {
            console.log("Aws file deleted.")
        });
    });
}

function removedCoHostFromGuestList(req, res, id, removedCoHost, next) {
    removedCoHost.map(e => {
        Event.updateOne({ _id: id }, { "$pull": { "guestList": { "nibiId": e.nibiId } } }, {
            safe: true,
            multi: true
        }, function (err, removeCoHost) {
            if (err) {
                console.log("Error on co host delete" + err);
                next();
            } else {
                console.log("Co-host removed from guest list.")
            }
        });
    });
}

function addCoHostInGuestList(req, res, id, item, newAddedCoHost, next) {
    let inGuestList = [];
    let outOFGuestList = [];
    let guestNibiIds = item.guestList.map(e => {
        return e.nibiId;
    });
    newAddedCoHost.map(coHost => {
        if (guestNibiIds.indexOf(coHost.nibiId) > -1) {
            inGuestList.push(coHost);
        } else {
            outOFGuestList.push(coHost)
        }
    });
    if (inGuestList.length > 0) {
        inGuestList.map(e => {
            Event.updateOne({
                _id: id,
                'guestList.nibiId': e.nibiId
            }, { 'guestList.$.role': 'cohost' }, function (err, updateGuest) {
                if (err) {
                    console.log("Error in update co host role in guest list.");
                    next();
                }
                console.log("In guest list co-host role update.");
            });
        });
    }
    if (outOFGuestList.length > 0) {
        outOFGuestList.map(e => {
            User.findOne({ nibiId: e.nibiId }, function (err, user) {
                if (err) {
                    console.log("Error in user fetching");
                    next();
                } else {
                    let guestObj = {
                        nibiId: e.nibiId,
                        firstName: user.fname,
                        lastName: user.lname,
                        email: user.email,
                        imageUrl: user.profilePic,
                        role: 'cohost',
                        hostApproval: true
                    }
                    Event.updateOne({ _id: id }, { $push: { "guestList": guestObj } }, function (err, updatedGuestList) {
                        if (err) {
                            console.log("error in update guest list");
                            next();
                        } else {
                            console.log("Guest list updated.");
                        }
                    });
                }
            });
        });
    }

}


function addCoHostInGuestListOnCreateEvent(coHosts, item, next) {
    if (coHosts.length > 0) {
        coHosts.map(e => {
            User.findOne({ nibiId: e.nibiId }, function (err, user) {
                if (err) {
                    console.log("Error in user fetching");
                    next();
                } else {
                    let guestObj = {
                        nibiId: e.nibiId,
                        firstName: user.fname,
                        lastName: user.lname,
                        email: user.email,
                        imageUrl: user.profilePic,
                        role: 'cohost',
                        hostApproval: true
                    }
                    Event.updateOne({ _id: item._id }, { $push: { "guestList": guestObj } }, function (err, updatedGuestList) {
                        if (err) {
                            console.log("error in update guest list");
                            next();
                        } else {
                            console.log("Guest list updated.");
                        }
                    });
                }
            });
        });
    } else {
        console.log("No co-host selected.")
    }
}