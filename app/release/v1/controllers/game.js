const mongoose = require("mongoose");
const Game = require("../models/game");
const EventGame = require("../models/event_game");
const Event = require("../models/event");
const AppSettings = require("../models/app_seetings");
const errorMsgJSON = require("../../../services/errors.json");


module.exports = {

    // createGame: (req, res, next) => {
    //     let lang = req.headers.language ? req.headers.language : "EN";
    //     let categoryId = req.body.categoryId
    //         ? req.body.categoryId
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: errorMsgJSON[lang]["404"] + " category id."
    //             }
    //         });
    //     let gameType = req.body.gameType
    //         ? req.body.gameType
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: errorMsgJSON[lang]["404"] + " game type."
    //             }
    //         });
    //     if (!req.body.categoryId || !req.body.gameType) {
    //         return true;
    //     }
    //     try {
    //         let newGame = new Game({
    //             gameType: gameType,
    //             gameName: req.body.gameName,
    //             description: req.body.description,
    //             categoryId: categoryId,
    //             isEditable: req.body.isEditable,
    //             shortDescription: req.body.shortDescription
    //         });
    //         newGame.save(function (err, item) {
    //             if (err) {
    //                 res.json({
    //                     serverResponse: {
    //                         isError: true,
    //                         message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                         statusCode: 500
    //                     }
    //                 });
    //             } else {
    //                 if (item) {
    //                     Game.updateMany(
    //                         {_id: item.id},
    //                         {$push: {"rounds": {$each: req.body.rounds}}}, function (err, response) {
    //                             if (err) {
    //                                 res.json({
    //                                     serverResponse: {
    //                                         isError: true,
    //                                         message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                         statusCode: 500
    //                                     }
    //                                 });
    //                             } else {
    //                                 res.json({
    //                                     serverResponse: {
    //                                         isError: false,
    //                                         message: "Game successfully created",
    //                                         statusCode: 200
    //                                     }
    //                                 });
    //                             }
    //                         });
    //                 } else {
    //                     res.json({
    //                         serverResponse: {
    //                             isError: true,
    //                             statusCode: 404,
    //                             message: errorMsgJSON[lang]["404"]
    //                         }
    //                     });
    //                 }
    //             }
    //         });
    //     } catch (e) {
    //         res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 message: errorMsgJSON[lang]["500"] + "- Error: " + e,
    //                 statusCode: 500
    //             }
    //         });
    //     }
    // },


    createGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let categoryId = req.body.categoryId
            ? req.body.categoryId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " category id."
                }
            });
        let gameType = req.body.gameType
            ? req.body.gameType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " game type."
                }
            });
        if (!req.body.categoryId || !req.body.gameType) {
            return true;
        }
        try {
            let gameArray = []
            categoryId.forEach(singleId => {
                let newGame = {
                    gameType: gameType,
                    gameName: req.body.gameName,
                    description: req.body.description,
                    categoryId: singleId,
                    isEditable: req.body.isEditable,
                    shortDescription: req.body.shortDescription,
                    rounds: req.body.rounds
                };
                gameArray.push(newGame)
            });
            Game.insertMany(gameArray, function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (item) {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: "Game successfully created",
                                statusCode: 200
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: errorMsgJSON[lang]["404"]
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },


    categoryWiseGameList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let categoryId = req.body.categoryId
            ? req.body.categoryId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide category id."
                }
            });
        if (!req.body.categoryId) {
            return true;
        }
        Game.find({categoryId: categoryId}).sort({'gameName': 'asc'}).exec(function (err, games) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                AppSettings.find({}, function (err, appSettings) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        // Here shuffle rounds of all game and shuffle according to app setting maximum question allowed number.
                        if (appSettings.length > 0) {
                            let finalGameList = [];
                            games.map((game) => {
                                let allGame = game.toJSON();
                                if (allGame.rounds.length > appSettings[0].maximumQuestionAllowed) {
                                    let newItems = [];
                                    let countItems = [];
                                    for (let i = 0; newItems.length < appSettings[0].maximumQuestionAllowed; i++) {
                                        let idx = Math.floor(Math.random() * allGame.rounds.length);
                                        if (!countItems.includes(idx)) {
                                            countItems.push(idx);
                                            newItems.push(allGame.rounds[idx]);
                                        }
                                    }
                                    allGame.rounds = newItems;
                                    finalGameList.push(allGame);
                                } else {
                                    finalGameList.push(allGame);
                                }
                            });
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["200"],
                                    statusCode: 200
                                },
                                result: {
                                    gameDetails: finalGameList
                                }
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "Maximum question allowed not found."
                                }
                            });
                        }
                    }
                });
            }
        });
    },
    specificGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let gameId = req.body.gameId
            ? req.body.gameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide game id."
                }
            });
        if (!req.body.gameId) {
            return true;
        }
        Game.findOne({_id: gameId}, function (err, games) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (games) {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            gameDetails: games
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Game details not found."
                        }
                    });
                }

            }
        });
    },
    editGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let gameId = req.body.gameId
            ? req.body.gameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide game id."
                }
            });

        if (!req.body.gameId) {
            return true;
        }
        let findGame = {
            _id: gameId
        }
        let updateData = {
            $set: {
                ...req.body
            }
        }
        Game.findOne(findGame, function (err, game) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (game) {
                    Game.updateOne(findGame, updateData, function (err, updateGame) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            Game.findOne(findGame, function (err, gameData) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            gameDetails: gameData
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Game details not found."
                        }
                    });
                }
            }
        });
    },
    deleteGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let gameId = req.body.gameId
            ? req.body.gameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide game id."
                }
            });

        if (!req.body.gameId) {
            return true;
        }
        let findGame = {
            _id: gameId
        }
        Game.findOne(findGame, function (err, game) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (game) {
                    Game.deleteOne(findGame, function (err, deletedGame) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: "Game successfully deleted.",
                                    statusCode: 200
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Game details not found."
                        }
                    });
                }
            }
        });
    },

    //#region for backup on 22 feb by subhendu
    // createGameForEvent: (req, res, next) => {
    //     let lang = req.headers.language ? req.headers.language : "EN";
    //     let categoryId = req.body.categoryId
    //         ? req.body.categoryId
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: errorMsgJSON[lang]["404"] + " category id."
    //             }
    //         });
    //     let gameType = req.body.gameType
    //         ? req.body.gameType
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: errorMsgJSON[lang]["404"] + " game type."
    //             }
    //         });
    //     let eventId = req.body.eventId
    //         ? req.body.eventId
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: errorMsgJSON[lang]["404"] + " event id."
    //             }
    //         });
    //     if (!req.body.categoryId || !req.body.gameType || !req.body.eventId) {
    //         return true;
    //     }
    //     try {

    //         Event.findOne({_id: eventId}, function (err, event) {
    //             if (err) {
    //                 res.json({
    //                     serverResponse: {
    //                         isError: true,
    //                         message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                         statusCode: 500
    //                     }
    //                 });
    //             } else {
    //                 let newEventGame = new EventGame({
    //                     gameType: gameType,
    //                     gameName: req.body.gameName,
    //                     description: req.body.description,
    //                     categoryId: categoryId,
    //                     gameId: req.body.gameId,
    //                     event: mongoose.Types.ObjectId(eventId),
    //                     shortDescription: req.body.shortDescription,
    //                     isEditable: req.body.isEditable,
    //                     isPublished: req.body.isPublished
    //                 });
    //                 if (event.type == 'free') {
    //                     if (req.body.rounds.length > 2) {
    //                         res.json({
    //                             serverResponse: {
    //                                 isError: true,
    //                                 message: "You have added more games than the limit. To add more games please upgrade your event.",
    //                                 statusCode: 404
    //                             }
    //                         });
    //                     } else {
    //                         newEventGame.save(function (err, item) {
    //                             if (err) {
    //                                 res.json({
    //                                     serverResponse: {
    //                                         isError: true,
    //                                         message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                         statusCode: 500
    //                                     }
    //                                 });
    //                             } else {
    //                                 if (item) {
    //                                     // let items = req.body.rounds ;
    //                                     // let newItems = [];
    //                                     // for (let i = 0; i < appSettings[0].maximumQuestionAllowed; i++) {
    //                                     //     let idx = Math.floor(Math.random() * items.length);
    //                                     //     newItems.push(items[idx]);
    //                                     //     items.splice(idx, 1);
    //                                     // }
    //                                     //        let filteredRounds = newItems.filter(function (el) {
    //                                     //            return el != null;
    //                                     //        });
    //                                     EventGame.updateMany(
    //                                         {_id: item.id},
    //                                         {$push: {"rounds": {$each: req.body.rounds}}}, function (err, response) {
    //                                             if (err) {
    //                                                 res.json({
    //                                                     serverResponse: {
    //                                                         isError: true,
    //                                                         message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                                         statusCode: 500
    //                                                     }
    //                                                 });
    //                                             } else {
    //                                                 res.json({
    //                                                     serverResponse: {
    //                                                         isError: false,
    //                                                         message: "Game successfully created.",
    //                                                         statusCode: 200
    //                                                     }
    //                                                 });
    //                                             }
    //                                         });
    //                                 } else {
    //                                     res.json({
    //                                         serverResponse: {
    //                                             isError: true,
    //                                             statusCode: 404,
    //                                             message: errorMsgJSON[lang]["404"]
    //                                         }
    //                                     });
    //                                 }

    //                             }
    //                         });
    //                     }
    //                 } else {
    //                     newEventGame.save(function (err, item) {
    //                         if (err) {
    //                             res.json({
    //                                 serverResponse: {
    //                                     isError: true,
    //                                     message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                     statusCode: 500
    //                                 }
    //                             });
    //                         } else {
    //                             if (item) {
    //                                 EventGame.updateMany(
    //                                     {_id: item.id},
    //                                     {$push: {"rounds": {$each: req.body.rounds}}}, function (err, response) {
    //                                         if (err) {
    //                                             res.json({
    //                                                 serverResponse: {
    //                                                     isError: true,
    //                                                     message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                                     statusCode: 500
    //                                                 }
    //                                             });
    //                                         } else {
    //                                             res.json({
    //                                                 serverResponse: {
    //                                                     isError: false,
    //                                                     message: "Game successfully created.",
    //                                                     statusCode: 200
    //                                                 }
    //                                             });
    //                                         }
    //                                     });
    //                             } else {
    //                                 res.json({
    //                                     serverResponse: {
    //                                         isError: true,
    //                                         statusCode: 404,
    //                                         message: errorMsgJSON[lang]["404"]
    //                                     }
    //                                 });
    //                             }

    //                         }
    //                     });
    //                 }
    //             }
    //         });
    //     } catch (e) {
    //         res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 message: errorMsgJSON[lang]["500"] + "- Error: " + e,
    //                 statusCode: 500
    //             }
    //         });
    //     }
    // },
    //#endregion for backup on 22 feb by subhendu

    createGameForEvent: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let categoryId = req.body.categoryId
            ? req.body.categoryId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " category id."
                }
            });
        let gameType = req.body.gameType
            ? req.body.gameType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " game type."
                }
            });
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event id."
                }
            });
        if (!req.body.categoryId || !req.body.gameType || !req.body.eventId) {
            return true;
        }

        let event = null;
        let eventGames = [];
        try {
            event = await Event.findOne({_id: eventId});
            eventGames = await EventGame.find({event: eventId});
            console.log('eventTitle',event.title);
            console.log('number of games',eventGames.length);
        } catch (e) {
            return res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error1: " + e,
                    statusCode: 500
                }
            });
        }    
        if (event.type == 'free'&&eventGames.length>=2) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "You have added more games than the limit. To add more games please upgrade your event.",
                    statusCode: 404
                }
            });
        } else {
            let newEventGame = new EventGame({
                gameType: gameType,
                gameName: req.body.gameName,
                description: req.body.description,
                categoryId: categoryId,
                gameId: req.body.gameId,
                event: mongoose.Types.ObjectId(eventId),
                shortDescription: req.body.shortDescription,
                isEditable: req.body.isEditable,
                isPublished: req.body.isPublished
            });
            newEventGame.save(function (err, item) {
                if (err) {
                    return res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error:2 " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (item) {
                        
                        EventGame.updateMany(
                            {_id: item.id},
                            {$push: {"rounds": {$each: req.body.rounds}}}, function (err, response) {
                                if (err) {
                                    return res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error:3 " + err,
                                            statusCode: 500
                                        }
                                    });
                                } else {
                                    return res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: "Game successfully created.",
                                            statusCode: 200
                                        }
                                    });
                                }
                            });
                    } else {
                        return res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: errorMsgJSON[lang]["404"]
                            }
                        });
                    }

                }
            });  
        }
    },

    eventWiseGameList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = mongoose.Types.ObjectId(req.body.eventId)
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });

        if (!req.body.eventId || !req.body.nibiId) {
            return true;
        }


        // const aggregate =([
        //     // Get only the documents where "email" equals "test@test.com" -- REPLACE with params.username
        //     {"$match" : {event : mongoose.Types.ObjectId(eventId)}},
        //     // Unwind the "inventories" array
        //     {"$unwind" : "$user"},
        //     // Get only elements where "inventories.title" equals "activeInventory"
        //     {"$match" : {"user.nibiId": nibiId}},
        //     // Tidy up the output
        //     {"$project" : {_id:1, gameType:1, user:"$user"}}
        // ])

        Event.findOne({_id: req.body.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event.guestList.length == 0) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "You are no longer participant in this event.",
                            statusCode: 410
                        },
                    });
                } else {
                    EventGame.find({event: eventId}).populate('event', 'title description').sort({addedAt: 'asc'}).exec(function (err, games) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            let allGame = games;

                            let newGame = [];
                            allGame.map(allGames => {
                                const items = allGames.user.filter(item => item.nibiId == nibiId);
                                if (items.length == 0) {
                                    allGames.user = [];
                                }
                                allGames.user = items
                                newGame.push(allGames)
                            });
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["200"],
                                    statusCode: 200
                                },
                                result: {
                                    gameDetails: newGame
                                }
                            });
                        }
                    });
                }
            }
        });

    },
    editEventGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventGameId = req.body.eventGameId
            ? req.body.eventGameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event game id."
                }
            });

        if (!req.body.eventGameId) {
            return true;
        }
        let findEventGame = {
            _id: eventGameId
        }
        delete req.body.eventGameId
        let updateValues = {
            $set: {
                ...req.body
            }
        };

        EventGame.findOne(findEventGame).populate('event', 'title description joinBefore').exec(function (err, eventGame) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (eventGame) {
                    if (eventGame.event.joinBefore > new Date()) {
                        if (eventGame.isPublished === false) {
                            if (eventGame.isEditable === true) {
                                EventGame.updateOne(findEventGame, updateValues, function (err, updatedEventGame) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        EventGame.findOne(findEventGame).populate('event', 'title description').exec(function (err, publishedGame) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        gameDetails: publishedGame
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Permission denied."
                                    }
                                });
                            }
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "You can't edit a published game."
                                }
                            });
                        }
                    } else {
                        if (eventGame.isEditable === true) {
                            if (eventGame.isPublished === false) {
                                EventGame.updateOne(findEventGame, updateValues, function (err, updatedEventGame) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        EventGame.findOne(findEventGame).populate('event', 'title description').exec(function (err, publishedGame) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        gameDetails: publishedGame
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "You can't edit your game at this time."
                                    }
                                });
                            }
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "Permission denied."
                                }
                            });
                        }
                    }
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Event game details not found."
                        }
                    });
                }
            }
        });
    },
    publishedGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventGameId = req.body.eventGameId
            ? req.body.eventGameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event game id."
                }
            });
        let isPublished = typeof (req.body.isPublished)
            ? req.body.isPublished
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide published status."
                }
            });

        if (!req.body.eventGameId || !typeof (req.body.isPublished)) {
            return true;
        }
        let findEventGame = {
            _id: eventGameId
        }
        let updateValues = {
            $set: {
                isPublished: isPublished
            }
        };
        EventGame.findOne(findEventGame).populate('event', 'title description joinBefore').exec(function (err, eventGame) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (eventGame) {
                    if (eventGame.event.joinBefore > new Date()) {
                        EventGame.updateOne(findEventGame, updateValues, function (err, updatedEventGame) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {
                                EventGame.findOne(findEventGame).populate('event', 'title description').exec(function (err, publishedGame) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: "Success",
                                                statusCode: 200
                                            },
                                            result: {
                                                gameDetails: publishedGame
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        if (isPublished == false) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "You can't disable your game at this time."
                                }
                            });
                        } else {
                            EventGame.updateOne(findEventGame, updateValues, function (err, updatedEventGame) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    EventGame.findOne(findEventGame).populate('event', 'title description').exec(function (err, publishedGame) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    message: "Success",
                                                    statusCode: 200
                                                },
                                                result: {
                                                    gameDetails: publishedGame
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }

                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Event game details not found."
                        }
                    });
                }
            }
        });
    },
    /*eventWiseGameListForUser: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = mongoose.Types.ObjectId(req.body.eventId)
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event id."
                }
            });

        if (!req.body.eventId) {
            return true;
        }
        EventGame.find({
            event: eventId,
            isPublished: true
        }).populate('event', 'title description').exec(function (err, games) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                res.json({
                    serverResponse: {
                        isError: false,
                        message: errorMsgJSON[lang]["200"],
                        statusCode: 200
                    },
                    result: {
                        gameDetails: games
                    }
                });
            }
        });
    }, */
    giveAnswer: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventGameId = req.body.eventGameId
            ? req.body.eventGameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event game id."
                }
            });
        let questionId = req.body.questionId
            ? req.body.questionId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide question id."
                }
            });
        if(!req.body.user || req.body.user.length == 0){
            res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide user details."
                }
            });
        }
        let findEventGame = {
            _id: eventGameId
        }
        if (!req.body.eventGameId || !req.body.questionId || !req.body.user) {
            return true;
        }
        let aggregate = ([
            {"$match": {"rounds._id": mongoose.Types.ObjectId(questionId)}},
            {
                "$addFields": {
                    "questionIndex": {
                        "$indexOfArray": [
                            "$rounds._id",
                            mongoose.Types.ObjectId(questionId)
                        ]
                    }
                }
            }
        ]);

        EventGame.findOne(findEventGame, function (err, gameAllDetails) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Event.findOne({_id: gameAllDetails.event}).select({guestList: {$elemMatch: {nibiId: req.body.user[0].nibiId}}}).exec(function (err, event) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        if (event.guestList.length == 0) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "You are no longer participant in this event.",
                                    statusCode: 410
                                },
                            });
                        } else {
                            EventGame.aggregate(aggregate, function (err, questionIndex) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    EventGame.findOne(findEventGame).select({'rounds': {$elemMatch: {_id: questionId}}}).exec(function (err, games) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            if (games) {
                                                let game = games.toJSON()
                                                let elements = (game.rounds[0].options);
                                                let element;

                                                if (gameAllDetails.gameType == "4" || gameAllDetails.gameType == "6") {

                                                    let answerText = req.body.answerText
                                                        ? req.body.answerText
                                                        : res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                statusCode: 404,
                                                                message: "Please provide answer text."
                                                            }
                                                        });
                                                    if (!req.body.answerText) {
                                                        return true;
                                                    }

                                                    element = elements.filter(options => {
                                                        return options.text.toLowerCase() == answerText.toLowerCase()
                                                    });
                                                } else {
                                                    let answerId = req.body.answerId
                                                        ? req.body.answerId
                                                        : res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                statusCode: 404,
                                                                message: "Please provide answer id."
                                                            }
                                                        });
                                                    if (!req.body.answerId) {
                                                        return true;
                                                    }
                                                    element = elements.filter(options => {
                                                        return options._id == answerId && options.isCorrect == true
                                                    });
                                                }
                                                EventGame.findOne(findEventGame).select({'user': {$elemMatch: {nibiId: req.body.user[0].nibiId}}}).exec(function (err, user) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        if (user.user.length > 0) {        // If previous any question answered and nibiId exist
                                                            if (element.length > 0) {

                                                                EventGame.updateOne({
                                                                    '_id': eventGameId,
                                                                    "user.nibiId": req.body.user[0].nibiId
                                                                }, {
                                                                    $set: {
                                                                        "user.$.numberOfCorrectAnswer": user.user[0].numberOfCorrectAnswer + 1,
                                                                        "user.$.score": user.user[0].score + 1,
                                                                        "user.$.currentRounds": questionIndex[0].questionIndex + 1
                                                                    }
                                                                }, function (err, updateResult) {

                                                                    if (err) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: true,
                                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                statusCode: 500
                                                                            },
                                                                        });
                                                                    } else {
                                                                        if (questionIndex[0].questionIndex + 1 == gameAllDetails.rounds.length) {

                                                                            EventGame.updateOne({
                                                                                '_id': eventGameId,
                                                                                "user.nibiId": req.body.user[0].nibiId
                                                                            }, {$set: {"user.$.answeredAllQuestion": true}}, function (err, updateResult) {

                                                                                if (err) {
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: true,
                                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                            statusCode: 500
                                                                                        },
                                                                                    });
                                                                                } else {
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: false,
                                                                                            message: errorMsgJSON[lang]["200"],
                                                                                            statusCode: 200
                                                                                        },
                                                                                        result: {
                                                                                            gameDetails: games
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        } else {
                                                                            res.json({
                                                                                serverResponse: {
                                                                                    isError: false,
                                                                                    message: errorMsgJSON[lang]["200"],
                                                                                    statusCode: 200
                                                                                }
                                                                            });
                                                                        }

                                                                    }
                                                                });
                                                            } else {
                                                                EventGame.updateOne({
                                                                    '_id': eventGameId,
                                                                    "user.nibiId": req.body.user[0].nibiId
                                                                }, {$set: {"user.$.currentRounds": questionIndex[0].questionIndex + 1}}, function (err, updateResult) {

                                                                    if (err) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: true,
                                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                statusCode: 500
                                                                            },
                                                                        });
                                                                    } else {
                                                                        if (questionIndex[0].questionIndex + 1 == gameAllDetails.rounds.length) {

                                                                            EventGame.updateOne({
                                                                                '_id': eventGameId,
                                                                                "user.nibiId": req.body.user[0].nibiId
                                                                            }, {$set: {"user.$.answeredAllQuestion": true}}, function (err, updateResult) {

                                                                                if (err) {
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: true,
                                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                            statusCode: 500
                                                                                        },
                                                                                    });
                                                                                } else {
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: false,
                                                                                            message: errorMsgJSON[lang]["200"],
                                                                                            statusCode: 200
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        } else {
                                                                            res.json({
                                                                                serverResponse: {
                                                                                    isError: false,
                                                                                    message: errorMsgJSON[lang]["200"],
                                                                                    statusCode: 200
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        } else {
                                                            if (element.length > 0) {
                                                                req.body.user[0].score = 1;
                                                                req.body.user[0].numberOfCorrectAnswer = 1;
                                                            }
                                                            req.body.user[0].totalScore = gameAllDetails.rounds.length;
                                                            req.body.user[0].totalNumberOfQuestion = gameAllDetails.rounds.length
                                                            EventGame.updateOne(findEventGame,
                                                                {$push: {"user": req.body.user}}, function (err, updatedData) {
                                                                    if (err) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: true,
                                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                statusCode: 500
                                                                            },
                                                                        });
                                                                    } else {
                                                                        EventGame.updateOne({
                                                                            '_id': eventGameId,
                                                                            "user.nibiId": req.body.user[0].nibiId
                                                                        }, {$set: {"user.$.currentRounds": questionIndex[0].questionIndex + 1}}, function (err, updateResult) {
                                                                            if (err) {
                                                                                res.json({
                                                                                    serverResponse: {
                                                                                        isError: true,
                                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                        statusCode: 500
                                                                                    },
                                                                                });
                                                                            } else {
                                                                                if (questionIndex[0].questionIndex + 1 == gameAllDetails.rounds.length) {

                                                                                    EventGame.updateOne({
                                                                                        '_id': eventGameId,
                                                                                        "user.nibiId": req.body.user[0].nibiId
                                                                                    }, {$set: {"user.$.answeredAllQuestion": true}}, function (err, updateResult) {

                                                                                        if (err) {
                                                                                            res.json({
                                                                                                serverResponse: {
                                                                                                    isError: true,
                                                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                                    statusCode: 500
                                                                                                },
                                                                                            });
                                                                                        } else {
                                                                                            res.json({
                                                                                                serverResponse: {
                                                                                                    isError: false,
                                                                                                    message: errorMsgJSON[lang]["200"],
                                                                                                    statusCode: 200
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: false,
                                                                                            message: errorMsgJSON[lang]["200"],
                                                                                            statusCode: 200
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                        }
                                                    }
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        statusCode: 404,
                                                        message: errorMsgJSON[lang]["404"]
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        });
    },
    deleteEventGame: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventGameId = req.body.eventGameId
            ? req.body.eventGameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event game id."
                }
            });

        if (!req.body.eventGameId) {
            return true;
        }
        let findEventGame = {
            _id: eventGameId
        }
        EventGame.findOne(findEventGame).populate('event', 'title description joinBefore').exec(function (err, eventGame) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (eventGame) {
                    if (eventGame.event.joinBefore > new Date()) {
                        if (eventGame.isPublished == false) {
                            EventGame.deleteOne(findEventGame, function (err, deletedEventGame) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: "Game successfully deleted.",
                                            statusCode: 200
                                        }
                                    });
                                }
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "You can't delete a published game."
                                }
                            });
                        }
                    } else {

                        if (eventGame.isPublished == false) {
                            EventGame.deleteOne(findEventGame, function (err, deletedEventGame) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        }
                                    });
                                }
                            });
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "You can't delete your game at this time."
                                }
                            });
                        }
                    }
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Event game details not found."
                        }
                    });
                }
            }
        });
    },
    gameWiseUsersScore: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventGameId = req.body.eventGameId
            ? req.body.eventGameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event game id."
                }
            });

        if (!req.body.eventGameId) {
            return true;
        }
        EventGame.findOne({_id: eventGameId}).select('gameName user').exec(function (err, userScore) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (userScore) {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            userScore: userScore
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: errorMsgJSON[lang]["404"]
                        }
                    });
                }

            }
        });
    },

    specificUserScore: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventGameId = req.body.eventGameId
            ? req.body.eventGameId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event game id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });
        if (!req.body.eventGameId || !req.body.nibiId) {
            return true;
        }
        EventGame.findOne({_id: eventGameId}).select({'user': {$elemMatch: {nibiId: nibiId}}}).exec(function (err, userScore) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (userScore) {
                    EventGame.findOne({_id: eventGameId}, function (err, gameDetails) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            Event.findOne({_id: gameDetails.event}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    if (event.guestList.length == 0) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: "You are no longer participant in this event.",
                                                statusCode: 410
                                            },
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: errorMsgJSON[lang]["200"],
                                                statusCode: 200
                                            },
                                            result: {
                                                gameName: gameDetails.gameName,
                                                userScore: userScore
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    })
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: errorMsgJSON[lang]["404"]
                        }
                    });
                }

            }
        });
    },

    saveAppSettingsInfo: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        try {
            let newAppSettings = new AppSettings({
                maximumQuestionAllowed: req.body.maximumQuestionAllowed
            });
            await newAppSettings.save(function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            appSettingsData: item
                        }
                    });
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    updateAppSettingsInfo: function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let appSettingsId = req.body.appSettingsId
            ? req.body.appSettingsId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide app settings id."
                }
            });
        if (!req.body.appSettingsId) {
            return true;
        }
        let query = {
            _id: appSettingsId
        };

        AppSettings.findOne(query, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    delete req.body.appSettingsId;
                    let updateValues = {
                        $set: {
                            ...req.body
                        }
                    };
                    AppSettings.updateOne(query, updateValues, function (err, res1) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            AppSettings.findOne(query, function (err, updatedItem) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            appSettingsData: updatedItem
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    }

};
