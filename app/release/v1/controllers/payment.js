const mongoose = require("mongoose");
const errorMsgJSON = require("../../../services/errors.json");
const Env = require("../../../../env");
const User = require("../models/user");
const Plan = require("../models/plan");
const Event = require("../models/event");
const Transaction = require("../models/transaction");
const UserPoints = require("../models/user_points");
const stripe = require('stripe')(Env.SECRET_KEY[Env.MODE])
module.exports = {
    createPlan: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let planName = req.body.planName
            ? req.body.planName
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " plan name."
                }
            });
        if (!req.body.planName) {
            return true;
        }
        try {
            let newPlan = new Plan({
                planId: req.body.planId,
                planName: planName,
                amount: req.body.amount,
                currency: req.body.currency,
                client: req.body.client,
                transactionType: req.body.transactionType,
                description: req.body.description,
                features: req.body.features
            });
            await newPlan.save(function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (item) {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                planDetails: item
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: errorMsgJSON[lang]["404"]
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    planList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let findPlan = {};
        if (req.body.client) {
            findPlan.client = req.body.client;
        }
        Plan.find(findPlan, function (err, plan) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (plan.length == 0) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            planList: plan
                        }
                    });
                }
            }
        });
    },
    createPaymentRequest: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let email = req.body.email
            ? req.body.email
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " email."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibiId."
                }
            });
        let stripeToken = req.body.stripeToken
            ? req.body.stripeToken
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " stripeToken."
                }
            });
        let planId = req.body.planId
            ? req.body.planId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " plan id."
                }
            });
        if (!req.body.email || !req.body.nibiId || !req.body.stripeToken || !req.body.planId) {
            return true;
        }
        const customer = await stripe.customers.list({
            email: email,
        });

        Plan.findOne({planId: planId}, function (err, plan) {
            if (plan) {

                if (customer.data.length > 0) {
                    stripe.charges.create({
                        amount: (plan.amount) * 100,
                        description: plan.description,
                        currency: plan.currency,
                        customer: customer.data[0].id,
                        metadata: {'nibiId': nibiId}
                    }).then((charge) => {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                data: charge
                            }
                        });
                    }).catch((err) => {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 404
                            },
                            error: {
                                errorData: err
                            }
                        });
                    });
                } else {
                    stripe.customers.create({
                        email: email,
                        source: stripeToken,
                        name: req.body.name,
                        metadata: {'nibiId': nibiId},
                        address: {
                            line1: '',
                            postal_code: '',
                            city: '',
                            state: '',
                            country: '',
                        }
                    }).then((customer) => {
                        return stripe.charges.create({
                            amount: (plan.amount) * 100,
                            description: plan.description,
                            currency: plan.currency,
                            customer: customer.id,
                            metadata: {'nibiId': req.body.nibiId}
                        });
                    })
                        .then((charge) => {
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["200"],
                                    statusCode: 200
                                },
                                result: {
                                    data: charge
                                }
                            });
                        })
                        .catch((err) => {
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["404"],
                                    statusCode: 404
                                },
                                error: {
                                    errorData: err
                                }
                            });
                        });
                }
            } else {
                res.json({
                    serverResponse: {
                        isError: false,
                        message: "Unable to fetch plan details.",
                        statusCode: 404
                    }
                });
            }
        });

    },
    saveTransactionData: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let transactionId = req.body.transactionId
            ? req.body.transactionId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " transaction id."
                }
            });
        let transactionType = req.body.transactionType
            ? req.body.transactionType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " transaction type."
                }
            });
        let amount = req.body.amount
            ? req.body.amount
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " amount."
                }
            });
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event id."
                }
            });
        let userId = req.body.userId
            ? req.body.userId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " user id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let planId = req.body.planId
            ? req.body.planId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " plan id."
                }
            });
        let client = req.body.client
            ? req.body.client
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " client."
                }
            });
        if (!req.body.transactionId || !req.body.transactionType || !req.body.amount || !req.body.eventId || !req.body.nibiId || !req.body.client || !req.body.planId || !req.body.userId) {
            return true;
        }
        try {
            Plan.findOne({planId: planId}, function (err, plan) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (plan) {
                        let newTransaction = new Transaction({
                            transactionId: transactionId,
                            transactionType: transactionType,
                            amount: plan.amount,
                            currency: req.body.currency,
                            client: client,
                            eventId: eventId,
                            user: mongoose.Types.ObjectId(userId),
                            nibiId: nibiId,
                            description: amount
                        });
                        newTransaction.save(function (err, item) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {
                                if (item) {
                                    Event.updateOne({_id: eventId}, {
                                        transactionId: transactionId,
                                        transactionType: transactionType,
                                        amount: plan.amount
                                    }, function (err, updateEvent) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500,
                                                }
                                            });
                                        } else {
                                            if (transactionType == 'points') {
                                                UserPoints.updateOne({_id: transactionId}, {
                                                    eventId: eventId,
                                                }, function (err, updateUserPoints) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500,
                                                            }
                                                        });
                                                    } else {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                transactionData: item
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        transactionData: item
                                                    }
                                                });
                                            }

                                        }
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            statusCode: 404,
                                            message: errorMsgJSON[lang]["404"]
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: "Plan details not found."
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    redeemUserPoints: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let planId = req.body.planId
            ? req.body.planId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " plan id."
                }
            });
        let client = req.body.client
            ? req.body.client
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " client."
                }
            });
        if (!req.body.nibiId || !req.body.client || !req.body.planId) {
            return true;
        }
        try {
            Plan.findOne({planId: planId}, function (err, plan) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (plan) {
                        User.findOne({nibiId: nibiId}, function (err, user) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {
                                if (user) {
                                    if (user.refundPoints >= plan.amount) {
                                        let newUserPoints = new UserPoints({
                                            nibiId: nibiId,
                                            points: plan.amount,
                                            client: client,
                                            status: "subtracted",
                                        });
                                        newUserPoints.save(function (err, newUserPoint) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {
                                                if (newUserPoint) {
                                                    let userPointAfterRedeem = (user.refundPoints - plan.amount).toFixed(2);
                                                    User.updateOne({"nibiId": nibiId}, {refundPoints: userPointAfterRedeem}, function (err, updateUserPoint) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                }
                                                            });
                                                        } else {
                                                            User.findOne({nibiId: nibiId}, function (err, userUpdatePoints) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        }
                                                                    });
                                                                } else {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: errorMsgJSON[lang]["200"],
                                                                            statusCode: 200
                                                                        },
                                                                        result: {
                                                                            transactionId: newUserPoint._id,
                                                                            transactionType: 'points',
                                                                            currency: 'points',
                                                                            amount: plan.amount,
                                                                            refundPoints: userUpdatePoints.refundPoints
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            statusCode: 404,
                                                            message: "Plan details not saved due to technical issue."
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                statusCode: 404,
                                                message: "You have no sufficient points to upgrade the event."
                                            }
                                        });
                                    }
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            statusCode: 404,
                                            message: "User details not found."
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: "Plan details not found."
                            }
                        });
                    }
                }
            });

        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    addPromoPoints: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let points = req.body.points
            ? req.body.points
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " points."
                }
            });
        if (!req.body.nibiId || !req.body.points) {
            return true;
        }
        try {
            User.findOne({nibiId: nibiId}, function (err, user) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (user) {
                        let updatedRefundPoints = user.refundPoints + points;
                        User.updateOne({nibiId: nibiId}, {refundPoints: updatedRefundPoints}, function (err, refundPoints) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {
                                User.findOne({nibiId: nibiId}, function (err, updatedUser) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            }
                                        });
                                    } else {
                                        res.json({
                                            serverResponse: {
                                                isError: false,
                                                message: errorMsgJSON[lang]["200"],
                                                statusCode: 200
                                            },
                                            result: {
                                                userDetails: updatedUser
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: "User details not found."
                            }
                        });
                    }
                }
            });

        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    transactionList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let limit = 25;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let page = 1;
        page = (page - 1) * limit;
        if (req.body.page) {
            page = (req.body.page - 1) * limit;
        }

        Transaction.find({}).countDocuments().exec(function (err, transactionCount) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Transaction.find({}).populate('user').limit(limit).skip(page).sort({'createdAt': 'desc'}).exec(function (err, transaction) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                totalTransaction: transactionCount,
                                transactionList: transaction
                            }
                        });
                    }
                });
            }
        });
    },
};
