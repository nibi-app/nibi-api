const mongoose = require("mongoose");
const Post = require("../models/post");
const Event = require("../models/event");
const Env = require("../../../../env");
const Notification = require("../models/notification");
const NotificationService = require("../../../services/notification");
const User = require("../models/user");
const Polling = require("../models/polling");
const errorMsgJSON = require("../../../services/errors.json");
const moment = require('moment');
const AWS = require('aws-sdk');
const {v4: uuidv4} = require('uuid');

const s3 = new AWS.S3({
    accessKeyId: Env.BUCKET[Env.MODE].accessKeyId,
    secretAccessKey: Env.BUCKET[Env.MODE].secretAccessKey,
    Bucket: Env.BUCKET[Env.MODE].Bucket
});


module.exports = {

    //--------------------------------------------------------------------------------------------------------------------
    eventWisePost: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        // let page = req.body.page
        //     ? req.body.page
        //     : res.json({
        //         serverResponse: {
        //             isError: true,
        //             statusCode: 404,
        //             message: "Please provide page number."
        //         }
        //     });

        let page = req.body.page ? (parseInt(req.body.page == 0 ? 0 : parseInt(req.body.page) - 1)) : 0;

        if (!req.body.eventId || !req.body.nibiId || !req.body.page) {
            return true;
        }
        let limit = 10;
        let skip = (page - 1) * limit;
        let findPost = {
            eventId: eventId,
            isDeleted: false
        };


        Event.findOne({_id: eventId}).select('coHosts').exec(function (err, coHost) {

            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Event.findOne({_id: eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {


                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        if (event.guestList.length == 0) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: "You are no longer participant in this event.",
                                    statusCode: 410
                                },
                            });
                        } else {


                            Post.find(findPost).countDocuments().exec(function (err, count) {

                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    let aggrQuery = [
                                        {
                                            $match: {
                                                'eventId': eventId,
                                                'isDeleted': false
                                            },
                                        },


                                        {
                                            $project: {
                                                'userScore': 1,
                                                'eventId': 1,
                                                'isDeleted': 1,
                                                'nibiId': 1,
                                                'hostNibiId': 1,
                                                'comment': 1,
                                                'postType': 1,
                                                'coHosts': 1,
                                                'sourceFile': 1,
                                                'mention': 1,
                                                'reply': 1,
                                                'like': 1,
                                                'addedAt': 1,
                                                'convertedId': {
                                                    $toString: '$_id'
                                                },
                                                'user': 1,
                                                'viewResultToAllGuest': 1,
                                                'questionsForPoll': 1,
                                                'options': 1
                                            },
                                        },

                                        {
                                            $lookup:
                                                {
                                                    from: 'pollings',
                                                    localField: 'convertedId',
                                                    foreignField: 'postId',
                                                    as: 'PostDetail'
                                                }
                                        },

                                        {
                                            $project: {
                                                'userScore': 1,
                                                'eventId': 1,
                                                'isDeleted': 1,
                                                'nibiId': 1,
                                                'hostNibiId': 1,
                                                'comment': 1,
                                                'postType': 1,
                                                'coHosts': 1,
                                                'sourceFile': 1,
                                                'mention': 1,
                                                'reply': 1,
                                                'like': 1,
                                                'addedAt': 1,
                                                'convertedId': 1,
                                                'user': 1,
                                                'PostDetail': 1,
                                                'postSize': {
                                                    $size: "$PostDetail"
                                                },
                                                'viewResultToAllGuest': 1,
                                                'questionsForPoll': 1,
                                                'options': 1
                                            },
                                        },

                                        {
                                            $unwind: {
                                                path: "$PostDetail",
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },

                                        {
                                            $group:
                                                {
                                                    _id: {
                                                        'convertedId': '$convertedId',
                                                        'optionId': '$PostDetail.optionId'
                                                    },
                                                    'countBy': {$sum: 1},
                                                    'userScore': {'$first': '$userScore'},
                                                    'eventId': {'$first': '$eventId'},
                                                    'isDeleted': {'$first': '$isDeleted'},
                                                    'nibiId': {'$first': '$nibiId'},
                                                    'hostNibiId': {'$first': '$hostNibiId'},
                                                    'comment': {'$first': '$comment'},
                                                    'postType': {'$first': '$postType'},
                                                    'coHosts': {'$first': '$coHosts'},
                                                    'sourceFile': {'$first': '$sourceFile'},
                                                    'mention': {'$first': '$mention'},
                                                    'reply': {'$first': '$reply'},
                                                    'like': {'$first': '$like'},
                                                    'addedAt': {'$first': '$addedAt'},
                                                    'user': {'$first': '$user'},
                                                    'PostDetail': {'$first': '$PostDetail'},
                                                    'postSize': {'$first': '$postSize'},
                                                    'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                    'questionsForPoll': {'$first': '$questionsForPoll'},
                                                    'options': {'$first': '$options'}
                                                }
                                        },

                                        {
                                            $project:
                                                {
                                                    '_id': 0,
                                                    'convertedId': '$_id.convertedId',
                                                    'optionId': '$_id.optionId',
                                                    'countBy': 1,
                                                    'optionArrDet': {
                                                        'optionId': '$_id.optionId',
                                                        'countBy': '$countBy',
                                                        'optionName': '$PostDetail.optionName',
                                                        'optionIndex': '$PostDetail.optionIndex',
                                                        'percentageOfVote': {
                                                            $cond: {
                                                                if: {
                                                                    $eq: ["$postSize", 0]
                                                                },
                                                                then: 0,
                                                                else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                            }
                                                        },
                                                        'voteByUser': {
                                                            $cond: {
                                                                if: {
                                                                    $eq: [nibiId, '$PostDetail.nibiId']
                                                                },
                                                                then: true,
                                                                else: false
                                                            }
                                                        }
                                                    },

                                                    'userScore': 1,
                                                    'eventId': 1,
                                                    'isDeleted': 1,
                                                    'nibiId': 1,
                                                    'hostNibiId': 1,
                                                    'comment': 1,
                                                    'postType': 1,
                                                    'coHosts': 1,
                                                    'sourceFile': 1,
                                                    'mention': 1,
                                                    'reply': 1,
                                                    'like': 1,
                                                    'addedAt': 1,
                                                    'user': 1,
                                                    // 'PostDetail': 1,
                                                    'postSize': 1,
                                                    'viewResultToAllGuest': 1,
                                                    'questionsForPoll': 1,
                                                    'options': 1
                                                }
                                        },

                                        {
                                            $group:
                                                {
                                                    _id: {
                                                        'convertedId': '$convertedId'
                                                    },
                                                    'optionArrDet': {'$push': '$optionArrDet'},
                                                    'userScore': {'$first': '$userScore'},
                                                    'eventId': {'$first': '$eventId'},
                                                    'isDeleted': {'$first': '$isDeleted'},
                                                    'nibiId': {'$first': '$nibiId'},
                                                    'hostNibiId': {'$first': '$hostNibiId'},
                                                    'comment': {'$first': '$comment'},
                                                    'postType': {'$first': '$postType'},
                                                    'coHosts': {'$first': '$coHosts'},
                                                    'sourceFile': {'$first': '$sourceFile'},
                                                    'mention': {'$first': '$mention'},
                                                    'reply': {'$first': '$reply'},
                                                    'like': {'$first': '$like'},
                                                    'addedAt': {'$first': '$addedAt'},
                                                    'user': {'$first': '$user'},
                                                    'postSize': {'$first': '$postSize'},
                                                    'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                    'questionsForPoll': {'$first': '$questionsForPoll'},
                                                    'options': {'$first': '$options'}
                                                }
                                        },


                                        {
                                            $project:
                                                {
                                                    '_id': '$_id.convertedId',
                                                    'convertedId': 1,
                                                    'optionArrDet':
                                                        {
                                                            $cond: {
                                                                if: {
                                                                    $eq: ["$postSize", 0]
                                                                },
                                                                then: [],
                                                                else: '$optionArrDet'
                                                            }
                                                        },
                                                    // 'voteByUser':
                                                    //     {
                                                    //         $cond: {
                                                    //             if: {
                                                    //                 $eq: [nibiId, "$PostDetail.nibiId"]
                                                    //             },
                                                    //             then: true,
                                                    //             else: false
                                                    //         }
                                                    //     },
                                                    'userScore': 1,
                                                    'eventId': 1,
                                                    'isDeleted': 1,
                                                    'nibiId': 1,
                                                    'hostNibiId': 1,
                                                    'comment': 1,
                                                    'postType': 1,
                                                    'coHosts': 1,
                                                    'sourceFile': 1,
                                                    'mention': 1,
                                                    'reply': 1,
                                                    'like': 1,
                                                    'addedAt': 1,
                                                    'user': 1,
                                                    'postSize': 1,
                                                    'viewResultToAllGuest': 1,
                                                    'questionsForPoll': 1,
                                                    'options': 1
                                                }
                                        },

                                        {
                                            $lookup:
                                                {
                                                    from: 'users',
                                                    localField: 'user',
                                                    foreignField: '_id',
                                                    as: 'userDetail'
                                                }
                                        },

                                        {
                                            $project:
                                                {
                                                    '_id': 1,
                                                    'convertedId': '$_id.convertedId',
                                                    'optionArrDet':
                                                        {
                                                            $cond: {
                                                                if: {
                                                                    $eq: ["$postSize", 0]
                                                                },
                                                                then: [],
                                                                else: '$optionArrDet'
                                                            }
                                                        },
                                                    'userScore': 1,
                                                    'eventId': 1,
                                                    'isDeleted': 1,
                                                    'nibiId': 1,
                                                    'hostNibiId': 1,
                                                    'comment': 1,
                                                    'postType': 1,
                                                    'coHosts': 1,
                                                    'sourceFile': 1,
                                                    'mention': 1,
                                                    'reply': 1,
                                                    'like': 1,
                                                    'addedAt': 1,
                                                    'userDetail': {$arrayElemAt: ['$userDetail', 0]},
                                                    'postSize': 1,
                                                    'viewResultToAllGuest': 1,
                                                    'questionsForPoll': 1,
                                                    'options': 1
                                                }
                                        },

                                        {
                                            $project:
                                                {
                                                    'convertedId': 1,
                                                    'optionArrDet': 1,
                                                    'userScore': 1,
                                                    'eventId': 1,
                                                    'isDeleted': 1,
                                                    'nibiId': 1,
                                                    'hostNibiId': 1,
                                                    'comment': 1,
                                                    'postType': 1,
                                                    'coHosts': 1,
                                                    'sourceFile': 1,
                                                    'mention': 1,
                                                    'reply': 1,
                                                    'like': 1,
                                                    'addedAt': 1,
                                                    'user': {
                                                        'fname': '$userDetail.fname',
                                                        'lname': '$userDetail.lname',
                                                        'profilePic': '$userDetail.profilePic',
                                                        '_id': '$userDetail._id',
                                                    },
                                                    'postSize': 1,
                                                    'viewResultToAllGuest': 1,
                                                    'questionsForPoll': 1,
                                                    'options': 1
                                                }
                                        },

                                        {$sort: {addedAt: -1}},

                                        {
                                            $group: {
                                                _id: null,
                                                total: {
                                                    $sum: 1
                                                },
                                                results: {
                                                    $push: '$$ROOT'
                                                }
                                            }
                                        },
                                        {
                                            $project: {
                                                '_id': 0,
                                                'total': 1,
                                                'noofpage': {$ceil: {$divide: ["$total", limit]}},
                                                'postDetails': {
                                                    $slice: [
                                                        '$results', page * limit, limit
                                                    ]
                                                }
                                            }
                                        }

                                    ];
                                    // Post.find(findPost).populate("user", "fname lname profilePic").skip(skip).limit(limit).sort({addedAt: "desc"}).exec(function (err, items) {
                                    Post.aggregate(aggrQuery,
                                        function (err, response) {

                                            if (err) {

                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                                return;
                                            } else {

                                                if (response.length > 0) {

                                                    let postDetails = response[0].postDetails;

                                                    let i;
                                                    let j;
                                                    let optionArrDet = [];
                                                    let options;
                                                    let OptionIdListInoptionArrDet = [];
                                                    let dataToInsert = {};

                                                    for (i = 0; i < postDetails.length; i++) {

                                                        optionArrDet = postDetails[i].optionArrDet;
                                                        options = postDetails[i].options;

                                                        OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                                                            return item.optionId;
                                                        })
                                                        //
                                                        for (j = 0; j < options.length; j++) {
                                                            if (!OptionIdListInoptionArrDet.includes(options[j]._id.toString())) {
                                                                dataToInsert = {
                                                                    "optionId": options[j]._id.toString(),
                                                                    "countBy": 0,
                                                                    "optionName": options[j].optionName,
                                                                    "optionIndex": options[j].optionIndex,
                                                                    "percentageOfVote": 0,
                                                                    "voteByUser": false
                                                                }

                                                                optionArrDet.push(dataToInsert)
                                                            }
                                                        } // END for (j = 0; j < options.length; j++) {

                                                        postDetails[i].optionArrDet = optionArrDet;
                                                        postDetails[i].options = options;

                                                    } // END for (i = 0; i < postDetails.length; i++) {

                                                    response[0].postDetails = postDetails;
                                                    let items = response[0].postDetails;
                                                    // res.json({
                                                    //     response: response,
                                                    //     // items: items
                                                    // });
                                                    // return;

                                                    // items = items.map(item => {
                                                    //     let itemJson = item.toJSON();
                                                    //     itemJson.timeDifference = moment(itemJson.addedAt).fromNow(true);
                                                    //     itemJson.isLiked = itemJson.like.find(like => {
                                                    //         return like.nibiId === nibiId
                                                    //     }) !== undefined;
                                                    //     return itemJson;
                                                    // });
                                                    let item = [];

                                                    items.map((post) => {
                                                        let posts = post;
                                                        // res.json({
                                                        //     serverResponse: {
                                                        //         isError: false,
                                                        //         message: errorMsgJSON[lang]["200"],
                                                        //         statusCode: 200
                                                        //     },
                                                        //     result:
                                                        //         // totalPost: count,
                                                        //         response
                                                        //     ,
                                                        // });
                                                        posts.timeDifference = moment(posts.addedAt).fromNow(true);
                                                        posts.isLiked = posts.like.find(like => {
                                                            return like.nibiId === nibiId
                                                        }) !== undefined;
                                                        posts.likeCount = posts.like.length;
                                                        // item.push(posts);
                                                        let newReply = [];
                                                        posts.reply.map(reply => {
                                                            let replies = reply;
                                                            replies.likeCountOnReply = replies.likeOnReply.length;
                                                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                return likeOnReply.nibiId === nibiId
                                                            }) !== undefined;
                                                            let newReplyOnReply = [];
                                                            reply.replyOnReply.forEach(replyOnReply => {
                                                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length;
                                                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                    return likeReplyOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                newReplyOnReply.push(replyOnReply);
                                                            });
                                                            replies.replyOnReply = newReplyOnReply
                                                            newReply.push(replies)
                                                        });
                                                        posts.reply = newReply;
                                                        item.push(posts);

                                                    });
                                                    res.json({
                                                        serverResponse: {
                                                            isError: false,
                                                            message: errorMsgJSON[lang]["200"],
                                                            statusCode: 200
                                                        },
                                                        result: {
                                                            totalPost: response[0].total,
                                                            noOfPage: response[0].noofpage,
                                                            postDetails: item,
                                                            coHost
                                                        },
                                                    });
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: false,
                                                            message: errorMsgJSON[lang]["200"],
                                                            statusCode: 200
                                                        },
                                                        result: {
                                                            totalPost: 0,
                                                            noOfPage: 1,
                                                            postDetails: [],
                                                            coHost
                                                        },
                                                    });
                                                }

                                            }
                                        });
                                }
                            })
                        }
                    }
                });
            }
        });
    },

    //--------------------------------------------------------------------------------------------------------------------

    voteOnPoll: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        try {
            let postId = req.body.postId
                ? req.body.postId
                : res.json({
                    serverResponse: {
                        isError: true,
                        statusCode: 404,
                        message: errorMsgJSON[lang]["404"] + " postId"
                    }
                });

            let userId = req.body.userId
                ? req.body.userId
                : res.json({
                    serverResponse: {
                        isError: true,
                        statusCode: 404,
                        message: errorMsgJSON[lang]["404"] + " User "
                    }
                });
            let nibiId = req.body.nibiId
                ? req.body.nibiId
                : res.json({
                    serverResponse: {
                        isError: true,
                        statusCode: 404,
                        message: errorMsgJSON[lang]["404"] + " nibiId "
                    }
                });
            let optionName = req.body.optionName
                ? req.body.optionName
                : res.json({
                    serverResponse: {
                        isError: true,
                        statusCode: 404,
                        message: errorMsgJSON[lang]["404"] + " optionName"
                    }
                });

            let optionId = req.body.optionId
                ? req.body.optionId
                : res.json({
                    serverResponse: {
                        isError: true,
                        statusCode: 404,
                        message: errorMsgJSON[lang]["404"] + " optionId"
                    }
                });

            let optionIndex = req.body.optionIndex
                ? req.body.optionIndex
                : res.json({
                    serverResponse: {
                        isError: true,
                        statusCode: 404,
                        message: errorMsgJSON[lang]["404"] + " optionIndex"
                    }
                });
            if (!req.body.postId || !req.body.userId || !req.body.nibiId || !req.body.optionName || !req.body.optionId || !req.body.optionIndex) {
                return true;
            }

            Polling.findOne({postId: postId, nibiId: nibiId}, function (err, poll) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (poll) {
                        Polling.updateOne({
                            postId: postId,
                            nibiId: nibiId
                        }, {
                            optionId: optionId,
                            optionName: optionName,
                            optionIndex: optionIndex
                        }, function (err, updatePoll) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {

                                //=================================================================
                                let aggrQuery = [
                                    {
                                        $match: {
                                            '_id': mongoose.Types.ObjectId(postId),
                                        },
                                    },


                                    {
                                        $project: {
                                            'userScore': 1,
                                            'eventId': 1,
                                            'isDeleted': 1,
                                            'nibiId': 1,
                                            'hostNibiId': 1,
                                            'comment': 1,
                                            'postType': 1,
                                            'coHosts': 1,
                                            'sourceFile': 1,
                                            'mention': 1,
                                            'reply': 1,
                                            'like': 1,
                                            'addedAt': 1,
                                            'convertedId': {
                                                $toString: '$_id'
                                            },
                                            'user': 1,
                                            'viewResultToAllGuest': 1,
                                            'questionsForPoll': 1,
                                            'options': 1
                                        },
                                    },

                                    {
                                        $lookup:
                                            {
                                                from: 'pollings',
                                                localField: 'convertedId',
                                                foreignField: 'postId',
                                                as: 'PostDetail'
                                            }
                                    },

                                    {
                                        $project: {
                                            'userScore': 1,
                                            'eventId': 1,
                                            'isDeleted': 1,
                                            'nibiId': 1,
                                            'hostNibiId': 1,
                                            'comment': 1,
                                            'postType': 1,
                                            'coHosts': 1,
                                            'sourceFile': 1,
                                            'mention': 1,
                                            'reply': 1,
                                            'like': 1,
                                            'addedAt': 1,
                                            'convertedId': 1,
                                            'user': 1,
                                            'PostDetail': 1,
                                            'postSize': {
                                                $size: "$PostDetail"
                                            },
                                            'viewResultToAllGuest': 1,
                                            'questionsForPoll': 1,
                                            'options': 1
                                        },
                                    },

                                    {
                                        $unwind: {
                                            path: "$PostDetail",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },


                                    {
                                        $group:
                                            {
                                                _id: {
                                                    'convertedId': '$convertedId',
                                                    'optionId': '$PostDetail.optionId'
                                                },
                                                'countBy': {$sum: 1},
                                                'userScore': {'$first': '$userScore'},
                                                'eventId': {'$first': '$eventId'},
                                                'isDeleted': {'$first': '$isDeleted'},
                                                'nibiId': {'$first': '$nibiId'},
                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                'comment': {'$first': '$comment'},
                                                'postType': {'$first': '$postType'},
                                                'coHosts': {'$first': '$coHosts'},
                                                'sourceFile': {'$first': '$sourceFile'},
                                                'mention': {'$first': '$mention'},
                                                'reply': {'$first': '$reply'},
                                                'like': {'$first': '$like'},
                                                'addedAt': {'$first': '$addedAt'},
                                                'user': {'$first': '$user'},
                                                'PostDetail': {'$first': '$PostDetail'},
                                                'postSize': {'$first': '$postSize'},
                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                'options': {'$first': '$options'}
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                '_id': 0,
                                                'convertedId': '$_id.convertedId',
                                                'optionId': '$_id.optionId',
                                                'countBy': 1,
                                                'options': 1,
                                                'optionArrDet': {
                                                    'optionId': '$_id.optionId',
                                                    'countBy': '$countBy',
                                                    'optionIndex': '$PostDetail.optionIndex',
                                                    'optionName': '$PostDetail.optionName',
                                                    'percentageOfVote': {
                                                        $cond: {
                                                            if: {
                                                                $eq: ["$postSize", 0]
                                                            },
                                                            then: 0,
                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                        },
                                                    },
                                                    'voteByUser': {
                                                        $cond: {
                                                            if: {
                                                                $eq: [nibiId, '$PostDetail.nibiId']
                                                            },
                                                            then: true,
                                                            else: false
                                                        }
                                                    },

                                                },
                                                'postSize': 1,
                                                'options': 1
                                            }
                                    },

                                    {
                                        $group:
                                            {
                                                _id: {
                                                    'convertedId': '$convertedId'
                                                },
                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                'user': {'$first': '$user'},
                                                'postSize': {'$first': '$postSize'},
                                                'options': {'$first': '$options'}
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                '_id': '$_id.convertedId',
                                                'optionArrDet':
                                                    {
                                                        $cond: {
                                                            if: {
                                                                $eq: ["$postSize", 0]
                                                            },
                                                            then: [],
                                                            else: '$optionArrDet'
                                                        }
                                                    },

                                                'postSize': 1,
                                                // 'selectMultipleOption': 1,
                                                // 'viewResultToAllGuest': 1,
                                                // 'questionsForPoll': 1,
                                                'options': 1
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                '_id': 1,
                                                'optionArrDet':
                                                    {
                                                        $cond: {
                                                            if: {
                                                                $eq: ["$postSize", 0]
                                                            },
                                                            then: [],
                                                            else: '$optionArrDet'
                                                        }
                                                    },
                                                'postSize': 1,
                                                // 'selectMultipleOption': 1,
                                                // 'viewResultToAllGuest': 1,
                                                // 'questionsForPoll': 1,
                                                'options': 1
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                'optionArrDet': 1,
                                                //   'postSize': 1,
                                                'options': 1
                                            }
                                    }

                                ];

                                Post.aggregate(aggrQuery,
                                    function (err, response) {

                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                }
                                            });
                                        } else {


                                            let optionArrDet = response[0].optionArrDet;
                                            let options = response[0].options;

                                            let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                                                return item.optionId;
                                            })

                                            let dataToInsert = {};

                                            let i;
                                            for (i = 0; i < options.length; i++) {
                                                if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                                                    dataToInsert = {
                                                        "optionId": options[i]._id.toString(),
                                                        "countBy": 0,
                                                        "optionName": options[i].optionName,
                                                        "optionIndex": options[i].optionIndex,
                                                        "percentageOfVote": 0,
                                                        "voteByUser": false
                                                    }

                                                    optionArrDet.push(dataToInsert)
                                                }
                                            }

                                            response[0].optionArrDet = optionArrDet;

                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    statusCode: 200,
                                                    message: errorMsgJSON[lang]["200"],
                                                },
                                                result: {
                                                    optionArrDet,
                                                    //  _id: response[0]._id
                                                }
                                            });
                                            return;
                                        }
                                    });
                                //=================================================================
                            }
                        });
                    } else {
                        let newPolling = new Polling({
                            postId: postId,
                            nibiId: nibiId,
                            userId: userId,
                            optionId: optionId,
                            optionName: optionName,
                            optionIndex: optionIndex
                        });
                        newPolling.save(function (err, item) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {

                                //=================================================================
                                let aggrQuery = [
                                    {
                                        $match: {
                                            '_id': mongoose.Types.ObjectId(postId),
                                        },
                                    },


                                    {
                                        $project: {
                                            'userScore': 1,
                                            'eventId': 1,
                                            'isDeleted': 1,
                                            'nibiId': 1,
                                            'hostNibiId': 1,
                                            'comment': 1,
                                            'postType': 1,
                                            'coHosts': 1,
                                            'sourceFile': 1,
                                            'mention': 1,
                                            'reply': 1,
                                            'like': 1,
                                            'addedAt': 1,
                                            'convertedId': {
                                                $toString: '$_id'
                                            },
                                            'user': 1,
                                            'viewResultToAllGuest': 1,
                                            'questionsForPoll': 1,
                                            'options': 1
                                        },
                                    },

                                    {
                                        $lookup:
                                            {
                                                from: 'pollings',
                                                localField: 'convertedId',
                                                foreignField: 'postId',
                                                as: 'PostDetail'
                                            }
                                    },

                                    {
                                        $project: {
                                            'userScore': 1,
                                            'eventId': 1,
                                            'isDeleted': 1,
                                            'nibiId': 1,
                                            'hostNibiId': 1,
                                            'comment': 1,
                                            'postType': 1,
                                            'coHosts': 1,
                                            'sourceFile': 1,
                                            'mention': 1,
                                            'reply': 1,
                                            'like': 1,
                                            'addedAt': 1,
                                            'convertedId': 1,
                                            'user': 1,
                                            'PostDetail': 1,
                                            'postSize': {
                                                $size: "$PostDetail"
                                            },
                                            'viewResultToAllGuest': 1,
                                            'questionsForPoll': 1,
                                            'options': 1
                                        },
                                    },

                                    {
                                        $unwind: {
                                            path: "$PostDetail",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },


                                    {
                                        $group:
                                            {
                                                _id: {
                                                    'convertedId': '$convertedId',
                                                    'optionId': '$PostDetail.optionId'
                                                },
                                                'countBy': {$sum: 1},
                                                'userScore': {'$first': '$userScore'},
                                                'eventId': {'$first': '$eventId'},
                                                'isDeleted': {'$first': '$isDeleted'},
                                                'nibiId': {'$first': '$nibiId'},
                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                'comment': {'$first': '$comment'},
                                                'postType': {'$first': '$postType'},
                                                'coHosts': {'$first': '$coHosts'},
                                                'sourceFile': {'$first': '$sourceFile'},
                                                'mention': {'$first': '$mention'},
                                                'reply': {'$first': '$reply'},
                                                'like': {'$first': '$like'},
                                                'addedAt': {'$first': '$addedAt'},
                                                'user': {'$first': '$user'},
                                                'PostDetail': {'$first': '$PostDetail'},
                                                'postSize': {'$first': '$postSize'},
                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                'options': {'$first': '$options'}
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                '_id': 0,
                                                'convertedId': '$_id.convertedId',
                                                'optionId': '$_id.optionId',
                                                'countBy': 1,
                                                // 'PostDetail': 1,
                                                'optionArrDet': {
                                                    'optionId': '$_id.optionId',
                                                    'countBy': '$countBy',
                                                    'optionIndex': '$PostDetail.optionIndex',
                                                    'optionName': '$PostDetail.optionName',
                                                    'percentageOfVote': {
                                                        $cond: {
                                                            if: {
                                                                $eq: ["$postSize", 0]
                                                            },
                                                            then: 0,
                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                        },
                                                    },
                                                    'voteByUser': {
                                                        $cond: {
                                                            if: {
                                                                $eq: [nibiId, '$PostDetail.nibiId']
                                                            },
                                                            then: true,
                                                            else: false
                                                        }
                                                    },

                                                },
                                                'postSize': 1,
                                                'options': 1
                                            }
                                    },

                                    {
                                        $group:
                                            {
                                                _id: {
                                                    'convertedId': '$convertedId'
                                                },
                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                'user': {'$first': '$user'},
                                                'postSize': {'$first': '$postSize'},
                                                'options': {'$first': '$options'}
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                '_id': '$_id.convertedId',
                                                'optionArrDet':
                                                    {
                                                        $cond: {
                                                            if: {
                                                                $eq: ["$postSize", 0]
                                                            },
                                                            then: [],
                                                            else: '$optionArrDet'
                                                        }
                                                    },

                                                'postSize': 1,
                                                // 'selectMultipleOption': 1,
                                                // 'viewResultToAllGuest': 1,
                                                // 'questionsForPoll': 1,
                                                'options': 1
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                '_id': 1,
                                                'optionArrDet':
                                                    {
                                                        $cond: {
                                                            if: {
                                                                $eq: ["$postSize", 0]
                                                            },
                                                            then: [],
                                                            else: '$optionArrDet'
                                                        }
                                                    },
                                                'postSize': 1,
                                                // 'selectMultipleOption': 1,
                                                // 'viewResultToAllGuest': 1,
                                                // 'questionsForPoll': 1,
                                                'options': 1
                                            }
                                    },

                                    {
                                        $project:
                                            {
                                                'optionArrDet': 1,
                                                //   'postSize': 1,
                                                'options': 1
                                            }
                                    }

                                ];

                                Post.aggregate(aggrQuery,
                                    function (err, response) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                }
                                            });
                                        } else {


                                            let optionArrDet = response[0].optionArrDet;
                                            let options = response[0].options;

                                            let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                                                return item.optionId;
                                            })

                                            let dataToInsert = {};

                                            let i;
                                            for (i = 0; i < options.length; i++) {
                                                if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                                                    dataToInsert = {
                                                        "optionId": options[i]._id.toString(),
                                                        "countBy": 0,
                                                        "optionName": options[i].optionName,
                                                        "optionIndex": options[i].optionIndex,
                                                        "percentageOfVote": 0,
                                                        "voteByUser": false
                                                    }

                                                    optionArrDet.push(dataToInsert)
                                                }
                                            }

                                            response[0].optionArrDet = optionArrDet;


                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    statusCode: 200,
                                                    message: errorMsgJSON[lang]["200"]
                                                },
                                                result: {
                                                    optionArrDet
                                                }

                                            });
                                            return;
                                        }
                                    });

                                //=================================================================

                            }
                        });
                    }
                }
            });

        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },

    createPost: async (req, res, next) => {


        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " event id."
                }
            });
        let postType = req.body.postType
            ? req.body.postType
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " post type."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let hostNibiId = req.body.hostNibiId
            ? req.body.hostNibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " host nibi id."
                }
            });
        let userId = req.body.userId
            ? req.body.userId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " user id."
                }
            });
        let sourceFile = req.body.sourceFile
            ? req.body.sourceFile
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " source file."
                }
            });
        let mention = req.body.mention
            ? req.body.mention
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " mention."
                }
            });

        let options = req.body.options ? req.body.options : [];

        if (!req.body.eventId || !req.body.postType || !req.body.nibiId || !req.body.hostNibiId || !req.body.userId || !req.body.sourceFile || !req.body.mention) {
            return true;
        }


        try {
            Event.findOne({_id: eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (event.guestList.length == 0) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "You are no longer participant in this event.",
                                statusCode: 410
                            },
                        });
                    } else {
                        let newPost = new Post({
                            eventId: eventId,
                            nibiId: nibiId,
                            hostNibiId: hostNibiId,
                            comment: req.body.comment,
                            postType: postType,
                            user: mongoose.Types.ObjectId(userId),
                            userScore: req.body.userScore,
                            viewResultToAllGuest: req.body.viewResultToAllGuest,
                            questionsForPoll: req.body.questionsForPoll,
                            options: options
                        });
                        newPost.save(function (err, item) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    }
                                });
                            } else {
                                if (!item) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            statusCode: 404,
                                            message: errorMsgJSON[lang]["404"]
                                        }
                                    });
                                } else {
                                    Post.findOne({_id: item._id}, function (err, findPost) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                }
                                            });
                                        } else {
                                            User.findOne({nibiId: nibiId}, function (err, user) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        }
                                                    });
                                                } else {
                                                    Event.findOne({_id: eventId}, function (err, guestNibiId) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                }
                                                            });
                                                        } else {
                                                            let guestNibiIds = guestNibiId.guestList.map(e => {
                                                                if (e.nibiId != nibiId) {
                                                                    return e.nibiId
                                                                }
                                                            });
                                                            User.find({
                                                                nibiId: {$in: guestNibiIds},
                                                                isNotify: true
                                                            }, function (err, fcm) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        }
                                                                    });
                                                                } else {
                                                                    saveNotificationForCreatePost(item, req, res, lang, nibiId, guestNibiId, eventId, item);

                                                                    let fcmFilter = fcm.filter(function (e) {
                                                                        return e.fcmToken != "";
                                                                    });
                                                                    let fcmTokens = fcmFilter.map(fcm => {
                                                                        return fcm;
                                                                    });

                                                                    if (req.body.sourceFile.length > 0 || req.body.mention.length > 0) {
                                                                        Post.updateMany(
                                                                            {"_id": item._id},
                                                                            {
                                                                                $push: {
                                                                                    "sourceFile": sourceFile,
                                                                                    "mention": mention
                                                                                }
                                                                            }, function (err, response) {
                                                                                if (err) {
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: true,
                                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                            statusCode: 500
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    if (mention.length > 0) {
                                                                                        let nibiIds = mention.map(e => {
                                                                                            if (e.nibiId != nibiId) {
                                                                                                return e.nibiId
                                                                                            }
                                                                                        });
                                                                                        let payload = {
                                                                                            notification: {
                                                                                                title: guestNibiId.title,
                                                                                                body: user.fname + " " + user.lname + " mentioned you in a post: " + req.body.comment
                                                                                            },
                                                                                            data: {
                                                                                                title: guestNibiId.title,
                                                                                                body: user.fname + " " + user.lname + " mentioned you in a post: " + req.body.comment,
                                                                                                notificationType: 'create_post',
                                                                                                postId: `${findPost._id}`,
                                                                                                eventId: eventId
                                                                                            }
                                                                                        };
                                                                                        findFcmAndSendNotification(res, nibiIds, payload);
                                                                                    }
                                                                                    let payload = {
                                                                                        notification: {
                                                                                            title: guestNibiId.title,
                                                                                            body: user.fname + " " + user.lname + " posted: " + req.body.comment
                                                                                        },
                                                                                        data: {
                                                                                            title: guestNibiId.title,
                                                                                            body: user.fname + " " + user.lname + " posted: " + req.body.comment,
                                                                                            notificationType: 'create_post',
                                                                                            postId: `${findPost._id}`,
                                                                                            eventId: eventId
                                                                                        }
                                                                                    };
                                                                                    NotificationService.sendNotification(fcmTokens, payload)
                                                                                    res.json({
                                                                                        serverResponse: {
                                                                                            isError: false,
                                                                                            message: errorMsgJSON[lang]["200"],
                                                                                            statusCode: 200
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                    } else {
                                                                        // let notificationBody = {
                                                                        //     title: guestNibiId.title,
                                                                        //     body: user.fname+ " "+user.lname+ " posted: "+ req.body.comment
                                                                        // }
                                                                        let postIds = findPost._id;
                                                                        let payload = {
                                                                            notification: {
                                                                                title: guestNibiId.title,
                                                                                body: user.fname + " " + user.lname + " posted: " + req.body.comment
                                                                            },
                                                                            data: {
                                                                                title: guestNibiId.title,
                                                                                body: user.fname + " " + user.lname + " posted: " + req.body.comment,
                                                                                notificationType: 'create_post',
                                                                                postId: `${findPost._id}`,
                                                                                eventId: eventId
                                                                            }
                                                                        };
                                                                        NotificationService.sendNotification(fcmTokens, payload)
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: errorMsgJSON[lang]["200"],
                                                                                statusCode: 200
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    replyOnPost: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " post id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let mentionOnReply = req.body.mentionOnReply
            ? req.body.mentionOnReply
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " mention on reply."
                }
            });
        if (!req.body.postId || !req.body.nibiId || !req.body.mentionOnReply) {
            return true;
        }

        try {
            Post.findOne({_id: postId}, function (err, post) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (post) {
                        Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {

                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {
                                if (event.guestList.length == 0) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: "You are no longer participant in this event.",
                                            statusCode: 410
                                        },
                                    });
                                } else {
                                    let newReply = {
                                        nibiId: nibiId,
                                        replyText: req.body.replyText,
                                        firstName: req.body.firstName,
                                        lastName: req.body.lastName,
                                        userImage: req.body.userImage,
                                        imageUrl: req.body.imageUrl,
                                        mentionOnReply: mentionOnReply
                                    };
                                    Post.updateOne(
                                        {"_id": postId},
                                        {$push: {"reply": newReply}}, function (err, response) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {

                                                let aggrQuery = [
                                                    {
                                                        $match: {
                                                            '_id': mongoose.Types.ObjectId(postId),
                                                            // 'isDeleted': false
                                                        },
                                                    },


                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': {
                                                                $toString: '$_id'
                                                            },
                                                            'user': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $lookup:
                                                            {
                                                                from: 'pollings',
                                                                localField: 'convertedId',
                                                                foreignField: 'postId',
                                                                as: 'PostDetail'
                                                            }
                                                    },

                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': 1,
                                                            'user': 1,
                                                            'PostDetail': 1,
                                                            'postSize': {
                                                                $size: "$PostDetail"
                                                            },
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $unwind: {
                                                            path: "$PostDetail",
                                                            preserveNullAndEmptyArrays: true
                                                        }
                                                    },


                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId',
                                                                    'optionId': '$PostDetail.optionId'
                                                                },
                                                                'countBy': {$sum: 1},
                                                                'userScore': {'$first': '$userScore'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'isDeleted': {'$first': '$isDeleted'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                                'comment': {'$first': '$comment'},
                                                                'postType': {'$first': '$postType'},
                                                                'coHosts': {'$first': '$coHosts'},
                                                                'sourceFile': {'$first': '$sourceFile'},
                                                                'mention': {'$first': '$mention'},
                                                                'reply': {'$first': '$reply'},
                                                                'like': {'$first': '$like'},
                                                                'addedAt': {'$first': '$addedAt'},
                                                                'user': {'$first': '$user'},
                                                                'PostDetail': {'$first': '$PostDetail'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 0,
                                                                'convertedId': '$_id.convertedId',
                                                                'optionId': '$_id.optionId',
                                                                'countBy': 1,
                                                                'optionArrDet': {
                                                                    'optionId': '$_id.optionId',
                                                                    'countBy': '$countBy',
                                                                    'optionName': '$PostDetail.optionName',
                                                                    'percentageOfVote': {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: 0,
                                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                        }
                                                                    }
                                                                },

                                                                'userScore': 1,
                                                                'eventId': 1,
                                                                'isDeleted': 1,
                                                                'nibiId': 1,
                                                                'hostNibiId': 1,
                                                                'comment': 1,
                                                                'postType': 1,
                                                                'coHosts': 1,
                                                                'sourceFile': 1,
                                                                'mention': 1,
                                                                'reply': 1,
                                                                'like': 1,
                                                                'addedAt': 1,
                                                                'user': 1,
                                                                // 'PostDetail': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId'
                                                                },
                                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'reply': {'$first': '$reply'},
                                                                'user': {'$first': '$user'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': '$_id.convertedId',
                                                                'convertedId': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                'optionArrDet': 1,
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    }

                                                ];

                                                Post.aggregate(aggrQuery,
                                                    function (err, response) {
                                                        // Post.findOne({ "_id": postId }).select('eventId nibiId reply').exec(function (err, item) {

                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {
                                                            item = response[0];
                                                            if (!item) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["404"],
                                                                        statusCode: 404
                                                                    }
                                                                });
                                                            } else {

                                                                findGuestAndSaveNotification(item, req, res, lang, nibiId, postId);

                                                                let nibiIds = [];
                                                                if (item.nibiId != nibiId) {
                                                                    nibiIds.push(item.nibiId);
                                                                }

                                                                // let notificationBody = {
                                                                //     title: req.body.title,
                                                                //     body: req.body.firstName + " "+ req.body.lastName+ " replied to your post: " + req.body.replyText
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: req.body.title,
                                                                        body: req.body.firstName + " " + req.body.lastName + " replied to your post: " + req.body.replyText
                                                                    },
                                                                    data: {
                                                                        title: req.body.title,
                                                                        body: req.body.firstName + " " + req.body.lastName + " replied to your post: " + req.body.replyText,
                                                                        notificationType: 'comment',
                                                                        postId: postId,
                                                                        eventId: item.eventId
                                                                    }
                                                                };
                                                                findFcmAndSendNotification(res, nibiIds, payload);

                                                                if (mentionOnReply.length > 0) {
                                                                    let nibiIds = mentionOnReply.map(e => {
                                                                        if (e.nibiId != nibiId) {
                                                                            return e.nibiId
                                                                        }
                                                                    });
                                                                    // let notificationBody = {
                                                                    //     title: req.body.title,
                                                                    //     body: req.body.firstName + " "+ req.body.lastName+ " mentioned you in a reply: " + req.body.replyText
                                                                    // }
                                                                    let payload = {
                                                                        notification: {
                                                                            title: req.body.title,
                                                                            body: req.body.firstName + " " + req.body.lastName + " mentioned you in a reply: " + req.body.replyText
                                                                        },
                                                                        data: {
                                                                            title: req.body.title,
                                                                            body: req.body.firstName + " " + req.body.lastName + " mentioned you in a reply: " + req.body.replyText,
                                                                            notificationType: 'comment',
                                                                            postId: postId,
                                                                            eventId: item.eventId
                                                                        }
                                                                    };

                                                                    findFcmAndSendNotification(res, nibiIds, payload);
                                                                }


                                                                // let posts = item.toJSON();
                                                                let posts = item;
                                                                let newReply = [];
                                                                posts.reply.map(reply => {
                                                                    let replies = reply;
                                                                    replies.likeCountOnReply = replies.likeOnReply.length;
                                                                    replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                    replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                        return likeOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    let newReplyOnReply = [];
                                                                    reply.replyOnReply.forEach(replyOnReply => {
                                                                        replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length;
                                                                        replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                        replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                            return likeReplyOnReply.nibiId === nibiId
                                                                        }) !== undefined;
                                                                        newReplyOnReply.push(replyOnReply);
                                                                    });
                                                                    replies.replyOnReply = newReplyOnReply
                                                                    newReply.push(replies)
                                                                });
                                                                posts.reply = newReply;
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: false,
                                                                        message: errorMsgJSON[lang]["200"],
                                                                        statusCode: 200
                                                                    },
                                                                    result: {
                                                                        postDetails: posts
                                                                    },
                                                                });
                                                            }
                                                        }
                                                    });
                                            }
                                        });
                                }
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "Post details not found.",
                                statusCode: 404
                            },
                        });
                    }
                }
            });

        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },

    sendReplyOnReply: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " post id."
                }
            });
        let replyId = req.body.replyId
            ? req.body.replyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " reply id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let mentionOnReplyOnReply = req.body.mentionOnReplyOnReply
            ? req.body.mentionOnReplyOnReply
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " mention on reply on reply."
                }
            });
        if (!req.body.postId || !req.body.replyId || !req.body.nibiId || !req.body.mentionOnReplyOnReply) {
            return true;
        }
        try {
            Post.findOne({_id: postId}, function (err, post) {

                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (post) {
                        Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {


                                if (event.guestList.length == 0) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: "You are no longer participant in this event.",
                                            statusCode: 410
                                        },
                                    });
                                } else {
                                    let newReply = {
                                        nibiId: nibiId,
                                        replyText: req.body.replyText,
                                        firstName: req.body.firstName,
                                        lastName: req.body.lastName,
                                        userImage: req.body.userImage,
                                        imageUrl: req.body.imageUrl,
                                        mentionOnReplyOnReply: mentionOnReplyOnReply
                                    };
                                    Post.updateOne(
                                        {_id: postId, "reply._id": replyId},
                                        {$push: {"reply.$.replyOnReply": newReply}}, function (err, response) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {

                                                let aggrQuery = [
                                                    {
                                                        $match: {
                                                            '_id': mongoose.Types.ObjectId(postId),
                                                            // 'isDeleted': false
                                                        },
                                                    },


                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': {
                                                                $toString: '$_id'
                                                            },
                                                            'user': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $lookup:
                                                            {
                                                                from: 'pollings',
                                                                localField: 'convertedId',
                                                                foreignField: 'postId',
                                                                as: 'PostDetail'
                                                            }
                                                    },

                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': 1,
                                                            'user': 1,
                                                            'PostDetail': 1,
                                                            'postSize': {
                                                                $size: "$PostDetail"
                                                            },
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $unwind: {
                                                            path: "$PostDetail",
                                                            preserveNullAndEmptyArrays: true
                                                        }
                                                    },


                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId',
                                                                    'optionId': '$PostDetail.optionId'
                                                                },
                                                                'countBy': {$sum: 1},
                                                                'userScore': {'$first': '$userScore'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'isDeleted': {'$first': '$isDeleted'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                                'comment': {'$first': '$comment'},
                                                                'postType': {'$first': '$postType'},
                                                                'coHosts': {'$first': '$coHosts'},
                                                                'sourceFile': {'$first': '$sourceFile'},
                                                                'mention': {'$first': '$mention'},
                                                                'reply': {'$first': '$reply'},
                                                                'like': {'$first': '$like'},
                                                                'addedAt': {'$first': '$addedAt'},
                                                                'user': {'$first': '$user'},
                                                                'PostDetail': {'$first': '$PostDetail'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 0,
                                                                'convertedId': '$_id.convertedId',
                                                                'optionId': '$_id.optionId',
                                                                'countBy': 1,
                                                                'optionArrDet': {
                                                                    'optionId': '$_id.optionId',
                                                                    'countBy': '$countBy',
                                                                    'optionName': '$PostDetail.optionName',
                                                                    'percentageOfVote': {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: 0,
                                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                        }
                                                                    }
                                                                },

                                                                'userScore': 1,
                                                                'eventId': 1,
                                                                'isDeleted': 1,
                                                                'nibiId': 1,
                                                                'hostNibiId': 1,
                                                                'comment': 1,
                                                                'postType': 1,
                                                                'coHosts': 1,
                                                                'sourceFile': 1,
                                                                'mention': 1,
                                                                'reply': 1,
                                                                'like': 1,
                                                                'addedAt': 1,
                                                                'user': 1,
                                                                // 'PostDetail': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId'
                                                                },
                                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'reply': {'$first': '$reply'},
                                                                'user': {'$first': '$user'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': '$_id.convertedId',
                                                                'convertedId': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                'optionArrDet': 1,
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    }

                                                ];

                                                Post.aggregate(aggrQuery,
                                                    function (err, response) {
                                                        // Post.findOne({ "_id": postId }).select('nibiId eventId reply').exec(function (err, item) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {

                                                            let item = response[0];

                                                            if (!item) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["404"],
                                                                        statusCode: 404
                                                                    }
                                                                });
                                                            } else {

                                                                if (mentionOnReplyOnReply.length > 0) {
                                                                    let nibiIds = mentionOnReplyOnReply.map(e => {
                                                                        if (e.nibiId != nibiId) {
                                                                            return e.nibiId
                                                                        }
                                                                    });
                                                                    // let notificationBody = {
                                                                    //     title: req.body.title,
                                                                    //     body: req.body.firstName + " "+ req.body.lastName+ " mentioned you in a reply: " + req.body.replyText
                                                                    // }
                                                                    let payload = {
                                                                        notification: {
                                                                            title: req.body.title,
                                                                            body: req.body.firstName + " " + req.body.lastName + " mentioned you in a reply: " + req.body.replyText
                                                                        },
                                                                        data: {
                                                                            title: req.body.title,
                                                                            body: req.body.firstName + " " + req.body.lastName + " mentioned you in a reply: " + req.body.replyText,
                                                                            notificationType: 'reply',
                                                                            postId: postId,
                                                                            eventId: item.eventId
                                                                        }
                                                                    };
                                                                    findFcmAndSendNotification(res, nibiIds, payload);
                                                                }
                                                                Post.findOne({
                                                                    _id: postId
                                                                }).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, reply) {

                                                                    if (err) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: true,
                                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                statusCode: 500
                                                                            },
                                                                        });
                                                                    } else {
                                                                        saveNotificationOnReplyOnReply(reply, req, res, lang, nibiId, item, postId);
                                                                        let replyNibiId;
                                                                        if (reply.reply[0].nibiId != nibiId) {
                                                                            replyNibiId = reply.reply[0].nibiId;
                                                                        }
                                                                        if (replyNibiId) {
                                                                            sendNotificationToReplier(req, res, replyNibiId, lang, postId, item);
                                                                        }
                                                                        if (item.nibiId != nibiId) {
                                                                            creatorOfPostNotificationForReplyOfComment(req, res, item, lang, postId, item);
                                                                        }

                                                                    }
                                                                });


                                                                // let posts = item.toJSON();
                                                                let posts = item;
                                                                let newReply = [];
                                                                posts.reply.map(reply => {
                                                                    let replies = reply;
                                                                    replies.likeCountOnReply = replies.likeOnReply.length;
                                                                    replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                    replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                        return likeOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    let newReplyOnReply = [];
                                                                    reply.replyOnReply.forEach(replyOnReply => {
                                                                        replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                        replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                        replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                            return likeReplyOnReply.nibiId === nibiId
                                                                        }) !== undefined;
                                                                        newReplyOnReply.push(replyOnReply);
                                                                    });
                                                                    replies.replyOnReply = newReplyOnReply
                                                                    newReply.push(replies)
                                                                });
                                                                posts.reply = newReply;
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: false,
                                                                        message: errorMsgJSON[lang]["200"],
                                                                        statusCode: 200
                                                                    },
                                                                    result: {
                                                                        postDetails: posts
                                                                    },
                                                                });
                                                            }
                                                        }
                                                    });
                                            }
                                        });
                                }
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "Post details not found.",
                                statusCode: 404
                            },
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },

    sendReplyOnReplyBkp: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " post id."
                }
            });
        let replyId = req.body.replyId
            ? req.body.replyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " reply id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " nibi id."
                }
            });
        let mentionOnReplyOnReply = req.body.mentionOnReplyOnReply
            ? req.body.mentionOnReplyOnReply
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " mention on reply on reply."
                }
            });
        if (!req.body.postId || !req.body.replyId || !req.body.nibiId || !req.body.mentionOnReplyOnReply) {
            return true;
        }
        try {
            Post.findOne({_id: postId}, function (err, post) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (post) {
                        Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                            if (err) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                        statusCode: 500
                                    },
                                });
                            } else {


                                if (event.guestList.length == 0) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: "You are no longer participant in this event.",
                                            statusCode: 410
                                        },
                                    });
                                } else {
                                    let newReply = {
                                        nibiId: nibiId,
                                        replyText: req.body.replyText,
                                        firstName: req.body.firstName,
                                        lastName: req.body.lastName,
                                        userImage: req.body.userImage,
                                        imageUrl: req.body.imageUrl,
                                        mentionOnReplyOnReply: mentionOnReplyOnReply
                                    };
                                    Post.updateOne(
                                        {_id: postId, "reply._id": replyId},
                                        {$push: {"reply.$.replyOnReply": newReply}}, function (err, response) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    }
                                                });
                                            } else {
                                                Post.findOne({"_id": postId}).select('nibiId eventId reply').exec(function (err, item) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        if (!item) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["404"],
                                                                    statusCode: 404
                                                                }
                                                            });
                                                        } else {
                                                            if (mentionOnReplyOnReply.length > 0) {
                                                                let nibiIds = mentionOnReplyOnReply.map(e => {
                                                                    if (e.nibiId != nibiId) {
                                                                        return e.nibiId
                                                                    }
                                                                });
                                                                // let notificationBody = {
                                                                //     title: req.body.title,
                                                                //     body: req.body.firstName + " "+ req.body.lastName+ " mentioned you in a reply: " + req.body.replyText
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: req.body.title,
                                                                        body: req.body.firstName + " " + req.body.lastName + " mentioned you in a reply: " + req.body.replyText
                                                                    },
                                                                    data: {
                                                                        title: req.body.title,
                                                                        body: req.body.firstName + " " + req.body.lastName + " mentioned you in a reply: " + req.body.replyText,
                                                                        notificationType: 'reply',
                                                                        postId: postId,
                                                                        eventId: item.eventId
                                                                    }
                                                                };
                                                                findFcmAndSendNotification(res, nibiIds, payload);
                                                            }
                                                            Post.findOne({
                                                                _id: postId
                                                            }).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, reply) {

                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        },
                                                                    });
                                                                } else {
                                                                    saveNotificationOnReplyOnReply(reply, req, res, lang, nibiId, item, postId);
                                                                    let replyNibiId;
                                                                    if (reply.reply[0].nibiId != nibiId) {
                                                                        replyNibiId = reply.reply[0].nibiId;
                                                                    }
                                                                    if (replyNibiId) {
                                                                        sendNotificationToReplier(req, res, replyNibiId, lang, postId, item);
                                                                    }
                                                                    if (item.nibiId != nibiId) {
                                                                        creatorOfPostNotificationForReplyOfComment(req, res, item, lang, postId, item);
                                                                    }

                                                                }
                                                            });

                                                            let posts = item.toJSON();
                                                            let newReply = [];
                                                            posts.reply.map(reply => {
                                                                let replies = reply;
                                                                replies.likeCountOnReply = replies.likeOnReply.length;
                                                                replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                    return likeOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                let newReplyOnReply = [];
                                                                reply.replyOnReply.forEach(replyOnReply => {
                                                                    replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                    replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                    replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                        return likeReplyOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    newReplyOnReply.push(replyOnReply);
                                                                });
                                                                replies.replyOnReply = newReplyOnReply
                                                                newReply.push(replies)
                                                            });
                                                            posts.reply = newReply;
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: errorMsgJSON[lang]["200"],
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    postDetails: posts
                                                                },
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "Post details not found.",
                                statusCode: 404
                            },
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },


    specificPost: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId;
        // console.log(nibiId);
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let findPost = {
            _id: postId,
            isDeleted: false
        };

        let aggrQuery = [
            {
                $match: {
                    '_id': mongoose.Types.ObjectId(postId),
                    'isDeleted': false
                },
            },


            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': {
                        $toString: '$_id'
                    },
                    'user': 1,
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $lookup:
                    {
                        from: 'pollings',
                        localField: 'convertedId',
                        foreignField: 'postId',
                        as: 'PostDetail'
                    }
            },

            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': 1,
                    'user': 1,
                    'PostDetail': 1,
                    'postSize': {
                        $size: "$PostDetail"
                    },
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $unwind: {
                    path: "$PostDetail",
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId',
                            'optionId': '$PostDetail.optionId'
                        },
                        'countBy': {$sum: 1},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'PostDetail': {'$first': '$PostDetail'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': 0,
                        'convertedId': '$_id.convertedId',
                        'optionId': '$_id.optionId',
                        'countBy': 1,
                        'optionArrDet': {
                            'optionId': '$_id.optionId',
                            'countBy': '$countBy',
                            'optionName': '$PostDetail.optionName',
                            'optionIndex': '$PostDetail.optionIndex',
                            'percentageOfVote': {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: 0,
                                    else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                }
                            },
                            'voteByUser': {
                                $cond: {
                                    if: {
                                        $eq: [nibiId, '$PostDetail.nibiId']
                                    },
                                    then: true,
                                    else: false
                                }
                            },
                        },

                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        // 'PostDetail': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId'
                        },
                        'optionArrDet': {'$push': '$optionArrDet'},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': '$_id.convertedId',
                        'convertedId': 1,
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $lookup:
                    {
                        from: 'users',
                        localField: 'user',
                        foreignField: '_id',
                        as: 'userDetail'
                    }
            },

            {
                $project:
                    {
                        '_id': 1,
                        'convertedId': '$_id.convertedId',
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'userDetail': {$arrayElemAt: ['$userDetail', 0]},
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $project:
                    {
                        'convertedId': 1,
                        'optionArrDet': 1,
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        // 'userDetail': 1,
                        'user': {
                            'fname': '$userDetail.fname',
                            'lname': '$userDetail.lname',
                            'profilePic': '$userDetail.profilePic',
                            // $concat: [ "$userDetail.fname", " ", "$userDetail.lname", " ", "$userDetail.profilePic" ]
                        },
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

        ];
        // Post.find(findPost).populate("user", "fname lname profilePic").skip(skip).limit(limit).sort({addedAt: "desc"}).exec(function (err, items) {
        Post.aggregate(aggrQuery,
            function (err, response) {

                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {
                    if (response.length > 0) {
                        let optionArrDet = response[0].optionArrDet;
                        let options = response[0].options;

                        let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                            return item.optionId;
                        })

                        let dataToInsert = {};

                        let i;
                        for (i = 0; i < options.length; i++) {
                            if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                                dataToInsert = {
                                    "optionId": options[i]._id.toString(),
                                    "countBy": 0,
                                    "optionName": options[i].optionName,
                                    "optionIndex": options[i].optionIndex,
                                    "percentageOfVote": 0,
                                    "voteByUser": false
                                }

                                optionArrDet.push(dataToInsert)
                            }
                        }

                        response[0].optionArrDet = optionArrDet;
                        response[0].options = options;

                        let items = response[0];

                        let posts = items;
                        posts.timeDifference = moment(posts.addedAt).fromNow(true);
                        posts.isLiked = posts.like.find(like => {
                            return like.nibiId === nibiId
                        }) !== undefined;
                        posts.likeCount = posts.like.length;
                        let newReply = [];
                        posts.reply.map(reply => {
                            let replies = reply;
                            replies.likeCountOnReply = replies.likeOnReply.length;
                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                return likeOnReply.nibiId === nibiId
                            }) !== undefined;
                            let newReplyOnReply = [];
                            reply.replyOnReply.forEach(replyOnReply => {
                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                    return likeReplyOnReply.nibiId === nibiId
                                }) !== undefined;
                                newReplyOnReply.push(replyOnReply);
                            });
                            replies.replyOnReply = newReplyOnReply
                            newReply.push(replies)
                        });
                        posts.reply = newReply;

                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                postDetails: posts
                            },
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: "Post details not found.",
                                statusCode: 404
                            }
                        });
                    }
                }
            });
    },

    specificPostBkp: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId;
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let findPost = {
            _id: postId,
            isDeleted: false
        };
        Post.findOne(findPost).populate("user", "fname lname profilePic").exec(function (err, items) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!items) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    Event.findOne({_id: items.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                let posts = items.toJSON();
                                posts.timeDifference = moment(posts.addedAt).fromNow(true);
                                posts.isLiked = posts.like.find(like => {
                                    return like.nibiId === nibiId
                                }) !== undefined;
                                posts.likeCount = posts.like.length;
                                let newReply = [];
                                posts.reply.map(reply => {
                                    let replies = reply;
                                    replies.likeCountOnReply = replies.likeOnReply.length;
                                    replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                    replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                        return likeOnReply.nibiId === nibiId
                                    }) !== undefined;
                                    let newReplyOnReply = [];
                                    reply.replyOnReply.forEach(replyOnReply => {
                                        replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                        replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                        replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                            return likeReplyOnReply.nibiId === nibiId
                                        }) !== undefined;
                                        newReplyOnReply.push(replyOnReply);
                                    });
                                    replies.replyOnReply = newReplyOnReply
                                    newReply.push(replies)
                                });
                                posts.reply = newReply;

                                res.json({
                                    serverResponse: {
                                        isError: false,
                                        message: errorMsgJSON[lang]["200"],
                                        statusCode: 200
                                    },
                                    result: {
                                        postDetails: posts
                                    },
                                });
                            }
                        }
                    });
                }
            }
        });
    },
    // eventWisePost: (req, res, next) => {
    //     let lang = req.headers.language ? req.headers.language : "EN";
    //     let eventId = req.body.eventId
    //         ? req.body.eventId
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: "Please provide event Id."
    //             }
    //         });
    //     let nibiId = req.body.nibiId
    //         ? req.body.nibiId
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: "Please provide nibi Id."
    //             }
    //         });
    //     let page = req.body.page
    //         ? req.body.page
    //         : res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 statusCode: 404,
    //                 message: "Please provide page number."
    //             }
    //         });
    //     if (!req.body.eventId || !req.body.nibiId || !req.body.page) {
    //         return true;
    //     }
    //     let limit = 6;
    //     let skip = (page - 1) * limit;
    //     let findPost = {
    //         eventId: eventId,
    //         isDeleted: false
    //     };
    //     Event.findOne({ _id: eventId }).select('coHosts').exec(function (err, coHost) {
    //         if (err) {
    //             res.json({
    //                 serverResponse: {
    //                     isError: true,
    //                     message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                     statusCode: 500
    //                 },
    //             });
    //         } else {
    //             Event.findOne({ _id: eventId }).select({ guestList: { $elemMatch: { nibiId: nibiId } } }).exec(function (err, event) {
    //                 if (err) {
    //                     res.json({
    //                         serverResponse: {
    //                             isError: true,
    //                             message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                             statusCode: 500
    //                         },
    //                     });
    //                 } else {
    //                     if (event.guestList.length == 0) {
    //                         res.json({
    //                             serverResponse: {
    //                                 isError: true,
    //                                 message: "You are no longer participant in this event.",
    //                                 statusCode: 410
    //                             },
    //                         });
    //                     } else {
    //                         Post.find(findPost).countDocuments().exec(function (err, count) {
    //                             if (err) {
    //                                 res.json({
    //                                     serverResponse: {
    //                                         isError: true,
    //                                         message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                         statusCode: 500
    //                                     },
    //                                 });
    //                             } else {
    //                                 Post.find(findPost).populate("user", "fname lname profilePic").skip(skip).limit(limit).sort({ addedAt: "desc" }).exec(function (err, items) {
    //                                     if (err) {
    //                                         res.json({
    //                                             serverResponse: {
    //                                                 isError: true,
    //                                                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
    //                                                 statusCode: 500
    //                                             },
    //                                         });
    //                                     } else {
    //                                         // items = items.map(item => {
    //                                         //     let itemJson = item.toJSON();
    //                                         //     itemJson.timeDifference = moment(itemJson.addedAt).fromNow(true);
    //                                         //     itemJson.isLiked = itemJson.like.find(like => {
    //                                         //         return like.nibiId === nibiId
    //                                         //     }) !== undefined;
    //                                         //     return itemJson;
    //                                         // });
    //                                         let item = [];
    //                                         items.map((post) => {
    //                                             let posts = post.toJSON();
    //                                             posts.timeDifference = moment(posts.addedAt).fromNow(true);
    //                                             posts.isLiked = posts.like.find(like => {
    //                                                 return like.nibiId === nibiId
    //                                             }) !== undefined;
    //                                             posts.likeCount = posts.like.length;
    //                                             // item.push(posts);
    //                                             let newReply = [];
    //                                             posts.reply.map(reply => {
    //                                                 let replies = reply;
    //                                                 replies.likeCountOnReply = replies.likeOnReply.length;
    //                                                 replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
    //                                                 replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
    //                                                     return likeOnReply.nibiId === nibiId
    //                                                 }) !== undefined;
    //                                                 let newReplyOnReply = [];
    //                                                 reply.replyOnReply.forEach(replyOnReply => {
    //                                                     replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length;
    //                                                     replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
    //                                                     replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
    //                                                         return likeReplyOnReply.nibiId === nibiId
    //                                                     }) !== undefined;
    //                                                     newReplyOnReply.push(replyOnReply);
    //                                                 });
    //                                                 replies.replyOnReply = newReplyOnReply
    //                                                 newReply.push(replies)
    //                                             });
    //                                             posts.reply = newReply;
    //                                             item.push(posts);
    //
    //                                         });
    //                                         res.json({
    //                                             serverResponse: {
    //                                                 isError: false,
    //                                                 message: errorMsgJSON[lang]["200"],
    //                                                 statusCode: 200
    //                                             },
    //                                             result: {
    //                                                 totalPost: count,
    //                                                 postDetails: item,
    //                                                 coHost
    //                                             },
    //                                         });
    //                                     }
    //                                 });
    //                             }
    //                         });
    //                     }
    //                 }
    //             });
    //         }
    //     });
    // },
    eventWiseMediaList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.decoded.nibiId;
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        if (!req.body.eventId) {
            return true;
        }
        let findData = {
            eventId: eventId,
            isDeleted: false
        };
        let findEvent = {
            _id: eventId
        };

        Event.findOne(findEvent).select('coHosts').exec(function (err, coHost) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Event.findOne(findEvent).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        if (event) {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                if (event.hasPhotoAlbum === false) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            statusCode: 404,
                                            message: "Host has this feature disabled."
                                        }
                                    });
                                } else {
                                    Post.find(findData).populate("user", "fname lname profilePic nibiId").exec(function (err, item) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            if (item.length > 0) {
                                                let imageUrl = [];
                                                item.forEach((element) => {
                                                    element.sourceFile.forEach((source) => {
                                                        if (source.sourceType != "image/gif") {
                                                            let sourceObj = {
                                                                postId: element._id,
                                                                sourceUrl: source.sourceUrl,
                                                                sourceType: source.sourceType,
                                                                hostNibiId: element.hostNibiId,
                                                                mediaId: source._id,
                                                                fname: element.user.fname,
                                                                lname: element.user.lname,
                                                                profilePic: element.user.profilePic,
                                                                nibiId: element.user.nibiId,
                                                                likeCount: element.like.length,
                                                                comment: element.comment,
                                                                replyCount: element.reply.length,
                                                                addedAt: element.addedAt,
                                                                mention: element.mention,
                                                                timeDifference: moment(element.addedAt).fromNow(true),
                                                                isLiked: element.like.find(like => {
                                                                    return like.nibiId === nibiId
                                                                }) !== undefined
                                                            }
                                                            imageUrl.push(sourceObj)
                                                        }
                                                    });
                                                    // element.reply.forEach((reply) => {
                                                    //     let replyObj = {postId: element._id, sourceUrl: reply.imageUrl}
                                                    //     imageUrl.push(replyObj)
                                                    //
                                                    //     reply.replyOnReply.forEach((replyOnReply) => {
                                                    //         let replyOnReplyObj = {
                                                    //             postId: element._id,
                                                    //             sourceUrl: replyOnReply.imageUrl
                                                    //         }
                                                    //         imageUrl.push(replyOnReplyObj);
                                                    //     });
                                                    // });
                                                });
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        mediaList: imageUrl,
                                                        coHost
                                                    },
                                                });
                                            } else {
                                                res.json({
                                                    serverResponse: {
                                                        isError: false,
                                                        message: errorMsgJSON[lang]["200"],
                                                        statusCode: 200
                                                    },
                                                    result: {
                                                        mediaList: item
                                                    },
                                                });
                                            }

                                        }
                                    });
                                }
                            }
                        } else {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    statusCode: 404,
                                    message: "Event details not found."
                                }
                            });
                        }

                    }
                });
            }
        });

    },

    editPost: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        let sourceIds = req.body.deletedSourceIds
            ? req.body.deletedSourceIds
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide source Id's that will be delete."
                }
            });
        let sourceUrl = req.body.addedSourceFile
            ? req.body.addedSourceFile
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide source url that will be add."
                }
            });
        let comment = typeof (req.body.comment)
            ? req.body.comment
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide comment."
                }
            });
        let mention = req.body.mention
            ? req.body.mention
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide mention."
                }
            });
        if (!req.body.postId || !req.body.deletedSourceIds || !req.body.addedSourceFile || !typeof (req.body.comment) || !req.body.nibiId || !req.body.mention) {
            return true;
        }
        let findPost = {
            _id: postId,
            isDeleted: false
        };

        let aggrQuery = [
            {
                $match: {
                    '_id': mongoose.Types.ObjectId(postId),
                    'isDeleted': false
                },
            },
            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': {
                        $toString: '$_id'
                    },
                    'user': 1,
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $lookup:
                    {
                        from: 'pollings',
                        localField: 'convertedId',
                        foreignField: 'postId',
                        as: 'PostDetail'
                    }
            },

            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': 1,
                    'user': 1,
                    'PostDetail': 1,
                    'postSize': {
                        $size: "$PostDetail"
                    },
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $unwind: {
                    path: "$PostDetail",
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId',
                            'optionId': '$PostDetail.optionId'
                        },
                        'countBy': {$sum: 1},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'PostDetail': {'$first': '$PostDetail'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': 0,
                        'convertedId': '$_id.convertedId',
                        'optionId': '$_id.optionId',
                        'countBy': 1,
                        'optionArrDet': {
                            'optionId': '$_id.optionId',
                            'countBy': '$countBy',
                            'optionName': '$PostDetail.optionName',
                            'optionIndex': '$PostDetail.optionIndex',
                            'percentageOfVote': {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: 0,
                                    else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                }
                            },
                            'voteByUser': {
                                $cond: {
                                    if: {
                                        $eq: [nibiId, '$PostDetail.nibiId']
                                    },
                                    then: true,
                                    else: false
                                }
                            },
                        },

                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        // 'PostDetail': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId'
                        },
                        'optionArrDet': {'$push': '$optionArrDet'},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': '$_id.convertedId',
                        'convertedId': 1,
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $lookup:
                    {
                        from: 'users',
                        localField: 'user',
                        foreignField: '_id',
                        as: 'userDetail'
                    }
            },

            {
                $project:
                    {
                        '_id': 1,
                        'convertedId': '$_id.convertedId',
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'userDetail': {$arrayElemAt: ['$userDetail', 0]},
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $project:
                    {
                        'convertedId': 1,
                        'optionArrDet': 1,
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        // 'userDetail': 1,
                        'user': {
                            'fname': '$userDetail.fname',
                            'lname': '$userDetail.lname',
                            'profilePic': '$userDetail.profilePic',
                            // $concat: [ "$userDetail.fname", " ", "$userDetail.lname", " ", "$userDetail.profilePic" ]
                        },
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },
        ];
        Post.findOne(findPost, function (err, items) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!items) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {


                    Event.findOne({_id: items.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                let addedFile = [];
                                let removeFile = [];
                                items.sourceFile.forEach(source => {
                                    let array = sourceIds.filter(element => {
                                        return element == source._id
                                    });
                                    if (array.length > 0) {
                                        let removeObj = {sourceUrl: source.sourceUrl, sourceType: source.sourceType}
                                        removeFile.push(removeObj);
                                    } else {
                                        let addedObj = {sourceUrl: source.sourceUrl, sourceType: source.sourceType}
                                        addedFile.push(addedObj);
                                    }
                                });
                                let finalAddedArray = addedFile.concat(sourceUrl);
                                Post.updateOne(findPost, {
                                    $set: {sourceFile: [], mention: []},
                                    comment: comment
                                }, function (err, affected) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        Post.updateOne(findPost, {
                                            $push: {
                                                sourceFile: finalAddedArray,
                                                mention: mention
                                            }
                                        }, function (err, updatedPost) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                let deleteUrl = [];
                                                if (removeFile.length > 0) {
                                                    removeFile.forEach(allSourceUrl => {
                                                        let imageUrl = allSourceUrl.sourceUrl;
                                                        let imageName = imageUrl.split(Env.SPLIT_FROM[Env.MODE]);
                                                        let deletedObj = {Key: imageName[1]};
                                                        deleteUrl.push(deletedObj)
                                                    });
                                                    const params = {
                                                        Bucket: Env.BUCKET[Env.MODE].Bucket,
                                                        Delete: {
                                                            Objects: deleteUrl,
                                                            Quiet: false
                                                        }
                                                    };
                                                    s3.deleteObjects(params, function (err, data) {
                                                        console.log("Aws file deleted.")
                                                    });
                                                }

                                                //    Post.findOne(findPost).populate("user", "fname lname profilePic").select('-userScore').exec(function (err, finalPost) {
                                                Post.aggregate(aggrQuery,
                                                    function (err, response) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {


                                                            let optionArrDet = response[0].optionArrDet;
                                                            let options = response[0].options;

                                                            let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                                                                return item.optionId;
                                                            })

                                                            let dataToInsert = {};

                                                            let i;
                                                            for (i = 0; i < options.length; i++) {
                                                                if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                                                                    dataToInsert = {
                                                                        "optionId": options[i]._id.toString(),
                                                                        "countBy": 0,
                                                                        "optionName": options[i].optionName,
                                                                        "optionIndex": options[i].optionIndex,
                                                                        "percentageOfVote": 0,
                                                                        "voteByUser": false
                                                                    }

                                                                    optionArrDet.push(dataToInsert)
                                                                }
                                                            }

                                                            response[0].optionArrDet = optionArrDet;
                                                            response[0].options = options;


                                                            let posts = response[0];
                                                            posts.timeDifference = moment(posts.addedAt).fromNow(true);
                                                            posts.isLiked = posts.like.find(like => {
                                                                return like.nibiId === nibiId
                                                            }) !== undefined;
                                                            posts.likeCount = posts.like.length;
                                                            let newReply = [];
                                                            posts.reply.map(reply => {
                                                                let replies = reply;
                                                                replies.likeCountOnReply = replies.likeOnReply.length;
                                                                replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                    return likeOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                let newReplyOnReply = [];
                                                                reply.replyOnReply.forEach(replyOnReply => {
                                                                    replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                    replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                    replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                        return likeReplyOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    newReplyOnReply.push(replyOnReply);
                                                                });
                                                                replies.replyOnReply = newReplyOnReply
                                                                newReply.push(replies)
                                                            });
                                                            posts.reply = newReply;

                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: errorMsgJSON[lang]["200"],
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    postDetails: posts
                                                                },
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    },

    editPollOption: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        let options = req.body.options
            ? req.body.options
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide options."
                }
            });
        let questionsForPoll = req.body.questionsForPoll
            ? req.body.questionsForPoll
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide questions for poll."
                }
            });
        if (!req.body.postId || !req.body.nibiId || !req.body.options || !req.body.questionsForPoll) {
            return true;
        }
        let findPost = {
            _id: postId,
            isDeleted: false
        };

        let aggrQuery = [
            {
                $match: {
                    '_id': mongoose.Types.ObjectId(postId),
                    'isDeleted': false
                },
            },


            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': {
                        $toString: '$_id'
                    },
                    'user': 1,
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $lookup:
                    {
                        from: 'pollings',
                        localField: 'convertedId',
                        foreignField: 'postId',
                        as: 'PostDetail'
                    }
            },

            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': 1,
                    'user': 1,
                    'PostDetail': 1,
                    'postSize': {
                        $size: "$PostDetail"
                    },
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $unwind: {
                    path: "$PostDetail",
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId',
                            'optionId': '$PostDetail.optionId'
                        },
                        'countBy': {$sum: 1},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'PostDetail': {'$first': '$PostDetail'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': 0,
                        'convertedId': '$_id.convertedId',
                        'optionId': '$_id.optionId',
                        'countBy': 1,
                        'optionArrDet': {
                            'optionId': '$_id.optionId',
                            'countBy': '$countBy',
                            'optionName': '$PostDetail.optionName',
                            'optionIndex': '$PostDetail.optionIndex',
                            'percentageOfVote': {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: 0,
                                    else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                }
                            },
                            'voteByUser': {
                                $cond: {
                                    if: {
                                        $eq: [nibiId, '$PostDetail.nibiId']
                                    },
                                    then: true,
                                    else: false
                                }
                            },
                        },

                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        // 'PostDetail': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId'
                        },
                        'optionArrDet': {'$push': '$optionArrDet'},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': '$_id.convertedId',
                        'convertedId': 1,
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $lookup:
                    {
                        from: 'users',
                        localField: 'user',
                        foreignField: '_id',
                        as: 'userDetail'
                    }
            },

            {
                $project:
                    {
                        '_id': 1,
                        'convertedId': '$_id.convertedId',
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'userDetail': {$arrayElemAt: ['$userDetail', 0]},
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $project:
                    {
                        'convertedId': 1,
                        'optionArrDet': 1,
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        // 'userDetail': 1,
                        'user': {
                            'fname': '$userDetail.fname',
                            'lname': '$userDetail.lname',
                            'profilePic': '$userDetail.profilePic',
                            // $concat: [ "$userDetail.fname", " ", "$userDetail.lname", " ", "$userDetail.profilePic" ]
                        },
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },
        ];
        Post.findOne(findPost, function (err, items) {

            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!items) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    Event.findOne({_id: items.eventId})
                        .select({guestList: {$elemMatch: {nibiId: nibiId}}})
                        .exec(async function (err, event) {
    
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                        ``
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                // Polling.findOne(findPoll, function (err, poll) {
                                //     if (err) {
                                //         res.json({
                                //             serverResponse: {
                                //                 isError: true,
                                //                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                //                 statusCode: 500
                                //             },
                                //         });
                                //     } else {
                                //         if (poll) {
                                //             res.json({
                                //                 serverResponse: {
                                //                     isError: true,
                                //                     message: "Poll can't be edit after voting start.",
                                //                     statusCode: 404
                                //                 },
                                //             });
                                //         } else {

                                // options.forEach(optionArray => {
                                //    Post.updateMany({
                                //          _id: postId,
                                //          "options._id": optionArray._id
                                //      }, {'options.$.optionName': optionArray.optionName, 'options.$.optionIndex': optionArray.optionIndex}, function (err, updatedPost) {
                                //          if (err) {
                                //              res.json({
                                //                  serverResponse: {
                                //                      isError: true,
                                //                      message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                //                      statusCode: 500
                                //                  },
                                //              });
                                //          } else {
                                //    Post.findOne(findPost).populate("user", "fname lname profilePic").select('-userScore').exec(function (err, finalPost) {

                                // updating options in post
                                let fetchedOption = items.options;
                                let resultOptions = [];
                                fetchedOption.map(x => {
                                    let index = x.optionIndex;
                                    options.map(y => {
                                        if (x._id == y._id) {
                                            resultOptions.push({
                                                "optionName": y.optionName,
                                                "optionIndex": y.optionIndex,
                                                "_id": x._id
                                            })
                                        } 
                                    })
                                });
                                let updateOptionResult = await updateOption(postId, resultOptions, req, questionsForPoll);
                                //========================================

                                // updating pollings updatePollings
                                let i; let resultAllupdatePollings = [];
                                for (i = 0; i < options.length; i++) {
                                    let resultupdatePollings = updatePollings(options[i]._id, options[i].optionName);
                                    resultAllupdatePollings.push(resultupdatePollings);
                                }

                                let resultPromiseAll = await Promise.all(resultAllupdatePollings);

                                // res.json({
                                //     options: options,
                                //     fetchedOption: fetchedOption,
                                //     resultOptions: resultOptions,
                                //     postId: postId,
                                //     resultPromiseAll: resultPromiseAll,
                                //     updateOptionResult: updateOptionResult
                                // });
                                // return;


                                Post.aggregate(aggrQuery,
                                    function (err, response) {

                                        // res.json({
                                        //     response: response,
                                        //     items: items,
                                        //     event: event
                                        // });
                                        // return;
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {

                                            let optionArrDet = response[0].optionArrDet;
                                            let options = response[0].options;

                                            let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                                                return item.optionId;
                                            })

                                            let dataToInsert = {};

                                            let i;
                                            for (i = 0; i < options.length; i++) {
                                                if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                                                    dataToInsert = {
                                                        "optionId": options[i]._id.toString(),
                                                        "countBy": 0,
                                                        "optionName": options[i].optionName,
                                                        "optionIndex": options[i].optionIndex,
                                                        "percentageOfVote": 0,
                                                        "voteByUser": false
                                                    }

                                                    optionArrDet.push(dataToInsert)
                                                }
                                            }

                                            response[0].optionArrDet = optionArrDet;
                                            response[0].options = options;
                                            let posts = response[0];
                                            posts.timeDifference = moment(posts.addedAt).fromNow(true);
                                            posts.isLiked = posts.like.find(like => {
                                                return like.nibiId === nibiId
                                            }) !== undefined;
                                            posts.likeCount = posts.like.length;
                                            let newReply = [];
                                            posts.reply.map(reply => {
                                                let replies = reply;
                                                replies.likeCountOnReply = replies.likeOnReply.length;
                                                replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                    return likeOnReply.nibiId === nibiId
                                                }) !== undefined;
                                                let newReplyOnReply = [];
                                                reply.replyOnReply.forEach(replyOnReply => {
                                                    replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                    replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                    replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                        return likeReplyOnReply.nibiId === nibiId
                                                    }) !== undefined;
                                                    newReplyOnReply.push(replyOnReply);
                                                });
                                                replies.replyOnReply = newReplyOnReply
                                                newReply.push(replies)
                                            });
                                            posts.reply = newReply;

                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    message: errorMsgJSON[lang]["200"],
                                                    statusCode: 200
                                                },
                                                result: {
                                                    postDetails: posts
                                                },
                                            });
                                        }
                                    });
                                //   }
                                //       });
                                //   });       //option array
                                //   }
                                //    }
                                //  });
                            }
                        }
                    });
                }
            }
        });
    },




    getPollOption: (req, res, next) => {

        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });

        if (!req.body.postId || !req.body.nibiId ) {
            return true;
        }
        let findPost = {
            _id: postId,
            isDeleted: false
        };

        let aggrQuery = [
            {
                $match: {
                    '_id': mongoose.Types.ObjectId(postId),
                    'isDeleted': false
                },
            },


            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': {
                        $toString: '$_id'
                    },
                    'user': 1,
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $lookup:
                    {
                        from: 'pollings',
                        localField: 'convertedId',
                        foreignField: 'postId',
                        as: 'PostDetail'
                    }
            },

            {
                $project: {
                    'userScore': 1,
                    'eventId': 1,
                    'isDeleted': 1,
                    'nibiId': 1,
                    'hostNibiId': 1,
                    'comment': 1,
                    'postType': 1,
                    'coHosts': 1,
                    'sourceFile': 1,
                    'mention': 1,
                    'reply': 1,
                    'like': 1,
                    'addedAt': 1,
                    'convertedId': 1,
                    'user': 1,
                    'PostDetail': 1,
                    'postSize': {
                        $size: "$PostDetail"
                    },
                    'viewResultToAllGuest': 1,
                    'questionsForPoll': 1,
                    'options': 1
                },
            },

            {
                $unwind: {
                    path: "$PostDetail",
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId',
                            'optionId': '$PostDetail.optionId'
                        },
                        'countBy': {$sum: 1},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'PostDetail': {'$first': '$PostDetail'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': 0,
                        'convertedId': '$_id.convertedId',
                        'optionId': '$_id.optionId',
                        'countBy': 1,
                        'optionArrDet': {
                            'optionId': '$_id.optionId',
                            'countBy': '$countBy',
                            'optionName': '$PostDetail.optionName',
                            'optionIndex': '$PostDetail.optionIndex',
                            'percentageOfVote': {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: 0,
                                    else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                }
                            },
                            'voteByUser': {
                                $cond: {
                                    if: {
                                        $eq: [nibiId, '$PostDetail.nibiId']
                                    },
                                    then: true,
                                    else: false
                                }
                            },
                        },

                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        // 'PostDetail': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $group:
                    {
                        _id: {
                            'convertedId': '$convertedId'
                        },
                        'optionArrDet': {'$push': '$optionArrDet'},
                        'userScore': {'$first': '$userScore'},
                        'eventId': {'$first': '$eventId'},
                        'isDeleted': {'$first': '$isDeleted'},
                        'nibiId': {'$first': '$nibiId'},
                        'hostNibiId': {'$first': '$hostNibiId'},
                        'comment': {'$first': '$comment'},
                        'postType': {'$first': '$postType'},
                        'coHosts': {'$first': '$coHosts'},
                        'sourceFile': {'$first': '$sourceFile'},
                        'mention': {'$first': '$mention'},
                        'reply': {'$first': '$reply'},
                        'like': {'$first': '$like'},
                        'addedAt': {'$first': '$addedAt'},
                        'user': {'$first': '$user'},
                        'postSize': {'$first': '$postSize'},
                        'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                        'questionsForPoll': {'$first': '$questionsForPoll'},
                        'options': {'$first': '$options'}
                    }
            },

            {
                $project:
                    {
                        '_id': '$_id.convertedId',
                        'convertedId': 1,
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'user': 1,
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $lookup:
                    {
                        from: 'users',
                        localField: 'user',
                        foreignField: '_id',
                        as: 'userDetail'
                    }
            },

            {
                $project:
                    {
                        '_id': 1,
                        'convertedId': '$_id.convertedId',
                        'optionArrDet':
                            {
                                $cond: {
                                    if: {
                                        $eq: ["$postSize", 0]
                                    },
                                    then: [],
                                    else: '$optionArrDet'
                                }
                            },
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        'userDetail': {$arrayElemAt: ['$userDetail', 0]},
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },

            {
                $project:
                    {
                        'convertedId': 1,
                        'optionArrDet': 1,
                        'userScore': 1,
                        'eventId': 1,
                        'isDeleted': 1,
                        'nibiId': 1,
                        'hostNibiId': 1,
                        'comment': 1,
                        'postType': 1,
                        'coHosts': 1,
                        'sourceFile': 1,
                        'mention': 1,
                        'reply': 1,
                        'like': 1,
                        'addedAt': 1,
                        // 'userDetail': 1,
                        'user': {
                            'fname': '$userDetail.fname',
                            'lname': '$userDetail.lname',
                            'profilePic': '$userDetail.profilePic',
                            // $concat: [ "$userDetail.fname", " ", "$userDetail.lname", " ", "$userDetail.profilePic" ]
                        },
                        'postSize': 1,
                        'viewResultToAllGuest': 1,
                        'questionsForPoll': 1,
                        'options': 1
                    }
            },
        ];


        Post.aggregate(aggrQuery,
            function (err, response) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        },
                    });
                } else {

                    let optionArrDet = response[0].optionArrDet;
                    let options = response[0].options;

                    let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                        return item.optionId;
                    })

                    let dataToInsert = {};

                    let i;
                    for (i = 0; i < options.length; i++) {
                        if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                            dataToInsert = {
                                "optionId": options[i]._id.toString(),
                                "countBy": 0,
                                "optionName": options[i].optionName,
                                "optionIndex": options[i].optionIndex,
                                "percentageOfVote": 0,
                                "voteByUser": false
                            }

                            optionArrDet.push(dataToInsert)
                        }
                    }

                    response[0].optionArrDet = optionArrDet;
                    response[0].options = options;
                    let posts = response[0];
                    posts.timeDifference = moment(posts.addedAt).fromNow(true);
                    posts.isLiked = posts.like.find(like => {
                        return like.nibiId === nibiId
                    }) !== undefined;
                    posts.likeCount = posts.like.length;
                    let newReply = [];
                    posts.reply.map(reply => {
                        let replies = reply;
                        replies.likeCountOnReply = replies.likeOnReply.length;
                        replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                        replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                            return likeOnReply.nibiId === nibiId
                        }) !== undefined;
                        let newReplyOnReply = [];
                        reply.replyOnReply.forEach(replyOnReply => {
                            replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                            replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                            replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                return likeReplyOnReply.nibiId === nibiId
                            }) !== undefined;
                            newReplyOnReply.push(replyOnReply);
                        });
                        replies.replyOnReply = newReplyOnReply
                        newReply.push(replies)
                    });
                    posts.reply = newReply;

                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            postDetails: posts
                        },
                    });
                }
            });
    },





    editPostBkp: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        let sourceIds = req.body.deletedSourceIds
            ? req.body.deletedSourceIds
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide source Id's that will be delete."
                }
            });
        let sourceUrl = req.body.addedSourceFile
            ? req.body.addedSourceFile
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide source url that will be add."
                }
            });
        let comment = typeof (req.body.comment)
            ? req.body.comment
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide comment."
                }
            });
        let mention = req.body.mention
            ? req.body.mention
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide mention."
                }
            });
        if (!req.body.postId || !req.body.deletedSourceIds || !req.body.addedSourceFile || !typeof (req.body.comment) || !req.body.nibiId || !req.body.mention) {
            return true;
        }
        let findPost = {
            _id: postId,
            isDeleted: false
        };
        Post.findOne(findPost, function (err, items) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!items) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    Event.findOne({_id: items.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                let addedFile = [];
                                let removeFile = [];
                                items.sourceFile.forEach(source => {
                                    let array = sourceIds.filter(element => {
                                        return element == source._id
                                    });
                                    if (array.length > 0) {
                                        let removeObj = {sourceUrl: source.sourceUrl, sourceType: source.sourceType}
                                        removeFile.push(removeObj);
                                    } else {
                                        let addedObj = {sourceUrl: source.sourceUrl, sourceType: source.sourceType}
                                        addedFile.push(addedObj);
                                    }
                                });
                                let finalAddedArray = addedFile.concat(sourceUrl);
                                Post.updateOne(findPost, {
                                    $set: {sourceFile: [], mention: []},
                                    comment: comment
                                }, function (err, affected) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        Post.updateOne(findPost, {
                                            $push: {
                                                sourceFile: finalAddedArray,
                                                mention: mention
                                            }
                                        }, function (err, updatedPost) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                let deleteUrl = [];
                                                if (removeFile.length > 0) {
                                                    removeFile.forEach(allSourceUrl => {
                                                        let imageUrl = allSourceUrl.sourceUrl;
                                                        let imageName = imageUrl.split(Env.SPLIT_FROM[Env.MODE]);
                                                        let deletedObj = {Key: imageName[1]};
                                                        deleteUrl.push(deletedObj)
                                                    });
                                                    const params = {
                                                        Bucket: Env.BUCKET[Env.MODE].Bucket,
                                                        Delete: {
                                                            Objects: deleteUrl,
                                                            Quiet: false
                                                        }
                                                    };
                                                    s3.deleteObjects(params, function (err, data) {
                                                        console.log("Aws file deleted.")
                                                    });
                                                }

                                                Post.findOne(findPost).populate("user", "fname lname profilePic").select('-userScore').exec(function (err, finalPost) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        let posts = finalPost.toJSON();
                                                        posts.timeDifference = moment(posts.addedAt).fromNow(true);
                                                        posts.isLiked = posts.like.find(like => {
                                                            return like.nibiId === nibiId
                                                        }) !== undefined;
                                                        posts.likeCount = posts.like.length;
                                                        let newReply = [];
                                                        posts.reply.map(reply => {
                                                            let replies = reply;
                                                            replies.likeCountOnReply = replies.likeOnReply.length;
                                                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                return likeOnReply.nibiId === nibiId
                                                            }) !== undefined;
                                                            let newReplyOnReply = [];
                                                            reply.replyOnReply.forEach(replyOnReply => {
                                                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                    return likeReplyOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                newReplyOnReply.push(replyOnReply);
                                                            });
                                                            replies.replyOnReply = newReplyOnReply
                                                            newReply.push(replies)
                                                        });
                                                        posts.reply = newReply;

                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                postDetails: posts
                                                            },
                                                        });
                                                    }
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    },
    removeSourceFile: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let sourceId = req.body.sourceId
            ? req.body.sourceId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide source Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.postId || !req.body.sourceId || !req.body.nibiId) {
            return true;
        }
        let findPost = {
            _id: postId,
            isDeleted: false
        };


        Post.findOne({_id: postId}, function (err, post) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (post) {
                    Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                Post.findOne(findPost).select({sourceFile: {$elemMatch: {_id: sourceId}}}).exec(function (err, item) {
                                    if (err) {
                                        res.json({
                                            serverResponse: {
                                                isError: true,
                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                statusCode: 500
                                            },
                                        });
                                    } else {
                                        if (!item || item.sourceFile.length == 0) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["404"],
                                                    statusCode: 404
                                                }
                                            });
                                        } else {
                                            Post.updateOne(findPost, {"$pull": {"sourceFile": {"_id": sourceId}}}, {
                                                safe: true,
                                                multi: true
                                            }, function (err, updatedResponse) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500
                                                        },
                                                    });
                                                } else {
                                                    let imageUrl = item.sourceFile[0].sourceUrl;
                                                    let imageName = imageUrl.split(Env.SPLIT_FROM[Env.MODE]);

                                                    const params = {
                                                        Bucket: Env.BUCKET[Env.MODE].Bucket,
                                                        Key: imageName[1]
                                                        //  Key: "img"
                                                    };
                                                    s3.deleteObject(params, function (err, data) {
                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {
                                                            Post.findOne(findPost).populate("user", "fname lname profilePic").select('-userScore').exec(function (err, post) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        },
                                                                    });
                                                                } else {
                                                                    if (post.comment === "" && post.sourceFile.length == 0) {
                                                                        Post.updateOne(findPost, {isDeleted: true}, function (err, removeResult) {
                                                                            if (err) {
                                                                                res.json({
                                                                                    serverResponse: {
                                                                                        isError: true,
                                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                        statusCode: 500
                                                                                    },
                                                                                });
                                                                            } else {
                                                                                res.json({
                                                                                    serverResponse: {
                                                                                        isError: false,
                                                                                        message: errorMsgJSON[lang]["200"],
                                                                                        statusCode: 201
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    } else {

                                                                        let posts = post.toJSON();
                                                                        posts.timeDifference = moment(posts.addedAt).fromNow(true);
                                                                        posts.isLiked = posts.like.find(like => {
                                                                            return like.nibiId === nibiId
                                                                        }) !== undefined;
                                                                        posts.likeCount = posts.like.length;
                                                                        let newReply = [];
                                                                        posts.reply.map(reply => {
                                                                            let replies = reply;
                                                                            replies.likeCountOnReply = replies.likeOnReply.length;
                                                                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                                return likeOnReply.nibiId === nibiId
                                                                            }) !== undefined;
                                                                            let newReplyOnReply = [];
                                                                            reply.replyOnReply.forEach(replyOnReply => {
                                                                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                                    return likeReplyOnReply.nibiId === nibiId
                                                                                }) !== undefined;
                                                                                newReplyOnReply.push(replyOnReply);
                                                                            });
                                                                            replies.replyOnReply = newReplyOnReply
                                                                            newReply.push(replies)
                                                                        });
                                                                        posts.reply = newReply;


                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: errorMsgJSON[lang]["200"],
                                                                                statusCode: 200
                                                                            },
                                                                            result: {
                                                                                postDetails: posts
                                                                            },
                                                                        });
                                                                    }

                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "Post details not found.",
                            statusCode: 404
                        },
                    });
                }
            }
        });
    },
    likeDislike: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.postId || !req.body.nibiId) {
            return true;
        }
        let findPost = {
            _id: postId,
            isDeleted: false
        };

        let updateQuery = {
            nibiId: nibiId
        }
        Post.findOne(findPost, function (err, post) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (post) {
                    Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                if (req.body.likeType === true) {
                                    Post.updateOne(findPost,
                                        {$push: {"like": updateQuery}}, function (err, updatedData) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                User.findOne({nibiId: post.nibiId}, function (err, postCreator) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        User.findOne({nibiId: nibiId}, function (err, userDetails) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                saveNotificationForLikeOnPost(req, res, nibiId, post, userDetails, postCreator, postId, lang);
                                                                let nibiIds = [];
                                                                if (post.nibiId != nibiId) {
                                                                    nibiIds.push(post.nibiId);
                                                                }

                                                                // let notificationBody = {
                                                                //     title: req.body.title,
                                                                //     body: userDetails.fname + " "+ userDetails.lname+ " liked your post: " + post.comment
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your post: " + post.comment
                                                                    },
                                                                    data: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your post: " + post.comment,
                                                                        notificationType: 'liked_post',
                                                                        postId: postId,
                                                                        eventId: post.eventId
                                                                    }
                                                                };
                                                                findFcmAndSendNotification(res, nibiIds, payload)

                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: false,
                                                                        message: errorMsgJSON[lang]["200"],
                                                                        statusCode: 200
                                                                    },
                                                                    result: {
                                                                        likeCount: (post.like.length) + 1
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                                //   }
                                                //  });
                                            }
                                        })
                                } else {
                                    Post.updateOne(findPost, {"$pull": {"like": {"nibiId": nibiId}}}, {
                                        safe: true,
                                        multi: true
                                    }, function (err, removeLike) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500
                                                },
                                            });
                                        } else {
                                            //   if (removeLike.ok == 1) {
                                            // Post.updateOne(findPost, {likeCount: post.likeCount - 1}, function (err, updateResult) {
                                            //     console.log(updateResult)
                                            //     if (err) {
                                            //         res.json({
                                            //             serverResponse: {
                                            //                 isError: true,
                                            //                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            //                 statusCode: 500
                                            //             },
                                            //         });
                                            //     } else {
                                            res.json({
                                                serverResponse: {
                                                    isError: false,
                                                    message: errorMsgJSON[lang]["200"],
                                                    statusCode: 200
                                                },
                                                result: {
                                                    likeCount: (post.like.length) - 1
                                                }
                                            });
                                            //   }
                                            //  })
                                            // } else {
                                            //     res.json({
                                            //         serverResponse: {
                                            //             isError: false,
                                            //             message: errorMsgJSON[lang]["200"],
                                            //             statusCode: 200
                                            //         }
                                            //     });
                                            // }

                                        }
                                    });
                                }
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Post details not found."
                        }
                    });
                }

            }
        });
    },

    likeDislikeOnReply: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let replyId = req.body.replyId
            ? req.body.replyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide reply Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.postId || !req.body.replyId || !req.body.nibiId || !req.body.eventId) {
            return true;
        }
        let findPost = {
            _id: postId,
            "reply._id": replyId
        };
        let updateQuery = {
            nibiId: nibiId
        }

        Event.findOne({_id: eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event.guestList.length == 0) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "You are no longer participant in this event.",
                            statusCode: 410
                        },
                    });
                } else {
                    Post.findOne({
                        _id: postId,
                        isDeleted: false
                    }).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, post) {


                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (post) {
                                if (req.body.likeType === true) {
                                    Post.updateOne(findPost,
                                        {$push: {"reply.$.likeOnReply": updateQuery}}, function (err, updatedData) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {

                                                //  Post.updateOne(findPost, {"reply.$.likeCountOnReply": post.reply[0].likeCountOnReply + 1}, function (err, updateResult) {
                                                //       if (err) {
                                                //           res.json({
                                                //               serverResponse: {
                                                //                   isError: true,
                                                //                   message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                //                   statusCode: 500
                                                //               },
                                                //           });
                                                //       } else {
                                                //  Post.findOne({_id: postId}).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, count) {

                                                let aggrQuery = [
                                                    {
                                                        $match: {
                                                            '_id': mongoose.Types.ObjectId(postId),
                                                            // 'isDeleted': false
                                                        },
                                                    },


                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': {
                                                                $toString: '$_id'
                                                            },
                                                            'user': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $lookup:
                                                            {
                                                                from: 'pollings',
                                                                localField: 'convertedId',
                                                                foreignField: 'postId',
                                                                as: 'PostDetail'
                                                            }
                                                    },

                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': 1,
                                                            'user': 1,
                                                            'PostDetail': 1,
                                                            'postSize': {
                                                                $size: "$PostDetail"
                                                            },
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $unwind: {
                                                            path: "$PostDetail",
                                                            preserveNullAndEmptyArrays: true
                                                        }
                                                    },


                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId',
                                                                    'optionId': '$PostDetail.optionId'
                                                                },
                                                                'countBy': {$sum: 1},
                                                                'userScore': {'$first': '$userScore'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'isDeleted': {'$first': '$isDeleted'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                                'comment': {'$first': '$comment'},
                                                                'postType': {'$first': '$postType'},
                                                                'coHosts': {'$first': '$coHosts'},
                                                                'sourceFile': {'$first': '$sourceFile'},
                                                                'mention': {'$first': '$mention'},
                                                                'reply': {'$first': '$reply'},
                                                                'like': {'$first': '$like'},
                                                                'addedAt': {'$first': '$addedAt'},
                                                                'user': {'$first': '$user'},
                                                                'PostDetail': {'$first': '$PostDetail'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 0,
                                                                'convertedId': '$_id.convertedId',
                                                                'optionId': '$_id.optionId',
                                                                'countBy': 1,
                                                                'optionArrDet': {
                                                                    'optionId': '$_id.optionId',
                                                                    'countBy': '$countBy',
                                                                    'optionName': '$PostDetail.optionName',
                                                                    'percentageOfVote': {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: 0,
                                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                        }
                                                                    }
                                                                },

                                                                'userScore': 1,
                                                                'eventId': 1,
                                                                'isDeleted': 1,
                                                                'nibiId': 1,
                                                                'hostNibiId': 1,
                                                                'comment': 1,
                                                                'postType': 1,
                                                                'coHosts': 1,
                                                                'sourceFile': 1,
                                                                'mention': 1,
                                                                'reply': 1,
                                                                'like': 1,
                                                                'addedAt': 1,
                                                                'user': 1,
                                                                // 'PostDetail': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId'
                                                                },
                                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'reply': {'$first': '$reply'},
                                                                'user': {'$first': '$user'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': '$_id.convertedId',
                                                                'convertedId': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                'optionArrDet': 1,
                                                                'eventId': 1,
                                                                'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    }

                                                ];

                                                Post.aggregate(aggrQuery,
                                                    function (err, response) {

                                                        // Post.findOne({ _id: postId }).select('nibiId eventId reply').exec(function (err, item) {

                                                        // res.json({
                                                        //     "trrr":"trr1",
                                                        //     response: response,

                                                        // });
                                                        // return;


                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {
                                                            User.findOne({nibiId: nibiId}, function (err, userDetails) {
                                                                if (err) {
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: true,
                                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                            statusCode: 500
                                                                        },
                                                                    });
                                                                } else {
                                                                    saveNotificationForLikeOnComment(req, res, nibiId, userDetails, item, postId, lang);
                                                                    // let newNotification = new Notification({
                                                                    //     senderNibiId: nibiId,
                                                                    //     senderFirstName: userDetails.fname,
                                                                    //     senderLastName: userDetails.lname,
                                                                    //     senderImage: userDetails.profilePic,
                                                                    //     purpose: "Liked your reply in",
                                                                    //     text: item.reply[0].replyText,
                                                                    //     eventId: item.eventId,
                                                                    //     eventName: req.body.title,
                                                                    //     postId: postId,
                                                                    //     notificationType: "liked_comment",
                                                                    //     receiver: [
                                                                    //         {
                                                                    //             nibiId: item.reply[0].nibiId,
                                                                    //             firstName: item.reply[0].firstName,
                                                                    //             lastName: item.reply[0].lastName,
                                                                    //             imageUrl: item.reply[0].userImage
                                                                    //         }
                                                                    //     ],
                                                                    //     mention: item.reply[0].mentionOnReply
                                                                    // });

                                                                    //   newNotification.save(function (err, notification) {
                                                                    //        if (err) {
                                                                    //            res.json({
                                                                    //                serverResponse: {
                                                                    //                    isError: true,
                                                                    //                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    //                    statusCode: 500
                                                                    //                }
                                                                    //            });
                                                                    //        } else {
                                                                    let nibiIds = [];
                                                                    if (post.reply[0].nibiId != nibiId) {
                                                                        nibiIds.push(post.reply[0].nibiId);
                                                                    }

                                                                    // let notificationBody = {
                                                                    //     title: req.body.title,
                                                                    //     body: userDetails.fname + " "+ userDetails.lname+ " liked your reply: " + item.reply[0].replyText
                                                                    // }
                                                                    let payload = {
                                                                        notification: {
                                                                            title: req.body.title,
                                                                            body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + item.reply[0].replyText
                                                                        },
                                                                        data: {
                                                                            title: req.body.title,
                                                                            body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + item.reply[0].replyText,
                                                                            notificationType: 'liked_comment',
                                                                            postId: postId,
                                                                            eventId: item.eventId
                                                                        }
                                                                    };
                                                                    findFcmAndSendNotification(res, nibiIds, payload);

                                                                    // let posts = item.toJSON();
                                                                    let posts = response[0];
                                                                    let newReply = [];
                                                                    posts.reply.map(reply => {
                                                                        let replies = reply;
                                                                        replies.likeCountOnReply = replies.likeOnReply.length;
                                                                        replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                        replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                            return likeOnReply.nibiId === nibiId
                                                                        }) !== undefined;
                                                                        let newReplyOnReply = [];
                                                                        reply.replyOnReply.forEach(replyOnReply => {
                                                                            replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                            replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                            replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                                return likeReplyOnReply.nibiId === nibiId
                                                                            }) !== undefined;
                                                                            newReplyOnReply.push(replyOnReply);
                                                                        });
                                                                        replies.replyOnReply = newReplyOnReply
                                                                        newReply.push(replies)
                                                                    });
                                                                    posts.reply = newReply;
                                                                    res.json({
                                                                        serverResponse: {
                                                                            isError: false,
                                                                            message: errorMsgJSON[lang]["200"],
                                                                            statusCode: 200
                                                                        },
                                                                        result: {
                                                                            postDetails: posts
                                                                        }
                                                                    });
                                                                    //    }
                                                                    //  })
                                                                }
                                                            });
                                                        }
                                                    });
                                                //  }
                                                //   });
                                            }
                                        })
                                } else {
                                    Post.findOneAndUpdate({
                                            "_id": mongoose.Types.ObjectId(postId),
                                            "reply._id": mongoose.Types.ObjectId(replyId)
                                        },
                                        {$pull: {"reply.$.likeOnReply": {nibiId: nibiId}}}, {useFindAndModify: false}, function (err, removeLike) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                // Post.updateOne(findPost, {"reply.$.likeCountOnReply": post.reply[0].likeCountOnReply - 1}, function (err, updateResult) {
                                                //     if (err) {
                                                //         res.json({
                                                //             serverResponse: {
                                                //                 isError: true,
                                                //                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                //                 statusCode: 500
                                                //             },
                                                //         });
                                                //     } else {
                                                //  Post.findOne({_id: postId}).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, count) {
                                                let aggrQuery = [
                                                    {
                                                        $match: {
                                                            '_id': mongoose.Types.ObjectId(postId),
                                                            // 'isDeleted': false
                                                        },
                                                    },


                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': {
                                                                $toString: '$_id'
                                                            },
                                                            'user': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $lookup:
                                                            {
                                                                from: 'pollings',
                                                                localField: 'convertedId',
                                                                foreignField: 'postId',
                                                                as: 'PostDetail'
                                                            }
                                                    },

                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': 1,
                                                            'user': 1,
                                                            'PostDetail': 1,
                                                            'postSize': {
                                                                $size: "$PostDetail"
                                                            },
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $unwind: {
                                                            path: "$PostDetail",
                                                            preserveNullAndEmptyArrays: true
                                                        }
                                                    },


                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId',
                                                                    'optionId': '$PostDetail.optionId'
                                                                },
                                                                'countBy': {$sum: 1},
                                                                'userScore': {'$first': '$userScore'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'isDeleted': {'$first': '$isDeleted'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                                'comment': {'$first': '$comment'},
                                                                'postType': {'$first': '$postType'},
                                                                'coHosts': {'$first': '$coHosts'},
                                                                'sourceFile': {'$first': '$sourceFile'},
                                                                'mention': {'$first': '$mention'},
                                                                'reply': {'$first': '$reply'},
                                                                'like': {'$first': '$like'},
                                                                'addedAt': {'$first': '$addedAt'},
                                                                'user': {'$first': '$user'},
                                                                'PostDetail': {'$first': '$PostDetail'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 0,
                                                                'convertedId': '$_id.convertedId',
                                                                'optionId': '$_id.optionId',
                                                                'countBy': 1,
                                                                'optionArrDet': {
                                                                    'optionId': '$_id.optionId',
                                                                    'countBy': '$countBy',
                                                                    'optionName': '$PostDetail.optionName',
                                                                    'percentageOfVote': {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: 0,
                                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                        }
                                                                    }
                                                                },

                                                                'userScore': 1,
                                                                'eventId': 1,
                                                                'isDeleted': 1,
                                                                'nibiId': 1,
                                                                'hostNibiId': 1,
                                                                'comment': 1,
                                                                'postType': 1,
                                                                'coHosts': 1,
                                                                'sourceFile': 1,
                                                                'mention': 1,
                                                                'reply': 1,
                                                                'like': 1,
                                                                'addedAt': 1,
                                                                'user': 1,
                                                                // 'PostDetail': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId'
                                                                },
                                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                                // 'eventId': { '$first': '$eventId' },
                                                                // 'nibiId': { '$first': '$nibiId' },
                                                                'reply': {'$first': '$reply'},
                                                                'user': {'$first': '$user'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': '$_id.convertedId',
                                                                'convertedId': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                // 'eventId': 1,
                                                                // 'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                // 'eventId': 1,
                                                                // 'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                'optionArrDet': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    }

                                                ];

                                                Post.aggregate(aggrQuery,
                                                    function (err, response) {

                                                        // Post.findOne({ _id: postId }).select('reply').exec(function (err, item) {

                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {

                                                            //let posts = item.toJSON();
                                                            let posts = response[0];
                                                            let newReply = [];
                                                            posts.reply.map(reply => {
                                                                let replies = reply;
                                                                replies.likeCountOnReply = replies.likeOnReply.length;
                                                                replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                    return likeOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                let newReplyOnReply = [];
                                                                reply.replyOnReply.forEach(replyOnReply => {
                                                                    replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                    replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                    replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                        return likeReplyOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    newReplyOnReply.push(replyOnReply);
                                                                });
                                                                replies.replyOnReply = newReplyOnReply
                                                                newReply.push(replies)
                                                            });
                                                            posts.reply = newReply;

                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: errorMsgJSON[lang]["200"],
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    postDetails: posts
                                                                }
                                                            });
                                                        }
                                                    });
                                                //    }
                                                //    })
                                            }
                                        });
                                }
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Post details not found."
                                    }
                                });
                            }

                        }
                    });
                }
            }
        });
    },

    likeDislikeOnReplyBKP: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let replyId = req.body.replyId
            ? req.body.replyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide reply Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.postId || !req.body.replyId || !req.body.nibiId || !req.body.eventId) {
            return true;
        }
        let findPost = {
            _id: postId,
            "reply._id": replyId
        };
        let updateQuery = {
            nibiId: nibiId
        }

        Event.findOne({_id: eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event.guestList.length == 0) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "You are no longer participant in this event.",
                            statusCode: 410
                        },
                    });
                } else {
                    Post.findOne({
                        _id: postId,
                        isDeleted: false
                    }).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, post) {

                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (post) {
                                if (req.body.likeType === true) {
                                    Post.updateOne(findPost,
                                        {$push: {"reply.$.likeOnReply": updateQuery}}, function (err, updatedData) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {

                                                //  Post.updateOne(findPost, {"reply.$.likeCountOnReply": post.reply[0].likeCountOnReply + 1}, function (err, updateResult) {
                                                //       if (err) {
                                                //           res.json({
                                                //               serverResponse: {
                                                //                   isError: true,
                                                //                   message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                //                   statusCode: 500
                                                //               },
                                                //           });
                                                //       } else {
                                                //  Post.findOne({_id: postId}).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, count) {
                                                Post.findOne({_id: postId}).select('nibiId eventId reply').exec(function (err, item) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        User.findOne({nibiId: nibiId}, function (err, userDetails) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                saveNotificationForLikeOnComment(req, res, nibiId, userDetails, item, postId, lang);
                                                                // let newNotification = new Notification({
                                                                //     senderNibiId: nibiId,
                                                                //     senderFirstName: userDetails.fname,
                                                                //     senderLastName: userDetails.lname,
                                                                //     senderImage: userDetails.profilePic,
                                                                //     purpose: "Liked your reply in",
                                                                //     text: item.reply[0].replyText,
                                                                //     eventId: item.eventId,
                                                                //     eventName: req.body.title,
                                                                //     postId: postId,
                                                                //     notificationType: "liked_comment",
                                                                //     receiver: [
                                                                //         {
                                                                //             nibiId: item.reply[0].nibiId,
                                                                //             firstName: item.reply[0].firstName,
                                                                //             lastName: item.reply[0].lastName,
                                                                //             imageUrl: item.reply[0].userImage
                                                                //         }
                                                                //     ],
                                                                //     mention: item.reply[0].mentionOnReply
                                                                // });

                                                                //   newNotification.save(function (err, notification) {
                                                                //        if (err) {
                                                                //            res.json({
                                                                //                serverResponse: {
                                                                //                    isError: true,
                                                                //                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                //                    statusCode: 500
                                                                //                }
                                                                //            });
                                                                //        } else {
                                                                let nibiIds = [];
                                                                if (post.reply[0].nibiId != nibiId) {
                                                                    nibiIds.push(post.reply[0].nibiId);
                                                                }

                                                                // let notificationBody = {
                                                                //     title: req.body.title,
                                                                //     body: userDetails.fname + " "+ userDetails.lname+ " liked your reply: " + item.reply[0].replyText
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + item.reply[0].replyText
                                                                    },
                                                                    data: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + item.reply[0].replyText,
                                                                        notificationType: 'liked_comment',
                                                                        postId: postId,
                                                                        eventId: item.eventId
                                                                    }
                                                                };
                                                                findFcmAndSendNotification(res, nibiIds, payload);

                                                                let posts = item.toJSON();
                                                                let newReply = [];
                                                                posts.reply.map(reply => {
                                                                    let replies = reply;
                                                                    replies.likeCountOnReply = replies.likeOnReply.length;
                                                                    replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                    replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                        return likeOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    let newReplyOnReply = [];
                                                                    reply.replyOnReply.forEach(replyOnReply => {
                                                                        replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                        replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                        replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                            return likeReplyOnReply.nibiId === nibiId
                                                                        }) !== undefined;
                                                                        newReplyOnReply.push(replyOnReply);
                                                                    });
                                                                    replies.replyOnReply = newReplyOnReply
                                                                    newReply.push(replies)
                                                                });
                                                                posts.reply = newReply;
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: false,
                                                                        message: errorMsgJSON[lang]["200"],
                                                                        statusCode: 200
                                                                    },
                                                                    result: {
                                                                        postDetails: posts
                                                                    }
                                                                });
                                                                //    }
                                                                //  })
                                                            }
                                                        });
                                                    }
                                                });
                                                //  }
                                                //   });
                                            }
                                        })
                                } else {
                                    Post.findOneAndUpdate({
                                            "_id": mongoose.Types.ObjectId(postId),
                                            "reply._id": mongoose.Types.ObjectId(replyId)
                                        },
                                        {$pull: {"reply.$.likeOnReply": {nibiId: nibiId}}}, {useFindAndModify: false}, function (err, removeLike) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                // Post.updateOne(findPost, {"reply.$.likeCountOnReply": post.reply[0].likeCountOnReply - 1}, function (err, updateResult) {
                                                //     if (err) {
                                                //         res.json({
                                                //             serverResponse: {
                                                //                 isError: true,
                                                //                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                //                 statusCode: 500
                                                //             },
                                                //         });
                                                //     } else {
                                                //  Post.findOne({_id: postId}).select({reply: {$elemMatch: {_id: replyId}}}).exec(function (err, count) {
                                                Post.findOne({_id: postId}).select('reply').exec(function (err, item) {

                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {

                                                        let posts = item.toJSON();
                                                        let newReply = [];
                                                        posts.reply.map(reply => {
                                                            let replies = reply;
                                                            replies.likeCountOnReply = replies.likeOnReply.length;
                                                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                return likeOnReply.nibiId === nibiId
                                                            }) !== undefined;
                                                            let newReplyOnReply = [];
                                                            reply.replyOnReply.forEach(replyOnReply => {
                                                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                    return likeReplyOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                newReplyOnReply.push(replyOnReply);
                                                            });
                                                            replies.replyOnReply = newReplyOnReply
                                                            newReply.push(replies)
                                                        });
                                                        posts.reply = newReply;

                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                postDetails: posts
                                                            }
                                                        });
                                                    }
                                                });
                                                //    }
                                                //    })
                                            }
                                        });
                                }
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Post details not found."
                                    }
                                });
                            }

                        }
                    });
                }
            }
        });
    },


    likeDislikeReplyOnReply: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let replyId = req.body.replyId
            ? req.body.replyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide reply Id."
                }
            });
        let replyOnReplyId = req.body.replyOnReplyId
            ? req.body.replyOnReplyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide reply on reply Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.postId || !req.body.replyId || !req.body.replyOnReplyId || !req.body.nibiId) {
            return true;
        }
        let findPost = {
            _id: postId
        };
        let findLikeFromReply = {
            _id: postId,
            "reply._id": replyId,
            "reply.replyOnReply._id": replyOnReplyId,
            "reply.$.replyOnReply.$.likeOnReplyOnReply.nibiId": nibiId
        };

        let updateQuery = {
            nibiId: nibiId
        };


        let aggrigate = ([
            {
                "$match": {
                    "reply.replyOnReply._id": mongoose.Types.ObjectId(replyOnReplyId)
                }
            },
            {"$unwind": "$reply"},
            {"$unwind": "$reply.replyOnReply"},
            {
                "$group": {
                    "_id": "$_id",
                    "replyOnReply": {"$first": "$reply.replyOnReply"}
                }
            },
            {"$project": {"_id": 0, "replyOnReply": 1}},
            {"$limit": 1}
        ])


        Event.findOne({_id: eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {


            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event.guestList.length == 0) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "You are no longer participant in this event.",
                            statusCode: 410
                        },
                    });
                } else {
                    Post.aggregate(aggrigate).exec(function (err, post) {
                        //  console.log(post[0].replyOnReply.likeOnReplyOnReply);
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (post) {
                                if (req.body.likeType == true) {
                                    Post.updateOne(
                                        {"_id": mongoose.Types.ObjectId(postId)},
                                        {
                                            $push: {
                                                "reply.$[].replyOnReply.$[element].likeOnReplyOnReply": {
                                                    "nibiId": nibiId
                                                }
                                            }
                                        },
                                        {
                                            arrayFilters: [{"element._id": mongoose.Types.ObjectId(replyOnReplyId)}]
                                        }, function (err, updatedData) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {

                                                User.findOne({nibiId: nibiId}, function (err, userDetails) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        Post.findOne(findPost, function (err, findEventId) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                saveNotificationForLikeOnReply(req, res, nibiId, userDetails, findEventId, postId, lang);
                                                                // let newNotification = new Notification({
                                                                //     senderNibiId: nibiId,
                                                                //     senderFirstName: userDetails.fname,
                                                                //     senderLastName: userDetails.lname,
                                                                //     senderImage: userDetails.profilePic,
                                                                //     purpose: "liked your reply in",
                                                                //     text: req.body.replyText,
                                                                //     eventId: findEventId.eventId,
                                                                //     eventName: req.body.title,
                                                                //     postId: postId,
                                                                //     notificationType: "liked_reply",
                                                                //     receiver: [
                                                                //         {
                                                                //             nibiId: req.body.replyOnReplyNibiId,
                                                                //             firstName: req.body.firstName,
                                                                //             lastName: req.body.lastName,
                                                                //             imageUrl: req.body.userImage
                                                                //         }
                                                                //     ]
                                                                // });

                                                                // newNotification.save(function (err, notification) {
                                                                //     if (err) {
                                                                //         res.json({
                                                                //             serverResponse: {
                                                                //                 isError: true,
                                                                //                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                //                 statusCode: 500
                                                                //             }
                                                                //         });
                                                                //     } else {
                                                                let nibiIds = [];
                                                                if (req.body.replyOnReplyNibiId != nibiId) {
                                                                    nibiIds.push(req.body.replyOnReplyNibiId);
                                                                }
                                                                // let notificationBody = {
                                                                //     title: req.body.title,
                                                                //     body: userDetails.fname + " "+ userDetails.lname+ " liked your reply: " + req.body.replyText
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + req.body.replyText
                                                                    },
                                                                    data: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + req.body.replyText,
                                                                        notificationType: 'liked_reply',
                                                                        postId: postId,
                                                                        eventId: findEventId.eventId
                                                                    }
                                                                };
                                                                findFcmAndSendNotification(res, nibiIds, payload);

                                                                let aggrQuery = [
                                                                    {
                                                                        $match: {
                                                                            '_id': mongoose.Types.ObjectId(postId),
                                                                            // 'isDeleted': false
                                                                        },
                                                                    },


                                                                    {
                                                                        $project: {
                                                                            'userScore': 1,
                                                                            'eventId': 1,
                                                                            'isDeleted': 1,
                                                                            'nibiId': 1,
                                                                            'hostNibiId': 1,
                                                                            'comment': 1,
                                                                            'postType': 1,
                                                                            'coHosts': 1,
                                                                            'sourceFile': 1,
                                                                            'mention': 1,
                                                                            'reply': 1,
                                                                            'like': 1,
                                                                            'addedAt': 1,
                                                                            'convertedId': {
                                                                                $toString: '$_id'
                                                                            },
                                                                            'user': 1,
                                                                            'viewResultToAllGuest': 1,
                                                                            'questionsForPoll': 1,
                                                                            'options': 1
                                                                        },
                                                                    },

                                                                    {
                                                                        $lookup:
                                                                            {
                                                                                from: 'pollings',
                                                                                localField: 'convertedId',
                                                                                foreignField: 'postId',
                                                                                as: 'PostDetail'
                                                                            }
                                                                    },

                                                                    {
                                                                        $project: {
                                                                            'userScore': 1,
                                                                            'eventId': 1,
                                                                            'isDeleted': 1,
                                                                            'nibiId': 1,
                                                                            'hostNibiId': 1,
                                                                            'comment': 1,
                                                                            'postType': 1,
                                                                            'coHosts': 1,
                                                                            'sourceFile': 1,
                                                                            'mention': 1,
                                                                            'reply': 1,
                                                                            'like': 1,
                                                                            'addedAt': 1,
                                                                            'convertedId': 1,
                                                                            'user': 1,
                                                                            'PostDetail': 1,
                                                                            'postSize': {
                                                                                $size: "$PostDetail"
                                                                            },
                                                                            'viewResultToAllGuest': 1,
                                                                            'questionsForPoll': 1,
                                                                            'options': 1
                                                                        },
                                                                    },

                                                                    {
                                                                        $unwind: {
                                                                            path: "$PostDetail",
                                                                            preserveNullAndEmptyArrays: true
                                                                        }
                                                                    },


                                                                    {
                                                                        $group:
                                                                            {
                                                                                _id: {
                                                                                    'convertedId': '$convertedId',
                                                                                    'optionId': '$PostDetail.optionId'
                                                                                },
                                                                                'countBy': {$sum: 1},
                                                                                'userScore': {'$first': '$userScore'},
                                                                                'eventId': {'$first': '$eventId'},
                                                                                'isDeleted': {'$first': '$isDeleted'},
                                                                                'nibiId': {'$first': '$nibiId'},
                                                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                                                'comment': {'$first': '$comment'},
                                                                                'postType': {'$first': '$postType'},
                                                                                'coHosts': {'$first': '$coHosts'},
                                                                                'sourceFile': {'$first': '$sourceFile'},
                                                                                'mention': {'$first': '$mention'},
                                                                                'reply': {'$first': '$reply'},
                                                                                'like': {'$first': '$like'},
                                                                                'addedAt': {'$first': '$addedAt'},
                                                                                'user': {'$first': '$user'},
                                                                                'PostDetail': {'$first': '$PostDetail'},
                                                                                'postSize': {'$first': '$postSize'},
                                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                                'options': {'$first': '$options'}
                                                                            }
                                                                    },

                                                                    {
                                                                        $project:
                                                                            {
                                                                                '_id': 0,
                                                                                'convertedId': '$_id.convertedId',
                                                                                'optionId': '$_id.optionId',
                                                                                'countBy': 1,
                                                                                'optionArrDet': {
                                                                                    'optionId': '$_id.optionId',
                                                                                    'countBy': '$countBy',
                                                                                    'optionName': '$PostDetail.optionName',
                                                                                    'percentageOfVote': {
                                                                                        $cond: {
                                                                                            if: {
                                                                                                $eq: ["$postSize", 0]
                                                                                            },
                                                                                            then: 0,
                                                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                                        }
                                                                                    }
                                                                                },

                                                                                'userScore': 1,
                                                                                'eventId': 1,
                                                                                'isDeleted': 1,
                                                                                'nibiId': 1,
                                                                                'hostNibiId': 1,
                                                                                'comment': 1,
                                                                                'postType': 1,
                                                                                'coHosts': 1,
                                                                                'sourceFile': 1,
                                                                                'mention': 1,
                                                                                'reply': 1,
                                                                                'like': 1,
                                                                                'addedAt': 1,
                                                                                'user': 1,
                                                                                // 'PostDetail': 1,
                                                                                'postSize': 1,
                                                                                'viewResultToAllGuest': 1,
                                                                                'questionsForPoll': 1,
                                                                                'options': 1
                                                                            }
                                                                    },

                                                                    {
                                                                        $group:
                                                                            {
                                                                                _id: {
                                                                                    'convertedId': '$convertedId'
                                                                                },
                                                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                                                'eventId': {'$first': '$eventId'},
                                                                                'nibiId': {'$first': '$nibiId'},
                                                                                'reply': {'$first': '$reply'},
                                                                                'user': {'$first': '$user'},
                                                                                'postSize': {'$first': '$postSize'},
                                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                                'options': {'$first': '$options'}
                                                                            }
                                                                    },

                                                                    {
                                                                        $project:
                                                                            {
                                                                                '_id': '$_id.convertedId',
                                                                                'convertedId': 1,
                                                                                'optionArrDet':
                                                                                    {
                                                                                        $cond: {
                                                                                            if: {
                                                                                                $eq: ["$postSize", 0]
                                                                                            },
                                                                                            then: [],
                                                                                            else: '$optionArrDet'
                                                                                        }
                                                                                    },
                                                                                'eventId': 1,
                                                                                'nibiId': 1,
                                                                                'reply': 1,
                                                                                'postSize': 1,
                                                                                'viewResultToAllGuest': 1,
                                                                                'questionsForPoll': 1,
                                                                                'options': 1
                                                                            }
                                                                    },

                                                                    {
                                                                        $project:
                                                                            {
                                                                                '_id': 1,
                                                                                'optionArrDet':
                                                                                    {
                                                                                        $cond: {
                                                                                            if: {
                                                                                                $eq: ["$postSize", 0]
                                                                                            },
                                                                                            then: [],
                                                                                            else: '$optionArrDet'
                                                                                        }
                                                                                    },
                                                                                'eventId': 1,
                                                                                'nibiId': 1,
                                                                                'reply': 1,
                                                                                'postSize': 1,
                                                                                'viewResultToAllGuest': 1,
                                                                                'questionsForPoll': 1,
                                                                                'options': 1
                                                                            }
                                                                    },

                                                                    {
                                                                        $project:
                                                                            {
                                                                                'optionArrDet': 1,
                                                                                'eventId': 1,
                                                                                'nibiId': 1,
                                                                                'reply': 1,
                                                                                'postSize': 1,
                                                                                'viewResultToAllGuest': 1,
                                                                                'questionsForPoll': 1,
                                                                                'options': 1
                                                                            }
                                                                    }

                                                                ];

                                                                Post.aggregate(aggrQuery,
                                                                    function (err, response) {

                                                                        // Post.findOne({ _id: postId }).select('reply').exec(function (err, item) {

                                                                        if (err) {
                                                                            res.json({
                                                                                serverResponse: {
                                                                                    isError: true,
                                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                    statusCode: 500
                                                                                },
                                                                            });
                                                                        } else {
                                                                            // let posts = item.toJSON();

                                                                            let posts = response[0];
                                                                            let newReply = [];
                                                                            posts.reply.map(reply => {
                                                                                let replies = reply;
                                                                                replies.likeCountOnReply = replies.likeOnReply.length;
                                                                                replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                                replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                                    return likeOnReply.nibiId === nibiId
                                                                                }) !== undefined;
                                                                                let newReplyOnReply = [];
                                                                                reply.replyOnReply.forEach(replyOnReply => {
                                                                                    replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                                    replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                                    replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                                        return likeReplyOnReply.nibiId === nibiId
                                                                                    }) !== undefined;
                                                                                    newReplyOnReply.push(replyOnReply);
                                                                                });
                                                                                replies.replyOnReply = newReplyOnReply
                                                                                newReply.push(replies)
                                                                            });
                                                                            posts.reply = newReply;

                                                                            res.json({
                                                                                serverResponse: {
                                                                                    isError: false,
                                                                                    message: errorMsgJSON[lang]["200"],
                                                                                    statusCode: 200
                                                                                },
                                                                                result: {
                                                                                    postDetails: posts
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                //   }
                                                                //   });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        })
                                } else {
                                    Post.update(
                                        {"_id": mongoose.Types.ObjectId(postId)},
                                        {
                                            $pull: {
                                                "reply.$[].replyOnReply.$[element].likeOnReplyOnReply": {
                                                    "nibiId": nibiId
                                                }
                                            }
                                        },
                                        {
                                            arrayFilters: [{"element._id": mongoose.Types.ObjectId(replyOnReplyId)}]
                                        }, function (err, removeLike) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {

                                                let aggrQuery = [
                                                    {
                                                        $match: {
                                                            '_id': mongoose.Types.ObjectId(postId),
                                                            // 'isDeleted': false
                                                        },
                                                    },


                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': {
                                                                $toString: '$_id'
                                                            },
                                                            'user': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $lookup:
                                                            {
                                                                from: 'pollings',
                                                                localField: 'convertedId',
                                                                foreignField: 'postId',
                                                                as: 'PostDetail'
                                                            }
                                                    },

                                                    {
                                                        $project: {
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'convertedId': 1,
                                                            'user': 1,
                                                            'PostDetail': 1,
                                                            'postSize': {
                                                                $size: "$PostDetail"
                                                            },
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        },
                                                    },

                                                    {
                                                        $unwind: {
                                                            path: "$PostDetail",
                                                            preserveNullAndEmptyArrays: true
                                                        }
                                                    },


                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId',
                                                                    'optionId': '$PostDetail.optionId'
                                                                },
                                                                'countBy': {$sum: 1},
                                                                'userScore': {'$first': '$userScore'},
                                                                'eventId': {'$first': '$eventId'},
                                                                'isDeleted': {'$first': '$isDeleted'},
                                                                'nibiId': {'$first': '$nibiId'},
                                                                'hostNibiId': {'$first': '$hostNibiId'},
                                                                'comment': {'$first': '$comment'},
                                                                'postType': {'$first': '$postType'},
                                                                'coHosts': {'$first': '$coHosts'},
                                                                'sourceFile': {'$first': '$sourceFile'},
                                                                'mention': {'$first': '$mention'},
                                                                'reply': {'$first': '$reply'},
                                                                'like': {'$first': '$like'},
                                                                'addedAt': {'$first': '$addedAt'},
                                                                'user': {'$first': '$user'},
                                                                'PostDetail': {'$first': '$PostDetail'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 0,
                                                                'convertedId': '$_id.convertedId',
                                                                'optionId': '$_id.optionId',
                                                                'countBy': 1,
                                                                'optionArrDet': {
                                                                    'optionId': '$_id.optionId',
                                                                    'countBy': '$countBy',
                                                                    'optionName': '$PostDetail.optionName',
                                                                    'percentageOfVote': {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: 0,
                                                                            else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                        }
                                                                    }
                                                                },

                                                                'userScore': 1,
                                                                'eventId': 1,
                                                                'isDeleted': 1,
                                                                'nibiId': 1,
                                                                'hostNibiId': 1,
                                                                'comment': 1,
                                                                'postType': 1,
                                                                'coHosts': 1,
                                                                'sourceFile': 1,
                                                                'mention': 1,
                                                                'reply': 1,
                                                                'like': 1,
                                                                'addedAt': 1,
                                                                'user': 1,
                                                                // 'PostDetail': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $group:
                                                            {
                                                                _id: {
                                                                    'convertedId': '$convertedId'
                                                                },
                                                                'optionArrDet': {'$push': '$optionArrDet'},
                                                                // 'eventId': { '$first': '$eventId' },
                                                                // 'nibiId': { '$first': '$nibiId' },
                                                                'reply': {'$first': '$reply'},
                                                                'user': {'$first': '$user'},
                                                                'postSize': {'$first': '$postSize'},
                                                                'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                                'questionsForPoll': {'$first': '$questionsForPoll'},
                                                                'options': {'$first': '$options'}
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': '$_id.convertedId',
                                                                'convertedId': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                // 'eventId': 1,
                                                                // 'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                '_id': 1,
                                                                'optionArrDet':
                                                                    {
                                                                        $cond: {
                                                                            if: {
                                                                                $eq: ["$postSize", 0]
                                                                            },
                                                                            then: [],
                                                                            else: '$optionArrDet'
                                                                        }
                                                                    },
                                                                // 'eventId': 1,
                                                                // 'nibiId': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    },

                                                    {
                                                        $project:
                                                            {
                                                                'optionArrDet': 1,
                                                                'reply': 1,
                                                                'postSize': 1,
                                                                'viewResultToAllGuest': 1,
                                                                'questionsForPoll': 1,
                                                                'options': 1
                                                            }
                                                    }

                                                ];

                                                Post.aggregate(aggrQuery,
                                                    function (err, response) {
                                                        // Post.findOne({ _id: postId }).select('reply').exec(function (err, item) {
                                                        // res.json({
                                                        //     item: response[0];
                                                        // });
                                                        // return;

                                                        if (err) {
                                                            res.json({
                                                                serverResponse: {
                                                                    isError: true,
                                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                    statusCode: 500
                                                                },
                                                            });
                                                        } else {


                                                            // let posts = item.toJSON();
                                                            let posts = response[0];
                                                            let newReply = [];
                                                            posts.reply.map(reply => {
                                                                let replies = reply;
                                                                replies.likeCountOnReply = replies.likeOnReply.length;
                                                                replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                    return likeOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                let newReplyOnReply = [];
                                                                reply.replyOnReply.forEach(replyOnReply => {
                                                                    replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                    replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                    replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                        return likeReplyOnReply.nibiId === nibiId
                                                                    }) !== undefined;
                                                                    newReplyOnReply.push(replyOnReply);
                                                                });
                                                                replies.replyOnReply = newReplyOnReply
                                                                newReply.push(replies)
                                                            });
                                                            posts.reply = newReply;

                                                            res.json({
                                                                serverResponse: {
                                                                    isError: false,
                                                                    message: errorMsgJSON[lang]["200"],
                                                                    statusCode: 200
                                                                },
                                                                result: {
                                                                    postDetails: posts
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                }
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Post details not found."
                                    }
                                });
                            }

                        }
                    });
                }
            }
        });
    },


    likeDislikeReplyOnReplyBkp: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let eventId = req.body.eventId
            ? req.body.eventId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide event Id."
                }
            });
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post Id."
                }
            });
        let replyId = req.body.replyId
            ? req.body.replyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide reply Id."
                }
            });
        let replyOnReplyId = req.body.replyOnReplyId
            ? req.body.replyOnReplyId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide reply on reply Id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        if (!req.body.eventId || !req.body.postId || !req.body.replyId || !req.body.replyOnReplyId || !req.body.nibiId) {
            return true;
        }
        let findPost = {
            _id: postId
        };
        let findLikeFromReply = {
            _id: postId,
            "reply._id": replyId,
            "reply.replyOnReply._id": replyOnReplyId,
            "reply.$.replyOnReply.$.likeOnReplyOnReply.nibiId": nibiId
        };

        let updateQuery = {
            nibiId: nibiId
        };


        let aggrigate = ([
            {
                "$match": {
                    "reply.replyOnReply._id": mongoose.Types.ObjectId(replyOnReplyId)
                }
            },
            {"$unwind": "$reply"},
            {"$unwind": "$reply.replyOnReply"},
            {
                "$group": {
                    "_id": "$_id",
                    "replyOnReply": {"$first": "$reply.replyOnReply"}
                }
            },
            {"$project": {"_id": 0, "replyOnReply": 1}},
            {"$limit": 1}
        ])


        Event.findOne({_id: eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (event.guestList.length == 0) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "You are no longer participant in this event.",
                            statusCode: 410
                        },
                    });
                } else {
                    Post.aggregate(aggrigate).exec(function (err, post) {
                        //  console.log(post[0].replyOnReply.likeOnReplyOnReply);

                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (post) {
                                if (req.body.likeType == true) {
                                    Post.updateOne(
                                        {"_id": mongoose.Types.ObjectId(postId)},
                                        {
                                            $push: {
                                                "reply.$[].replyOnReply.$[element].likeOnReplyOnReply": {
                                                    "nibiId": nibiId
                                                }
                                            }
                                        },
                                        {
                                            arrayFilters: [{"element._id": mongoose.Types.ObjectId(replyOnReplyId)}]
                                        }, function (err, updatedData) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {

                                                User.findOne({nibiId: nibiId}, function (err, userDetails) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {
                                                        Post.findOne(findPost, function (err, findEventId) {
                                                            if (err) {
                                                                res.json({
                                                                    serverResponse: {
                                                                        isError: true,
                                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                        statusCode: 500
                                                                    },
                                                                });
                                                            } else {
                                                                saveNotificationForLikeOnReply(req, res, nibiId, userDetails, findEventId, postId, lang);
                                                                // let newNotification = new Notification({
                                                                //     senderNibiId: nibiId,
                                                                //     senderFirstName: userDetails.fname,
                                                                //     senderLastName: userDetails.lname,
                                                                //     senderImage: userDetails.profilePic,
                                                                //     purpose: "liked your reply in",
                                                                //     text: req.body.replyText,
                                                                //     eventId: findEventId.eventId,
                                                                //     eventName: req.body.title,
                                                                //     postId: postId,
                                                                //     notificationType: "liked_reply",
                                                                //     receiver: [
                                                                //         {
                                                                //             nibiId: req.body.replyOnReplyNibiId,
                                                                //             firstName: req.body.firstName,
                                                                //             lastName: req.body.lastName,
                                                                //             imageUrl: req.body.userImage
                                                                //         }
                                                                //     ]
                                                                // });

                                                                // newNotification.save(function (err, notification) {
                                                                //     if (err) {
                                                                //         res.json({
                                                                //             serverResponse: {
                                                                //                 isError: true,
                                                                //                 message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                //                 statusCode: 500
                                                                //             }
                                                                //         });
                                                                //     } else {
                                                                let nibiIds = [];
                                                                if (req.body.replyOnReplyNibiId != nibiId) {
                                                                    nibiIds.push(req.body.replyOnReplyNibiId);
                                                                }
                                                                // let notificationBody = {
                                                                //     title: req.body.title,
                                                                //     body: userDetails.fname + " "+ userDetails.lname+ " liked your reply: " + req.body.replyText
                                                                // }
                                                                let payload = {
                                                                    notification: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + req.body.replyText
                                                                    },
                                                                    data: {
                                                                        title: req.body.title,
                                                                        body: userDetails.fname + " " + userDetails.lname + " liked your reply: " + req.body.replyText,
                                                                        notificationType: 'liked_reply',
                                                                        postId: postId,
                                                                        eventId: findEventId.eventId
                                                                    }
                                                                };
                                                                findFcmAndSendNotification(res, nibiIds, payload);
                                                                Post.findOne({_id: postId}).select('reply').exec(function (err, item) {
                                                                    if (err) {
                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: true,
                                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                                statusCode: 500
                                                                            },
                                                                        });
                                                                    } else {
                                                                        let posts = item.toJSON();
                                                                        let newReply = [];
                                                                        posts.reply.map(reply => {
                                                                            let replies = reply;
                                                                            replies.likeCountOnReply = replies.likeOnReply.length;
                                                                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                                return likeOnReply.nibiId === nibiId
                                                                            }) !== undefined;
                                                                            let newReplyOnReply = [];
                                                                            reply.replyOnReply.forEach(replyOnReply => {
                                                                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                                    return likeReplyOnReply.nibiId === nibiId
                                                                                }) !== undefined;
                                                                                newReplyOnReply.push(replyOnReply);
                                                                            });
                                                                            replies.replyOnReply = newReplyOnReply
                                                                            newReply.push(replies)
                                                                        });
                                                                        posts.reply = newReply;

                                                                        res.json({
                                                                            serverResponse: {
                                                                                isError: false,
                                                                                message: errorMsgJSON[lang]["200"],
                                                                                statusCode: 200
                                                                            },
                                                                            result: {
                                                                                postDetails: posts
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                                //   }
                                                                //   });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        })
                                } else {
                                    Post.update(
                                        {"_id": mongoose.Types.ObjectId(postId)},
                                        {
                                            $pull: {
                                                "reply.$[].replyOnReply.$[element].likeOnReplyOnReply": {
                                                    "nibiId": nibiId
                                                }
                                            }
                                        },
                                        {
                                            arrayFilters: [{"element._id": mongoose.Types.ObjectId(replyOnReplyId)}]
                                        }, function (err, removeLike) {
                                            if (err) {
                                                res.json({
                                                    serverResponse: {
                                                        isError: true,
                                                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                        statusCode: 500
                                                    },
                                                });
                                            } else {
                                                Post.findOne({_id: postId}).select('reply').exec(function (err, item) {

                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500
                                                            },
                                                        });
                                                    } else {


                                                        let posts = item.toJSON();
                                                        let newReply = [];
                                                        posts.reply.map(reply => {
                                                            let replies = reply;
                                                            replies.likeCountOnReply = replies.likeOnReply.length;
                                                            replies.timeDifferenceOnReply = moment(replies.createdAt).fromNow(true);
                                                            replies.isLikedOnReply = replies.likeOnReply.find(likeOnReply => {
                                                                return likeOnReply.nibiId === nibiId
                                                            }) !== undefined;
                                                            let newReplyOnReply = [];
                                                            reply.replyOnReply.forEach(replyOnReply => {
                                                                replyOnReply.likeCountOnReplyOnReply = replyOnReply.likeOnReplyOnReply.length
                                                                replyOnReply.timeDifferenceReplyOnReply = moment(replyOnReply.createdAt).fromNow(true);
                                                                replyOnReply.isLikedReplyOnReply = replyOnReply.likeOnReplyOnReply.find(likeReplyOnReply => {
                                                                    return likeReplyOnReply.nibiId === nibiId
                                                                }) !== undefined;
                                                                newReplyOnReply.push(replyOnReply);
                                                            });
                                                            replies.replyOnReply = newReplyOnReply
                                                            newReply.push(replies)
                                                        });
                                                        posts.reply = newReply;

                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                postDetails: posts
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                }
                            } else {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        statusCode: 404,
                                        message: "Post details not found."
                                    }
                                });
                            }

                        }
                    });
                }
            }
        });
    },


    deletePost: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });
        if (!req.body.postId || !req.body.nibiId) {
            return true;
        }
        let findPost = {
            _id: postId
        };
        Post.findOne(findPost, function (err, post) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (post) {
                    Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                Post.updateOne(
                                    findPost,
                                    {
                                        isDeleted: true
                                    },
                                    function (err, item) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500,
                                                }
                                            });
                                        } else {

                                            let aggrQuery = [
                                                {
                                                    $match: {
                                                         '_id': mongoose.Types.ObjectId(postId),
                                                        'isDeleted': true
                                                    },
                                                },


                                                {
                                                    $project: {
                                                        'userScore': 1,
                                                        'eventId': 1,
                                                        'isDeleted': 1,
                                                        'nibiId': 1,
                                                        'hostNibiId': 1,
                                                        'comment': 1,
                                                        'postType': 1,
                                                        'coHosts': 1,
                                                        'sourceFile': 1,
                                                        'mention': 1,
                                                        'reply': 1,
                                                        'like': 1,
                                                        'addedAt': 1,
                                                        'convertedId': {
                                                            $toString: '$_id'
                                                        },
                                                        'user': 1,
                                                        'viewResultToAllGuest': 1,
                                                        'questionsForPoll': 1,
                                                        'options': 1
                                                    },
                                                },

                                                {
                                                    $lookup:
                                                        {
                                                            from: 'pollings',
                                                            localField: 'convertedId',
                                                            foreignField: 'postId',
                                                            as: 'PostDetail'
                                                        }
                                                },

                                                {
                                                    $project: {
                                                        'userScore': 1,
                                                        'eventId': 1,
                                                        'isDeleted': 1,
                                                        'nibiId': 1,
                                                        'hostNibiId': 1,
                                                        'comment': 1,
                                                        'postType': 1,
                                                        'coHosts': 1,
                                                        'sourceFile': 1,
                                                        'mention': 1,
                                                        'reply': 1,
                                                        'like': 1,
                                                        'addedAt': 1,
                                                        'convertedId': 1,
                                                        'user': 1,
                                                        'PostDetail': 1,
                                                        'postSize': {
                                                            $size: "$PostDetail"
                                                        },
                                                        'viewResultToAllGuest': 1,
                                                        'questionsForPoll': 1,
                                                        'options': 1
                                                    },
                                                },

                                                {
                                                    $unwind: {
                                                        path: "$PostDetail",
                                                        preserveNullAndEmptyArrays: true
                                                    }
                                                },

                                                {
                                                    $group:
                                                        {
                                                            _id: {
                                                                'convertedId': '$convertedId',
                                                                'optionId': '$PostDetail.optionId'
                                                            },
                                                            'countBy': {$sum: 1},
                                                            'userScore': {'$first': '$userScore'},
                                                            'eventId': {'$first': '$eventId'},
                                                            'isDeleted': {'$first': '$isDeleted'},
                                                            'nibiId': {'$first': '$nibiId'},
                                                            'hostNibiId': {'$first': '$hostNibiId'},
                                                            'comment': {'$first': '$comment'},
                                                            'postType': {'$first': '$postType'},
                                                            'coHosts': {'$first': '$coHosts'},
                                                            'sourceFile': {'$first': '$sourceFile'},
                                                            'mention': {'$first': '$mention'},
                                                            'reply': {'$first': '$reply'},
                                                            'like': {'$first': '$like'},
                                                            'addedAt': {'$first': '$addedAt'},
                                                            'user': {'$first': '$user'},
                                                            'PostDetail': {'$first': '$PostDetail'},
                                                            'postSize': {'$first': '$postSize'},
                                                            'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                            'questionsForPoll': {'$first': '$questionsForPoll'},
                                                            'options': {'$first': '$options'}
                                                        }
                                                },

                                                {
                                                    $project:
                                                        {
                                                            '_id': 0,
                                                            'convertedId': '$_id.convertedId',
                                                            'optionId': '$_id.optionId',
                                                            'countBy': 1,
                                                            'optionArrDet': {
                                                                'optionId': '$_id.optionId',
                                                                'countBy': '$countBy',
                                                                'optionName': '$PostDetail.optionName',
                                                                'optionIndex': '$PostDetail.optionIndex',
                                                                'percentageOfVote': {
                                                                    $cond: {
                                                                        if: {
                                                                            $eq: ["$postSize", 0]
                                                                        },
                                                                        then: 0,
                                                                        else: {$round: [{$divide: [{$multiply: ["$countBy", 100]}, '$postSize']}, 2]}
                                                                    }
                                                                },
                                                                'voteByUser': {
                                                                    $cond: {
                                                                        if: {
                                                                            $eq: [nibiId, '$PostDetail.nibiId']
                                                                        },
                                                                        then: true,
                                                                        else: false
                                                                    }
                                                                }
                                                            },

                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'user': 1,
                                                            // 'PostDetail': 1,
                                                            'postSize': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        }
                                                },

                                                {
                                                    $group:
                                                        {
                                                            _id: {
                                                                'convertedId': '$convertedId'
                                                            },
                                                            'optionArrDet': {'$push': '$optionArrDet'},
                                                            'userScore': {'$first': '$userScore'},
                                                            'eventId': {'$first': '$eventId'},
                                                            'isDeleted': {'$first': '$isDeleted'},
                                                            'nibiId': {'$first': '$nibiId'},
                                                            'hostNibiId': {'$first': '$hostNibiId'},
                                                            'comment': {'$first': '$comment'},
                                                            'postType': {'$first': '$postType'},
                                                            'coHosts': {'$first': '$coHosts'},
                                                            'sourceFile': {'$first': '$sourceFile'},
                                                            'mention': {'$first': '$mention'},
                                                            'reply': {'$first': '$reply'},
                                                            'like': {'$first': '$like'},
                                                            'addedAt': {'$first': '$addedAt'},
                                                            'user': {'$first': '$user'},
                                                            'postSize': {'$first': '$postSize'},
                                                            'viewResultToAllGuest': {'$first': '$viewResultToAllGuest'},
                                                            'questionsForPoll': {'$first': '$questionsForPoll'},
                                                            'options': {'$first': '$options'}
                                                        }
                                                },

                                                {
                                                    $project:
                                                        {
                                                            '_id': '$_id.convertedId',
                                                            'convertedId': 1,
                                                            'optionArrDet':
                                                                {
                                                                    $cond: {
                                                                        if: {
                                                                            $eq: ["$postSize", 0]
                                                                        },
                                                                        then: [],
                                                                        else: '$optionArrDet'
                                                                    }
                                                                },
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'user': 1,
                                                            'postSize': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        }
                                                },

                                                {
                                                    $lookup:
                                                        {
                                                            from: 'users',
                                                            localField: 'user',
                                                            foreignField: '_id',
                                                            as: 'userDetail'
                                                        }
                                                },

                                                {
                                                    $project:
                                                        {
                                                            '_id': 1,
                                                            'convertedId': '$_id.convertedId',
                                                            'optionArrDet':
                                                                {
                                                                    $cond: {
                                                                        if: {
                                                                            $eq: ["$postSize", 0]
                                                                        },
                                                                        then: [],
                                                                        else: '$optionArrDet'
                                                                    }
                                                                },
                                                            'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'userDetail': {$arrayElemAt: ['$userDetail', 0]},
                                                            'postSize': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        }
                                                },

                                                {
                                                    $project:
                                                        {
                                                            'convertedId': 1,
                                                            'optionArrDet': 1,
                                                            // 'userScore': 1,
                                                            'eventId': 1,
                                                            'isDeleted': 1,
                                                            'nibiId': 1,
                                                            'hostNibiId': 1,
                                                            'comment': 1,
                                                            'postType': 1,
                                                            'coHosts': 1,
                                                            'sourceFile': 1,
                                                            'mention': 1,
                                                            'reply': 1,
                                                            'like': 1,
                                                            'addedAt': 1,
                                                            'user': {
                                                                'fname': '$userDetail.fname',
                                                                'lname': '$userDetail.lname',
                                                                'profilePic': '$userDetail.profilePic',
                                                                '_id': '$userDetail._id',
                                                            },
                                                            'postSize': 1,
                                                            'viewResultToAllGuest': 1,
                                                            'questionsForPoll': 1,
                                                            'options': 1
                                                        }
                                                }
                                            ];

                                            Post.aggregate(aggrQuery,
                                                function (err, response) {
                                                    // Post.findOne(findPost).populate("user", "fname lname profilePic").select('-userScore').exec(function (err, deletedPost) {
                                                    if (err) {
                                                        res.json({
                                                            serverResponse: {
                                                                isError: true,
                                                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                                statusCode: 500,
                                                            }
                                                        });
                                                    } else {
                                                        let optionArrDet = response[0].optionArrDet;
                                                        let options = response[0].options;

                                                        let OptionIdListInoptionArrDet = optionArrDet.map((item) => {
                                                            return item.optionId;
                                                        })

                                                        let dataToInsert = {};

                                                        let i;
                                                        for (i = 0; i < options.length; i++) {
                                                            if (!OptionIdListInoptionArrDet.includes(options[i]._id.toString())) {
                                                                dataToInsert = {
                                                                    "optionId": options[i]._id.toString(),
                                                                    "countBy": 0,
                                                                    "optionName": options[i].optionName,
                                                                    "optionIndex": options[i].optionIndex,
                                                                    "percentageOfVote": 0,
                                                                    "voteByUser": false
                                                                }

                                                                optionArrDet.push(dataToInsert)
                                                            }
                                                        }

                                                        response[0].optionArrDet = optionArrDet;
                                                        response[0].options = options;

                                                        res.json({
                                                            serverResponse: {
                                                                isError: false,
                                                                message: errorMsgJSON[lang]["200"],
                                                                statusCode: 200
                                                            },
                                                            result: {
                                                                postDetails: response[0]
                                                            }
                                                        });
                                                    }
                                                });
                                        }
                                    });
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Post details not found."
                        }
                    });
                }
            }
        });
    },


    deletePostBkp: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let postId = req.body.postId
            ? req.body.postId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide post id."
                }
            });
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi id."
                }
            });
        if (!req.body.postId || !req.body.nibiId) {
            return true;
        }
        let findPost = {
            _id: postId
        };
        Post.findOne(findPost, function (err, post) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (post) {
                    Event.findOne({_id: post.eventId}).select({guestList: {$elemMatch: {nibiId: nibiId}}}).exec(function (err, event) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                },
                            });
                        } else {
                            if (event.guestList.length == 0) {
                                res.json({
                                    serverResponse: {
                                        isError: true,
                                        message: "You are no longer participant in this event.",
                                        statusCode: 410
                                    },
                                });
                            } else {
                                Post.updateOne(
                                    findPost,
                                    {
                                        isDeleted: true
                                    },
                                    function (err, item) {
                                        if (err) {
                                            res.json({
                                                serverResponse: {
                                                    isError: true,
                                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                    statusCode: 500,
                                                }
                                            });
                                        } else {
                                            Post.findOne(findPost).populate("user", "fname lname profilePic").select('-userScore').exec(function (err, deletedPost) {
                                                if (err) {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: true,
                                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                                            statusCode: 500,
                                                        }
                                                    });
                                                } else {
                                                    res.json({
                                                        serverResponse: {
                                                            isError: false,
                                                            message: errorMsgJSON[lang]["200"],
                                                            statusCode: 200
                                                        },
                                                        result: {
                                                            postDetails: deletedPost
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                            }
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            statusCode: 404,
                            message: "Post details not found."
                        }
                    });
                }
            }
        });
    },
    notificationList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let nibiId = req.body.nibiId
            ? req.body.nibiId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide nibi Id."
                }
            });
        let findNotification = {
            senderNibiId: {$ne: nibiId},
            $or: [
                {"receiver.nibiId": nibiId},
                {"mention.nibiId": nibiId}
            ],

        };
        let limit = 50;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let page = 1;
        page = (page - 1) * limit;
        if (req.body.page) {
            page = (req.body.page - 1) * limit;
        }
        Notification.find(findNotification).countDocuments().exec(function (err, notificationCount) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                Notification.find(findNotification).limit(limit).skip(page).sort({'addedAt': 'desc'}).exec(function (err, notification) {

                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            },
                        });
                    } else {
                        if (req.body.page == 1) {
                            User.updateOne({nibiId: nibiId}, {isNewNotification: false}, function (err, updateUser) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    let notificationArray = []
                                    notification.map(e => {
                                        let notifications = e.toJSON();
                                        notifications.timeDifference = moment(notifications.addedAt).fromNow(true);
                                        notifications.isRead = true
                                        notificationArray.push(notifications);
                                    })
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            notificationCount: notificationCount,
                                            notificationList: notificationArray
                                        },
                                    });
                                }
                            });
                        } else {
                            let notificationArray = []
                            notification.map(e => {
                                let notifications = e.toJSON();
                                notifications.timeDifference = moment(notifications.addedAt).fromNow(true);
                                notifications.isRead = true
                                notificationArray.push(notifications);
                            })
                            res.json({
                                serverResponse: {
                                    isError: false,
                                    message: errorMsgJSON[lang]["200"],
                                    statusCode: 200
                                },
                                result: {
                                    notificationCount: notificationCount,
                                    notificationList: notificationArray
                                },
                            });
                        }

                    }
                });

            }
        });
    },

}

function findFcmAndSendNotification(res, nibiIds, notificationBody) {
    User.find({nibiId: {$in: nibiIds}, isNotify: true}, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "Generic error - Error: ",
                    statusCode: 500
                }
            });
        } else {
            if (fcm.length > 0) {
                let fcmFilter = fcm.filter(function (e) {
                    return e.fcmToken != "";
                });
                let fcmTokens = fcmFilter.map(fcm => {
                    return fcm;
                });

                NotificationService.sendNotification(fcmTokens, notificationBody);
            } else {
                console.log("No user found to send notification.")
            }

        }
    });
}

function findGuestAndSendNotification(nibiId, item, res, notificationBody) {
    Event.findOne({_id: item.eventId}, function (err, guestNibiId) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: "Generic error - Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            let guestNibiIds = guestNibiId.guestList.map(e => {
                if (e.nibiId != nibiId) {
                    return e.nibiId
                }
            });
            User.find({nibiId: {$in: guestNibiIds}, isNotify: true}, function (err, fcm) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (fcm.length > 0) {
                        let fcmFilter = fcm.filter(function (e) {
                            return e.fcmToken != "";
                        });
                        let fcmTokens = fcmFilter.map(fcm => {
                            return fcm.fcmToken
                        });
                        NotificationService.sendNotification(fcmTokens, notificationBody);
                    } else {
                        console.log("No user found to send notification.")
                    }

                }
            })
        }
    })
}

function findGuestAndSaveNotification(item, req, res, lang, nibiId, postId) {
    User.findOne({nibiId: item.nibiId}, function (err, user) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                },
            });
        } else {
            let receiverArray = [];
            if (item.nibiId != nibiId) {
                let receiverObj = {
                    nibiId: item.nibiId,
                    firstName: user.fname,
                    lastName: user.lname,
                    imageUrl: user.profilePic
                }
                receiverArray.push(receiverObj)
            }

            if (req.body.mentionOnReply.length > 0 || receiverArray.length > 0) {
                let newNotification = new Notification({
                    senderNibiId: nibiId,
                    senderFirstName: req.body.firstName,
                    senderLastName: req.body.lastName,
                    senderImage: req.body.userImage,
                    purpose: "replied to your post in",
                    text: req.body.replyText,
                    eventId: item.eventId,
                    eventName: req.body.title,
                    postId: postId,
                    notificationType: "comment",
                    receiver: receiverArray,
                    mention: req.body.mentionOnReply
                });
                newNotification.save(function (err, notification) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            }
                        });
                    } else {
                        if (receiverArray.length > 0) {
                            User.updateOne({nibiId: receiverArray[0].nibiId}, {isNewNotification: true}, function (err, isNewNotificationStatus) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        }
                                    });
                                } else {
                                    console.log("Notification saved to db");
                                }
                            });
                        } else {
                            console.log("Notification saved to db");
                        }
                    }
                });
            } else {
                console.log("No receiver or mentioner found.");
            }


        }
    });


}

function saveNotificationOnReplyOnReply(reply, req, res, lang, nibiId, item, postId) {
    User.findOne({nibiId: item.nibiId}, function (err, postCreator) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                },
            });
        } else {
            let receiverArray = [];
            if (reply.reply[0].nibiId != nibiId) {
                let receiverObj = {
                    nibiId: reply.reply[0].nibiId,
                    firstName: reply.reply[0].firstName,
                    lastName: reply.reply[0].lastName,
                    imageUrl: reply.reply[0].userImage,
                }
                receiverArray.push(receiverObj);
            }
            if (postCreator.nibiId != nibiId) {
                let receiverObj = {
                    nibiId: postCreator.nibiId,
                    firstName: postCreator.fname,
                    lastName: postCreator.lname,
                    imageUrl: postCreator.profilePic,
                }
                receiverArray.push(receiverObj)
            }
            if (receiverArray.length > 0 || req.body.mentionOnReplyOnReply > 0) {
                let newNotification = new Notification({
                    senderNibiId: nibiId,
                    senderFirstName: req.body.firstName,
                    senderLastName: req.body.lastName,
                    senderImage: req.body.userImage,
                    purpose: "Replied to your post in",
                    text: req.body.replyText,
                    eventId: item.eventId,
                    eventName: req.body.title,
                    postId: postId,
                    notificationType: "reply",
                    receiver: receiverArray,
                    mention: req.body.mentionOnReplyOnReply
                });
                newNotification.save(function (err, notification) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            }
                        });
                    } else {
                        let updatedUserNibiIds = receiverArray.map(e => {
                            return e.nibiId
                        });
                        updatedUserNibiIds.forEach(nibiIdOfUser => {
                            User.updateMany({nibiId: nibiIdOfUser}, {isNewNotification: true}, function (err, updateNewNotificationStatus) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        }
                                    });
                                } else {
                                    console.log("Notification saved to db")
                                }
                            });
                        });
                    }
                });
            } else {
                console.log("No Notification saved due to same nibi id")
            }

        }
    });


}

function saveNotificationForCreatePost(item, req, res, lang, nibiId, guestNibiId, eventId, item) {
    User.findOne({nibiId: nibiId}, function (err, user) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            let guestFilter = guestNibiId.guestList.filter(function (e) {
                return e.nibiId != nibiId;
            });
            if (req.body.mention.length > 0 || guestFilter.length > 0) {
                let newNotification = new Notification({
                    senderNibiId: nibiId,
                    senderFirstName: user.fname,
                    senderLastName: user.lname,
                    senderImage: user.profilePic,
                    purpose: "posted in",
                    text: req.body.comment,
                    eventId: eventId,
                    eventName: guestNibiId.title,
                    postId: item._id,
                    notificationType: "create_post",
                    receiver: guestFilter,
                    mention: req.body.mention
                });
                newNotification.save(function (err, notification) {
                    if (err) {
                        res.json({
                            serverResponse: {
                                isError: true,
                                message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                statusCode: 500
                            }
                        });
                    } else {
                        // Below functionality to update all guest profile isNewNotification key
                        let updateUserNibiIds = guestFilter.map(guest => {
                            return guest.nibiId
                        });
                        let mentionFilter = req.body.mention.filter(function (e) {
                            return e.nibiId != nibiId;
                        });
                        let updateMantionNibiIds = mentionFilter.map(mentionUser => {
                            return mentionUser.nibiId
                        });
                        let finalUpdatedUser = updateUserNibiIds.concat(updateMantionNibiIds);

                        finalUpdatedUser.forEach(nibiIdOfUser => {
                            User.updateMany({nibiId: nibiIdOfUser}, {isNewNotification: true}, function (err, updateNewNotificationStatus) {
                                console.log(updateNewNotificationStatus);
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        }
                                    });
                                } else {
                                    console.log("Notification saved to db")
                                }
                            });
                        });
                    }
                });
            } else {
                console.log("No guest or mentioner found.")
            }
        }
    });
}

// This push notification for the creator of a post when a user reply on a comment.
function creatorOfPostNotificationForReplyOfComment(req, res, item, lang, postId, item) {
    User.findOne({nibiId: item.nibiId, isNotify: true}, function (err, postCreatorDetails) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            // let notificationBody = {
            //     title: req.body.title,
            //     body: req.body.firstName + " "+ req.body.lastName+ " replied to your post: " + req.body.replyText
            // }
            let payload = {
                notification: {
                    title: req.body.title,
                    body: req.body.firstName + " " + req.body.lastName + " replied to your post: " + req.body.replyText
                },
                data: {
                    title: req.body.title,
                    body: req.body.firstName + " " + req.body.lastName + " replied to your post: " + req.body.replyText,
                    notificationType: 'reply',
                    postId: postId,
                    eventId: item.eventId
                }
            };
            if (postCreatorDetails) {
                if (postCreatorDetails.fcmToken) {
                    let fcmTokens = [];
                    fcmTokens.push(postCreatorDetails);
                    NotificationService.sendNotification(fcmTokens, payload);
                } else {
                    console.log("creator of post not wants to get notification.");
                }
            } else {
                console.log("creator of post and replier is same person.");
            }

        }
    });
}

function sendNotificationToReplier(req, res, replyNibiId, lang, postId, item) {
    User.findOne({nibiId: replyNibiId, isNotify: true}, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            // let notificationBody = {
            //     title: req.body.title,
            //     body: req.body.firstName + " "+ req.body.lastName+ " replied to your reply: " + req.body.replyText
            // }
            let payload = {
                notification: {
                    title: req.body.title,
                    body: req.body.firstName + " " + req.body.lastName + " replied to your reply: " + req.body.replyText
                },
                data: {
                    title: req.body.title,
                    body: req.body.firstName + " " + req.body.lastName + " replied to your reply: " + req.body.replyText,
                    notificationType: 'reply',
                    postId: postId,
                    eventId: item.eventId
                }
            };
            if (fcm) {
                if (fcm.fcmToken) {
                    let fcmTokens = [];
                    fcmTokens.push(fcm);
                    NotificationService.sendNotification(fcmTokens, payload);
                } else {
                    console.log("Replier not wants to get notification.")
                }
            } else {
                console.log("Replier of comment and owner of comment is same user.")
            }

        }
    });
}

function sendNotificationToPostCreatorForPostLike(req, res, nibiId, post, lang) {
    User.findOne({nibiId: nibiId, isNotify: true}, function (err, fcm) {
        if (err) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                    statusCode: 500
                }
            });
        } else {
            let notificationBody = {
                title: "Liked your post.",
                body: req.body.firstName + " " + req.body.lastName + " Liked your post: " + post.comment
            }
            if (fcm.fcmToken) {
                let fcmTokens = fcm.fcmToken
                NotificationService.sendNotification(fcmTokens, notificationBody);
            } else {
                console.log("Replier not wants to get notification.")
            }
        }
    });
}

function saveNotificationForLikeOnPost(req, res, nibiId, post, userDetails, postCreator, postId, lang) {
    let receiverArray = [];
    if (post.nibiId != nibiId) {
        let receiverObj = {
            nibiId: post.nibiId,
            firstName: postCreator.fname,
            lastName: postCreator.lname,
            imageUrl: postCreator.profilePic
        }
        receiverArray.push(receiverObj)
    }
    if (receiverArray.length > 0 || post.mention.length > 0) {
        let newNotification = new Notification({
            senderNibiId: nibiId,
            senderFirstName: userDetails.fname,
            senderLastName: userDetails.lname,
            senderImage: userDetails.profilePic,
            purpose: "Liked your post in",
            text: post.comment,
            eventId: post.eventId,
            eventName: req.body.title,
            postId: postId,
            notificationType: "liked_post",
            receiver: receiverArray,
            mention: post.mention
        });
        newNotification.save(function (err, notification) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    }
                });
            } else {
                if (receiverArray.length > 0) {
                    User.updateOne({nibiId: receiverArray[0].nibiId}, {isNewNotification: true}, function (err, isNewNotificationStatus) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                }
                            });
                        } else {
                            console.log("Notification saved to db for like on post.");
                        }
                    });
                } else {
                    console.log("Notification saved to db for like on post.");
                }
            }
        });
    } else {
        console.log("Notification not saved due to same id and not mention any");
    }
}

function saveNotificationForLikeOnComment(req, res, nibiId, userDetails, item, postId, lang) {
    let receiverArray = [];
    if (item.reply[0].nibiId != nibiId) {
        let receiverObj = {
            nibiId: item.reply[0].nibiId,
            firstName: item.reply[0].firstName,
            lastName: item.reply[0].lastName,
            imageUrl: item.reply[0].userImage
        }
        receiverArray.push(receiverObj);
    }
    if (receiverArray.length > 0 || item.reply[0].mentionOnReply.length > 0) {
        let newNotification = new Notification({
            senderNibiId: nibiId,
            senderFirstName: userDetails.fname,
            senderLastName: userDetails.lname,
            senderImage: userDetails.profilePic,
            purpose: "Liked your reply in",
            text: item.reply[0].replyText,
            eventId: item.eventId,
            eventName: req.body.title,
            postId: postId,
            notificationType: "liked_comment",
            receiver: receiverArray,
            mention: item.reply[0].mentionOnReply
        });
        newNotification.save(function (err, notification) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    }
                });
            } else {
                if (receiverArray.length > 0) {
                    User.updateOne({nibiId: receiverArray[0].nibiId}, {isNewNotification: true}, function (err, isNewNotificationStatus) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                }
                            });
                        } else {
                            console.log("Notification saved to db for like on comment");
                        }
                    });
                } else {
                    console.log("Notification saved to db for like on comment");
                }
            }
        });
    } else {
        console.log("Notification not saved due to same id and not mention any");
    }
}

function saveNotificationForLikeOnReply(req, res, nibiId, userDetails, findEventId, postId, lang) {
    let receiverArray = [];
    if (req.body.replyOnReplyNibiId != nibiId) {
        let receiverObj = {
            nibiId: req.body.replyOnReplyNibiId,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            imageUrl: req.body.userImage
        }
        receiverArray.push(receiverObj);
    }
    if (receiverArray.length > 0) {
        let newNotification = new Notification({
            senderNibiId: nibiId,
            senderFirstName: userDetails.fname,
            senderLastName: userDetails.lname,
            senderImage: userDetails.profilePic,
            purpose: "liked your reply in",
            text: req.body.replyText,
            eventId: findEventId.eventId,
            eventName: req.body.title,
            postId: postId,
            notificationType: "liked_reply",
            receiver: [
                {
                    nibiId: req.body.replyOnReplyNibiId,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    imageUrl: req.body.userImage
                }
            ]
        });
        newNotification.save(function (err, notification) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    }
                });
            } else {
                if (receiverArray.length > 0) {
                    User.updateOne({nibiId: receiverArray[0].nibiId}, {isNewNotification: true}, function (err, isNewNotificationStatus) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500
                                }
                            });
                        } else {
                            console.log("Notification saved to db for like on reply");
                        }
                    });
                } else {
                    console.log("Notification saved to db for like on reply");
                }
            }
        });
    } else {
        console.log("Notification not saved due to same id and not mention any");
    }
}


async function updatePollings(optionId, optionName) {
    console.log(optionId);
    console.log(optionName)
    return new Promise(function (resolve, reject) {
        Polling.updateMany({
            optionId: optionId
        },{
            $set: { optionName: optionName }
        },function (err, response) {
    
            if (err) {
                resolve({
                    "isError": true,
                });
    
                return;
            } else {
                resolve({
                    "isError": false
                });
            }
    
        }); // END newUser.save(function(err, item)
    }) // END return new Promise(function (resolve, reject) {

} // async function updatePollings(postId, options) {   


async function updateOption(postId, options, req, questionsForPoll) {

    return new Promise(function (resolve, reject) {
        Post.updateOne({
            _id: postId
        },{
            $set: { options: options, viewResultToAllGuest: req.body.viewResultToAllGuest, questionsForPoll: questionsForPoll }
        },function (err, response) {
    
            if (err) {
                resolve({
                    "isError": true,
                });
    
                return;
            } else {
                resolve({
                    "isError": false
                });
            }
    
        }); // END newUser.save(function(err, item)
    }) // END return new Promise(function (resolve, reject) {

    // options.forEach(async (optionArray) => {
    //     try {

    //         let promiseResult = [];
    //       await Post.updateOne({
    //             _id: postId,
    //             "options._id": optionArray._id
    //         }, {
    //             'options.$.optionName': optionArray.optionName,
    //             'options.$.optionIndex': optionArray.optionIndex
    //         });
    //         await Polling.updateOne({postId: postId, optionId: optionArray._id}, {
    //             optionName: optionArray.optionName,
    //             optionIndex: optionArray.optionIndex
    //         });

    //     } catch (err) {
    //         return res.json({
    //             serverResponse: {
    //                 isError: true,
    //                 message: err,
    //                 statusCode: 500
    //             },
    //         });
    //     }

    // });
    //     return true ;
}

//---------------------------------------------------------------------------------
  // function updateOption (){
  //     options.forEach(async (optionArray) => {
  //         try {
  //
  //             let promiseResult = [];
  //             promiseResult[0] = Post.updateOne({
  //                 _id: postId,
  //                 "options._id": optionArray._id
  //             }, {
  //                 'options.$.optionName': optionArray.optionName,
  //                 'options.$.optionIndex': optionArray.optionIndex
  //             });
  //             promiseResult[1] = Polling.updateOne({postId: postId, optionId: optionArray._id}, {
  //                 optionName: optionArray.optionName,
  //                 optionIndex: optionArray.optionIndex
  //             });
  //
  //
  //             Promise.all(promiseResult).then((values) => {
  //                 console.log(values);
  //
  //             });
  //         } catch (err) {
  //             return res.json({
  //                 serverResponse: {
  //                     isError: true,
  //                     message: err,
  //                     statusCode: 500
  //                 },
  //             });
  //         }
  //
  //     });
  // }