const mongoose = require("mongoose");
const Category = require("../models/category");
const errorMsgJSON = require("../../../services/errors.json");

module.exports = {

    createCategory: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let categoryName = req.body.categoryName
            ? req.body.categoryName
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: errorMsgJSON[lang]["404"] + " category name."
                }
            });
        if(!req.body.categoryName){
            return true ;
        }
        try {

            let newCategory = new Category({
                categoryName: categoryName,
                imageUrl: req.body.imageUrl
            });
            await newCategory.save(function (err, item) {
                if (err) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                            statusCode: 500
                        }
                    });
                } else {
                    if (item) {
                        res.json({
                            serverResponse: {
                                isError: false,
                                message: errorMsgJSON[lang]["200"],
                                statusCode: 200
                            },
                            result: {
                                categoryDetails: item
                            }
                        });
                    } else {
                        res.json({
                            serverResponse: {
                                isError: true,
                                statusCode: 404,
                                message: errorMsgJSON[lang]["404"]
                            }
                        });
                    }
                }
            });
        } catch (e) {
            res.json({
                serverResponse: {
                    isError: true,
                    message: errorMsgJSON[lang]["500"] + "- Error: " + e,
                    statusCode: 500
                }
            });
        }
    },
    categoryList: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";

        Category.find({}).sort({'categoryName': 'asc'}).exec( function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            categoryDetails: item
                        }
                    });
                }
            }
        });
    },
    specificCategory: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        let categoryId = req.body.categoryId
            ? req.body.categoryId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide category id."
                }
            });
        if(!req.body.categoryId){
            return true ;
        }
        let findCategory = {
            _id: categoryId
        };
        Category.findOne(findCategory, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statusCode: 200
                        },
                        result: {
                            categoryDetails: item
                        }
                    });
                }
            }
        });
    },
    updateCategory: function (req, res, next) {
        let lang = req.headers.language ? req.headers.language : "EN";
        let categoryId = req.body.categoryId
            ? req.body.categoryId
            : res.json({
                serverResponse: {
                    isError: true,
                    statusCode: 404,
                    message: "Please provide category id."
                }
            });
        if(!req.body.categoryId){
            return true ;
        }
        let query = {
            _id: categoryId
        };

        Category.findOne(query, function (err, item) {
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                        statusCode: 500,
                    }
                });
            } else {
                if (!item) {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statusCode: 404
                        }
                    });
                } else {
                    delete req.body.categoryId;
                    let updateValues = {
                        $set: {
                            ...req.body
                        }
                    };
                    Category.updateOne(query, updateValues, function (err, res1) {
                        if (err) {
                            res.json({
                                serverResponse: {
                                    isError: true,
                                    message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                    statusCode: 500,
                                }
                            });
                        } else {
                            Category.findOne(query, function (err, updatedItem) {
                                if (err) {
                                    res.json({
                                        serverResponse: {
                                            isError: true,
                                            message: errorMsgJSON[lang]["500"] + "- Error: " + err,
                                            statusCode: 500
                                        },
                                    });
                                } else {
                                    res.json({
                                        serverResponse: {
                                            isError: false,
                                            message: errorMsgJSON[lang]["200"],
                                            statusCode: 200
                                        },
                                        result: {
                                            categoryDetails: updatedItem
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};
